# Pharmacy chain management system (DEPRECATED)

Pharmacy chain management system implemented in three-person team.

Technologies
---
Project is created with: Java, Spring, Hibernate, MySQL, HTML, CSS, JavaScript, Maven, Tomcat

Getting Started
---
In home directory, run the following command to download project:

    git clone https://trzebinski@bitbucket.org/trzebinski/pharmacy-chain-management-system.git
