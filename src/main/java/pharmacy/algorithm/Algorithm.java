package pharmacy.algorithm;

import pharmacy.model.DateParser;
import pharmacy.model.MedicineStateInGivenPharmacy;
import pharmacy.model.MedicineWithShortBestBeforeDate;
import pharmacy.model.RequestToGivenPharmacy;

import java.util.HashMap;
import java.util.List;

public class Algorithm {
    final static double COUNT_THRESHOLD = 0.5;
    final static double SEND_THRESHOLD = 0.5;
    final static double HIGH_PRIORITY_THRESHOLD = 0.3;
    final static double MEDIUM_PRIORITY_THRESHOLD = 0.7;

    public static HashMap<Integer, Integer> getActionOnMissingMedicines(List<List<MedicineStateInGivenPharmacy>> list, int currentPharmacy) {
        HashMap toReturn = new HashMap<Integer, Integer>();

        if (list.get(currentPharmacy) != null) {
            for (MedicineStateInGivenPharmacy m : list.get(currentPharmacy)) {
                if (m.getCount() < (m.getDemandedCount() * COUNT_THRESHOLD) && transferMedicinesFromAnotherPharmacies(list, currentPharmacy, m)) {
                    toReturn.put(m.getId(), 1);
                } else
                    toReturn.put(m.getId(), 2);
            }
        }
        return toReturn;
    }

    public static boolean transferMedicinesFromAnotherPharmacies(List<List<MedicineStateInGivenPharmacy>> list, int currentPharmacy, MedicineStateInGivenPharmacy med) {
        boolean canTransfer = false;
        for (int i = 0; i < list.size(); i++) {
            if (i != currentPharmacy && list.get(i) != null) {
                for (MedicineStateInGivenPharmacy m : list.get(i)) {
                    if (m.getId() == med.getId()) {
                        if (m.getCount() > m.getDemandedCount() || m.getCount() - med.getDeficite() >= m.getDemandedCount() * COUNT_THRESHOLD) {
                            canTransfer = true;
                        }
                    }
                }
            }
        }

        return canTransfer;
    }

    public static HashMap<Integer, Integer> getDecisionOnRequest(List<MedicineStateInGivenPharmacy> list, List<RequestToGivenPharmacy> requests) {
        HashMap toReturn = new HashMap<Integer, Integer>();

        for (RequestToGivenPharmacy request : requests) {
            MedicineStateInGivenPharmacy ourPharmacy = getMedicineWithId(list, request.getId());

            if (ourPharmacy.getCount() - request.getNumber() > ourPharmacy.getDemandedCount() * SEND_THRESHOLD) {

                toReturn.put(request.getRequestId(), 1);
            } else {

                toReturn.put(request.getRequestId(), 0);
            }
        }
        return toReturn;
    }

    public static MedicineStateInGivenPharmacy getMedicineWithId(List<MedicineStateInGivenPharmacy> medicine, int id) {
        MedicineStateInGivenPharmacy toReturn = null;

        for (MedicineStateInGivenPharmacy m : medicine) {
            if (m.getId() == id)
                toReturn = m;
        }

        return toReturn;
    }

    public static void setPriorityOfRequest(List<List<MedicineStateInGivenPharmacy>> list, List<List<RequestToGivenPharmacy>> requests) {
        MedicineStateInGivenPharmacy medicine;
        for (int i = 0; i < requests.size(); i++) {
            for (int j = 0; j < requests.get(i).size(); j++) {
                medicine = getMedicineWithId(list.get(requests.get(i).get(j).getPharmacySourceId() - 1), requests.get(i).get(j).getId());

                if (medicine.getCount() <= medicine.getDemandedCount() * HIGH_PRIORITY_THRESHOLD) {
                    requests.get(i).get(j).setPriority("Wysoki");

                } else if (medicine.getCount() <= medicine.getDemandedCount() * MEDIUM_PRIORITY_THRESHOLD) {
                    requests.get(i).get(j).setPriority("Sredni");
                } else {
                    requests.get(i).get(j).setPriority("Niski");
                }
            }

        }
    }

    public static HashMap<Integer, Integer> getDecisionSendOrUtilize(List<MedicineWithShortBestBeforeDate> shortBestBefore, List<List<MedicineStateInGivenPharmacy>> medicines,
                                                                     int currentPharmacy, String currentDate) {
        HashMap toReturn = new HashMap<Integer, Integer>();

        for (MedicineWithShortBestBeforeDate medicine : shortBestBefore) {
            if (DateParser.shortDate(medicine.getExpirationDate()).compareTo(DateParser.getDateWithShift_withStartDate(currentDate, 0)) < 0) {
                toReturn.put(medicine.getSerial(), 1);
            } else if (otherPharmacyCanSoldBefore(medicines, currentPharmacy, medicine.getId())) {
                toReturn.put(medicine.getSerial(), 2);
            } else
                toReturn.put(medicine.getSerial(), 0);
        }

        return toReturn;
    }

    public static boolean otherPharmacyCanSoldBefore(List<List<MedicineStateInGivenPharmacy>> list, int currentPharmacy, int id) {
        boolean canSoldBefore = false;
        MedicineStateInGivenPharmacy medicine = null;
        for (MedicineStateInGivenPharmacy m : list.get(currentPharmacy)) {
            if (m.getId() == id)
                medicine = m;
        }

        for (int i = 0; i < list.size(); i++) {
            if (i != currentPharmacy) {
                for (int j = 0; j < list.get(i).size(); j++) {
                    if (list.get(i).get(j).getId() == medicine.getId() && list.get(i).get(j).getDeficite() > medicine.getDeficite())
                        canSoldBefore = true;
                }
            }
        }

        return canSoldBefore;
    }
}
