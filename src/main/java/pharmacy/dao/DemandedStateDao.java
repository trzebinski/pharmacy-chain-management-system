package pharmacy.dao;

import pharmacy.model.MedicineStateInGivenPharmacy;

import java.util.List;

public interface DemandedStateDao {
    List<MedicineStateInGivenPharmacy> demandedState(int pharmacyId);

    List<MedicineStateInGivenPharmacy> demandedState(int pharmacyId, String drugName,
                                                     String kind, String order);

    boolean changeDemandedState(int pharmacyId, int typeId, int newDemandedState);
}
