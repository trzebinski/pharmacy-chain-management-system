package pharmacy.dao;

        import pharmacy.entity.Medicine_Warehouse;

public interface Medicine_WarehouseDao {
    boolean addMedicine_WarehouseDao(Medicine_Warehouse medicine_warehouse);

    int getPharmacyIdWithGivenMedicineSerial(int serial);
}
