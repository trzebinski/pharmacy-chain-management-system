package pharmacy.dao;

import pharmacy.model.MedicineToRaport;

import java.util.List;

public interface RaportDao {
    List<MedicineToRaport> getMedicineToRaport(int pharmacyId, String currentDate);
}
