package pharmacy.dao;

import pharmacy.entity.Time;

public interface TimeDao {
    boolean setDate(String date);

    boolean setTime(String time);

    Time getTimeObj();
}
