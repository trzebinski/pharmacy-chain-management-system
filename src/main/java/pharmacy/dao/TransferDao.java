package pharmacy.dao;

import pharmacy.entity.Transfer;
import pharmacy.model.TransferToDestination;
import pharmacy.model.TransferToSource;

import java.util.List;

/**
 * Created by Konrad on 23.11.2017.
 */
public interface TransferDao {
    List<Transfer> getTransfers();

    List<TransferToSource> getTransferToSource(int pharmacyId, String currentDate, String currentTime);

    List<TransferToDestination> getTransferToDestination(int pharmacyId, String currentDate, String currentTime);

    void newTransfer(int serial, int pharmacy_dst, int pharmacy_src, String[] startDate, String[] endDate);

    int getMaxId();

    boolean addTransfer(Transfer transfer);
}
