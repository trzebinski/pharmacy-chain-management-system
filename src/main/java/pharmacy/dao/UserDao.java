package pharmacy.dao;

public interface UserDao {
    boolean logIn(String login, String password);
}
