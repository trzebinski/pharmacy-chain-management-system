package pharmacy.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pharmacy.dao.DemandedStateDao;
import pharmacy.model.MedicineStateInGivenPharmacy;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DemandedStateDaoImpl implements DemandedStateDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<MedicineStateInGivenPharmacy> demandedState(int pharmacyId) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery<Object[]> query = session.createNativeQuery("SELECT T.id, T.name, T.kind, T.dose, T.doseUnit, T.capacity, COUNT(MW.id) AS Count, DS.number " +
                " FROM ((DemandedState DS JOIN Type T ON DS.type = T.id) LEFT JOIN Medicine M ON M.type = T.id) LEFT JOIN ((SELECT * FROM Medicine_Warehouse MW WHERE MW.warehouse = :warehouse and MW.status = 1) AS MW) ON M.serial = MW.serial " +
                " WHERE DS.warehouse = :warehouse and M.utilized = 0 " +
                " GROUP By DS.type")
                .setParameter("warehouse", pharmacyId);

        List<Object[]> rows = query.list();

        System.out.println(rows.size());

        List<MedicineStateInGivenPharmacy> resultList = new ArrayList<>();
        for (Object[] current : rows) {
            if (current[3] == null)
                current[3] = 0;
            resultList.add(new MedicineStateInGivenPharmacy(pharmacyId,
                    ((BigInteger) current[6]).intValue(),
                    (int) current[0],
                    (String) current[1],
                    (String) current[2],
                    (int) current[3],
                    (String) current[4],
                    (int) current[5],
                    (int) current[7]));
        }

        return resultList;
    }

    @Override
    public List<MedicineStateInGivenPharmacy> demandedState(int pharmacyId, String drugName, String kind, String order) {
        Session session = sessionFactory.getCurrentSession();

        String queryString = "SELECT T.id, T.name, T.kind, T.dose, T.doseUnit, T.capacity, COUNT(MW.id) AS drugCount, DS.number, :drugName AS d, :kind as k " +
                " FROM ((DemandedState DS JOIN Type T ON DS.type = T.id) LEFT JOIN Medicine M ON M.type = T.id) LEFT JOIN ((SELECT * FROM Medicine_Warehouse MW WHERE MW.warehouse = :warehouse and MW.status = 1) AS MW) ON M.serial = MW.serial " +
                " WHERE DS.warehouse = :warehouse and M.utilized = 0";

        if (!drugName.equals(""))
            queryString += " and T.name = :drugName ";

        if (!kind.equals(""))
            queryString += " and T.kind = :kind ";


        queryString += " GROUP BY T.id ORDER BY ";
        queryString += getKind(order);

        NativeQuery<Object[]> query = session.createNativeQuery(queryString)
                .setParameter("warehouse", pharmacyId)
                .setParameter("kind", kind.equals("") ? "All" : kind)
                .setParameter("drugName", drugName.equals("") ? "All" : drugName);

        List<Object[]> rows = query.list();

        List<MedicineStateInGivenPharmacy> resultList = new ArrayList<>();
        for (Object[] current : rows) {
            resultList.add(new MedicineStateInGivenPharmacy(pharmacyId,
                    ((BigInteger) current[6]).intValue(),
                    (int) current[0],
                    (String) current[1],
                    (String) current[2],
                    (int) current[3],
                    (String) current[4],
                    (int) current[5],
                    (int) current[7]));
        }

        return resultList;
    }

    @Override
    public boolean changeDemandedState(int pharmacyId, int typeId, int newDemandedState) {

        Session session = sessionFactory.getCurrentSession();

        String queryString = " UPDATE DemandedState SET number = :newDemandedState WHERE warehouse = :pharmacyId and type = :typeId ";
        int result = session.createNativeQuery(queryString)
                .setParameter("newDemandedState", newDemandedState)
                .setParameter("pharmacyId", pharmacyId)
                .setParameter("typeId", typeId)
                .executeUpdate();

        return result == 1;
    }

    private String getKind(String order) {
        switch (order) {
            case "drugName":
                return " T.name ";
            case "count":
                return " drugCount ";
            case "capacity":
                return " T.capacity ";
            case "price":
                return " T.price ";
        }
        return "";
    }
}
