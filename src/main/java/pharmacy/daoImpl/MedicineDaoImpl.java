package pharmacy.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pharmacy.dao.MedicineDao;
import pharmacy.entity.Medicine;
import pharmacy.model.PharmacyMedicineTypeState;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MedicineDaoImpl implements MedicineDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public int getMaxSerialNumber() {
        String query = " SELECT MAX(serial) FROM Medicine ";

        int maxSerialNumber = (int) sessionFactory.getCurrentSession()
                .createQuery(query)
                .getSingleResult();

        return maxSerialNumber;
    }

    @Override
    public boolean addMedicine(Medicine medicine) {

        Session session = sessionFactory.getCurrentSession();

        NativeQuery query = session.createNativeQuery("INSERT INTO Medicine (serial, expiryDate, type, utilized, soldDate) " +
                " VALUES (:serial, :expiryDate, :type, :utilized, :soldDate)")
                .setParameter("serial", medicine.getSerial())
                .setParameter("expiryDate", medicine.getExpiryDate())
                .setParameter("type", medicine.getType())
                .setParameter("utilized", medicine.getUtilized())
                .setParameter("soldDate", medicine.getSoldDate());

        query.executeUpdate();


        return true;
    }

    @Override
    public List<Medicine> getMedicines() {
        List medicinesList = sessionFactory.getCurrentSession()
                .createQuery("select m from Medicine m").list();

        System.out.println("Rozmiar: " + medicinesList.size());

        return medicinesList;
    }

    @Override
    public List<Medicine> getMedicines(int pharmacyId, int typeId) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery<Object[]> query = session.createNativeQuery("SELECT M.serial, M.expiryDate, M.type, M.utilized, M.soldDate " +
                " FROM ((Medicine_Warehouse MW JOIN Medicine M ON MW.serial=M.serial) JOIN Type T ON M.type=T.id ) LEFT JOIN Transfer TR ON TR.serial=M.serial " +
                " WHERE MW.status = 1 and M.utilized = 0 and MW.warehouse = :warehouse and M.type = :type " +
                " and (TR.endDate IS NULL    OR    TR.endDate < (SELECT date FROM Time)   OR  (TR.endDate = (SELECT date FROM Time) and TR.endTime <= (SELECT time FROM Time))) ")
                .setParameter("warehouse", pharmacyId)
                .setParameter("type", typeId);


        List<Object[]> rows = query.list();

        System.out.println(rows.size());

        List<Medicine> resultList = new ArrayList<>();
        for (Object[] current : rows) {
            System.out.println("Leki szczegóły: " + current[0] + " " + current[1] + " " + current[2] + " " +
                    current[3] + " " + current[4]);

            resultList.add(new Medicine(
                    (int) current[0],
                    (String) current[1],
                    (int) current[2],
                    (int) current[3],
                    (String) current[4]
            ));
        }

        return resultList;
    }

    @Override
    public List<PharmacyMedicineTypeState> getGroupedMedicines(int typeId, int currentPharmacyId) {
        Session session = sessionFactory.getCurrentSession();

        String queryString = " SELECT W.id, W.name, COUNT(MW.warehouse) AS counter " +
                " FROM ((Medicine_Warehouse MW JOIN Medicine M ON MW.serial=M.serial) JOIN Type T ON M.type=T.id) " +
                " JOIN Warehouse W ON W.id=MW.warehouse " +
                " WHERE T.id = :typeId and MW.status = 1 and M.utilized = 0 and MW.warehouse <> :currentPharmacyId " +
                " GROUP BY MW.warehouse ";

        NativeQuery<Object[]> query = session.createNativeQuery(queryString)
                .setParameter("typeId", typeId)
                .setParameter("currentPharmacyId", currentPharmacyId);

        List<Object[]> rows = query.list();

        List<PharmacyMedicineTypeState> resultList = new ArrayList<>();
        for (Object[] current : rows) {
            resultList.add(new PharmacyMedicineTypeState(
                    (int) current[0],
                    (String) current[1],
                    ((BigInteger) current[2]).intValue()
            ));
        }

        return resultList;
    }

    @Override
    public boolean utilizeMedicine(int serial) {
        Session session = sessionFactory.getCurrentSession();

        String queryString = " UPDATE Medicine SET utilized = 1 WHERE serial = :serial and utilized=0 ";
        int result = session.createNativeQuery(queryString).setParameter("serial", serial).executeUpdate();

        return result == 1;
    }

    @Override
    public boolean soldMedicine(int serial) {
        Session session = sessionFactory.getCurrentSession();

        String queryString = " UPDATE Medicine SET utilized = 2 WHERE serial = :serial and utilized=0 ";
        int result = session.createNativeQuery(queryString).setParameter("serial", serial).executeUpdate();

        return result == 1;
    }

    @Override
    public boolean transferMedicine(int serial, int oldPharmacy, int newPharmacy) {
        Session session = sessionFactory.getCurrentSession();
        String queryString = " UPDATE Medicine_Warehouse SET warehouse = :newPharmacy WHERE serial = :serial and warehouse = :oldPharmacy and status = 1 ";
        int result = session.createNativeQuery(queryString)
                .setParameter("serial", serial)
                .setParameter("oldPharmacy", oldPharmacy)
                .setParameter("newPharmacy", newPharmacy)
                .executeUpdate();

        return result == 1;
    }
}
