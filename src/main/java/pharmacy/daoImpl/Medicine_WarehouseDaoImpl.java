package pharmacy.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pharmacy.dao.Medicine_WarehouseDao;
import pharmacy.entity.Medicine_Warehouse;

@Repository
public class Medicine_WarehouseDaoImpl implements Medicine_WarehouseDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean addMedicine_WarehouseDao(Medicine_Warehouse medicine_warehouse) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery query = session.createNativeQuery("INSERT INTO Medicine_Warehouse (serial, warehouse, status) " +
                " VALUES (:serial, :warehouse, :status)")
                .setParameter("serial", medicine_warehouse.getSerial())
                .setParameter("warehouse", medicine_warehouse.getWarehouse())
                .setParameter("status", medicine_warehouse.getStatus());

        query.executeUpdate();

        return true;
    }

    @Override
    public int getPharmacyIdWithGivenMedicineSerial(int serial) {
        String query = " SELECT MW.warehouse FROM Medicine M JOIN Medicine_Warehouse MW ON M.serial=MW.serial " +
                " WHERE M.utilized=0 and MW.status=1 and MW.serial = :serial ";

        int pharmacyId = (int) sessionFactory.getCurrentSession()
                .createQuery(query)
                .setParameter("serial", serial)
                .getSingleResult();

        return pharmacyId;
    }
}
