package pharmacy.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pharmacy.dao.RaportDao;
import pharmacy.model.DateParser;
import pharmacy.model.MedicineToRaport;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konrad on 23.11.2017.
 */
@Repository
public class RaportDaoImpl implements RaportDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<MedicineToRaport> getMedicineToRaport(int pharmacyId, String currentDate) {
        Session session = sessionFactory.getCurrentSession();
        String lastYear = DateParser.getDateWithShiftFromFirstDayOfMonth_withStartDate(currentDate, -12);

        NativeQuery<Object[]> query = session.createNativeQuery("SELECT T.id, T.name, T.kind, T.dose, T.doseUnit, (SELECT SUBSTRING(M.soldDate, 3, 2)) AS Year, (SELECT SUBSTRING(M.soldDate, 5, 2)) AS Month, COUNT(*) AS Count " +
                " FROM (Type T JOIN Medicine M ON T.id = M.type) JOIN Medicine_Warehouse MW ON M.serial = MW.serial " +
                " WHERE MW.status = 1 and M.utilized = 2 and MW.warehouse = :warehouse and M.soldDate >=  :lastYear" +
                " GROUP By Year, Month")
                .setParameter("warehouse", pharmacyId)
                .setParameter("lastYear", lastYear);

        List<Object[]> rows = query.list();

        System.out.println(rows.size());

        List<MedicineToRaport> resultList = new ArrayList<>();
        System.out.println("RAPORTY");
        for (Object[] current : rows) {
            System.out.println("Leki: " + current[0] + " " + current[1] + " " + current[2] + " " +
                    current[3] + " " + current[4] + " " + current[5] + " " + current[6] + " " + current[7]);

            if (current[3] == null)
                current[3] = 0;

            resultList.add(new MedicineToRaport(
                    (int) current[0],
                    (String) current[1],
                    (String) current[2],
                    (int) current[3],
                    (String) current[4],
                    (String) current[5],
                    (String) current[6],
                    ((BigInteger) current[7]).intValue()));
        }

        return resultList;
    }
}
