package pharmacy.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pharmacy.dao.RequestDao;
import pharmacy.entity.Request;
import pharmacy.model.RequestToGivenPharmacy;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RequestDaoImpl implements RequestDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Request> getRequests() {
        List requestsList = sessionFactory.getCurrentSession()
                .createQuery("select r from Request r").list();

        System.out.println("Rozmiar: " + requestsList.size());

        return requestsList;
    }

    @Override
    public List<RequestToGivenPharmacy> getRequests(int pharmacyId) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery query1 = session.createNativeQuery("SELECT R.id AS R_ID, T.id, T.name, T.kind, T.dose, T.doseUnit, T.capacity, W.name AS PN, R.warehouse_src, R.warehouse_dst, R.number, (SELECT COUNT(*) FROM Medicine M2 JOIN Medicine_Warehouse MW ON M2.serial=MW.serial WHERE MW.status = 1 AND M2.utilized = 0 AND M2.type=R.type AND MW.warehouse=:warehouse)" +
                " FROM (Request R JOIN Warehouse W ON R.warehouse_src=W.id) JOIN Type T ON R.type=T.id " +
                " WHERE R.warehouse_dst = :warehouse AND R.status = 1")
                .setParameter("warehouse", pharmacyId);
        List<Object[]> rows = query1.list();

        System.out.println(rows.size());

        List<RequestToGivenPharmacy> resultList = new ArrayList<>();

        for (Object[] current : rows) {
            System.out.println("Leki: " + pharmacyId + " " + current[0] + " " + current[1] + " " +
                    current[2] + " " + current[3] + " " +
                    current[4] + " " + current[5] + " " + current[6] + " " +
                    current[7] + " stan: " + current[8]);

            resultList.add(new RequestToGivenPharmacy((int) current[0],
                    (int) current[1],
                    (String) current[2],
                    (String) current[3],
                    (int) current[4],
                    (String) current[5],
                    (int) current[6],
                    (String) current[7],
                    (int) current[8],
                    (int) current[9],
                    "Wysoki",
                    (int) current[10],
                    ((BigInteger) current[11]).intValue()));
        }

        return resultList;
    }

    @Override
    public List<RequestToGivenPharmacy> getRequestsFrom(int pharmacyId) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery query1 = session.createNativeQuery("SELECT R.id AS R_ID, T.id, T.name, T.kind, T.dose, T.doseUnit, T.capacity, W.name AS PN, R.warehouse_src, R.warehouse_dst, R.number, (SELECT COUNT(*) FROM Medicine M2 JOIN Medicine_Warehouse MW ON M2.serial=MW.serial WHERE MW.status = 1 AND M2.utilized = 0 AND M2.type=R.type AND MW.warehouse=:warehouse), R.status" +
                " FROM (Request R JOIN Warehouse W ON R.warehouse_dst=W.id) JOIN Type T ON R.type=T.id " +
                " WHERE R.warehouse_src = :warehouse")
                .setParameter("warehouse", pharmacyId);
        List<Object[]> rows = query1.list();

        List<RequestToGivenPharmacy> resultList = new ArrayList<>();

        for (Object[] current : rows) {
            System.out.println("Leki: " + pharmacyId + " " + current[0] + " " + current[1] + " " +
                    current[2] + " " + current[3] + " " +
                    current[4] + " " + current[5] + " " + current[6] + " " +
                    current[7] + " " + current[8] + " " + current[9] + " " + current[10] + " " + current[11]);

            resultList.add(new RequestToGivenPharmacy((int) current[0],
                    (int) current[1],
                    (String) current[2],
                    (String) current[3],
                    (int) current[4],
                    (String) current[5],
                    (int) current[6],
                    (String) current[7],
                    (int) current[8],
                    (int) current[9],
                    "Wysoki",
                    (int) current[10],
                    ((BigInteger) current[11]).intValue(),
                    (int) current[12]));
        }

        return resultList;
    }

    @Override
    public void updateRequest(int requestId, int state) {
        Session session = sessionFactory.getCurrentSession();
        NativeQuery query1 = session.createNativeQuery("UPDATE Request" +
                " SET status = :state " +
                " WHERE id = :requestId")
                .setParameter("requestId", requestId)
                .setParameter("state", state);
        System.out.println("Request id is: " + requestId);
        query1.executeUpdate();
    }

    @Override
    public boolean addRequest(Request request) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery query = session.createNativeQuery("INSERT INTO Request (type, warehouse_src, warehouse_dst, number, status) " +
                " VALUES (:type, :warehouse_src, :warehouse_dst, :number, :status) ")
                .setParameter("type", request.getType())
                .setParameter("warehouse_src", request.getWarehouse_src())
                .setParameter("warehouse_dst", request.getWarehouse_dst())
                .setParameter("number", request.getNumber())
                .setParameter("status", request.getStatus());
        query.executeUpdate();

        return true;
    }
}
