package pharmacy.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pharmacy.dao.TimeDao;
import pharmacy.entity.Time;

@Repository
public class TimeDaoImpl implements TimeDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean setDate(String date) {
        Session session = sessionFactory.getCurrentSession();

        String queryString = " UPDATE Time SET date = :date WHERE id = :id ";
        int result = session.createNativeQuery(queryString)
                .setParameter("date", date)
                .setParameter("id", 1)
                .executeUpdate();

        return result == 1;
    }

    @Override
    public boolean setTime(String time) {
        Session session = sessionFactory.getCurrentSession();

        String queryString = " UPDATE Time SET time = :time WHERE id = :id ";
        int result = session.createNativeQuery(queryString)
                .setParameter("time", time)
                .setParameter("id", 1)
                .executeUpdate();

        return result == 1;
    }

    @Override
    public Time getTimeObj() {
        Time time = (Time) sessionFactory.getCurrentSession()
                .createQuery("select ti from Time ti where ti.id= :id ")
                .setParameter("id", 1)
                .getSingleResult();

        return time;
    }
}
