package pharmacy.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pharmacy.dao.TransferDao;
import pharmacy.entity.Transfer;
import pharmacy.model.TransferToDestination;
import pharmacy.model.TransferToSource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konrad on 23.11.2017.
 */
@Repository
public class TransferDaolmpl implements TransferDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public int getMaxId() {
        String query = " SELECT MAX(id) FROM Transfer ";

        int maxId = (int) sessionFactory.getCurrentSession()
                .createQuery(query)
                .getSingleResult();

        return maxId;
    }

    @Override
    public boolean addTransfer(Transfer transfer) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery query = session.createNativeQuery("INSERT INTO Transfer (source, destination, startDate, endDate, " +
                " startTime, endTime, serial ) " +
                " VALUES (:source, :destination, :startDate, :endDate, :startTime, :endTime, :serial )")
                .setParameter("source", transfer.getSource())
                .setParameter("destination", transfer.getDestination())
                .setParameter("startDate", transfer.getStartDate())
                .setParameter("endDate", transfer.getEndDate())
                .setParameter("startTime", transfer.getStartTime())
                .setParameter("endTime", transfer.getEndTime())
                .setParameter("serial", transfer.getSerial());

        query.executeUpdate();

        return true;
    }

    @Override
    public List<Transfer> getTransfers() {
        List transfersList = sessionFactory.getCurrentSession()
                .createQuery("select t from Transfer t").list();

        System.out.println("Rozmiar: " + transfersList.size());

        return transfersList;
    }

    @Override
    public List<TransferToSource> getTransferToSource(int pharmacyId, String currentDate, String currentTime) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery query = session.createNativeQuery("SELECT Tr.id, Tr.serial, T.name, Tr.endDate, Tr.endTime, W.name AS SOURCE " +
                " From (((Transfer Tr JOIN Warehouse W ON Tr.source = W.id) JOIN Medicine M ON Tr.serial = M.serial) JOIN Medicine_Warehouse MW ON M.serial = MW.serial) JOIN Type T ON M.type = T.id " +
                " WHERE M.utilized = 0 and MW.status = 1 and Tr.destination = :warehouse and (Tr.endDate > :currentDate or (Tr.endDate = :currentDate and Tr.endTime > :currentTime)) ")
                .setParameter("warehouse", pharmacyId)
                .setParameter("currentDate", currentDate)
                .setParameter("currentTime", currentTime);

        List<Object[]> rows = query.list();

        System.out.println(rows.size());

        List<TransferToSource> resultList = new ArrayList<>();

        for (Object[] current : rows) {
            System.out.println("Leki: " + pharmacyId + " " + current[0] + " " + current[1] + " " +
                    current[2] + " " + current[3] + " " + current[4] + " " + current[5]);

            resultList.add(new TransferToSource((int) current[0],
                    (int) current[1],
                    (String) current[2],
                    (String) current[3],
                    (String) current[4],
                    (String) current[5]));
        }
        return resultList;
    }

    @Override
    public List<TransferToDestination> getTransferToDestination(int pharmacyId, String currentDate, String currentTime) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery query = session.createNativeQuery("SELECT Tr.id, Tr.serial, T.name, Tr.startDate, Tr.startTime, W.name AS DESTINATION " +
                " From (((Transfer Tr JOIN Warehouse W ON Tr.destination = W.id) JOIN Medicine M ON Tr.serial = M.serial) JOIN Medicine_Warehouse MW ON M.serial = MW.serial) JOIN Type T ON M.type = T.id " +
                " WHERE W.warehouse = 0 and M.utilized = 0 and MW.status = 1 and Tr.source = :warehouse and (Tr.endDate > :currentDate or (Tr.endDate = :currentDate and Tr.endTime > :currentTime)) ")
                .setParameter("warehouse", pharmacyId)
                .setParameter("currentDate", currentDate)
                .setParameter("currentTime", currentTime);

        List<Object[]> rows = query.list();

        System.out.println(rows.size());

        List<TransferToDestination> resultList = new ArrayList<>();

        for (Object[] current : rows) {
            System.out.println("Leki: " + pharmacyId + " " + current[0] + " " + current[1] + " " +
                    current[2] + " " + current[3] + " " + current[4] + " " + current[5]);

            resultList.add(new TransferToDestination((int) current[0],
                    (int) current[1],
                    (String) current[2],
                    (String) current[3],
                    (String) current[4],
                    (String) current[5]));
        }
        return resultList;
    }

    @Override
    public void newTransfer(int serial, int pharmacy_dst, int pharmacy_src, String[] startDate, String[] endDate) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery query = session.createNativeQuery("INSERT INTO Transfer (source, destination, startDate, endDate, startTime, endTime, serial) " +
                " VALUES (:pharmacy_src, :pharmacy_dst, :startDate, :endDate, :startTime, :endTime, :serial)")
                .setParameter("pharmacy_src", pharmacy_src)
                .setParameter("pharmacy_dst", pharmacy_dst)
                .setParameter("serial", serial)
                .setParameter("startDate", startDate[0])
                .setParameter("endDate", endDate[0])
                .setParameter("startTime", startDate[1])
                .setParameter("endTime", endDate[1]);
        query.executeUpdate();
    }
}
