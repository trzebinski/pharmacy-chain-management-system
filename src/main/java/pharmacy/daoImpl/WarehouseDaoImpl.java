package pharmacy.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pharmacy.dao.WarehouseDao;
import pharmacy.entity.Warehouse;
import pharmacy.model.DateParser;
import pharmacy.model.MedicineStateInGivenPharmacy;
import pharmacy.model.MedicineWithShortBestBeforeDate;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class WarehouseDaoImpl implements WarehouseDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Warehouse> getPharmacies() {
        List pharmaciesList = sessionFactory.getCurrentSession()
                .createQuery("select wh from Warehouse wh where wh.warehouse= :warehouse")
                .setParameter("warehouse", 0).list();

        System.out.println("Rozmiar: " + pharmaciesList.size());

        return pharmaciesList;
    }

    @Override
    public Warehouse getPharmacyByName(String name) {
        Warehouse pharmacy = (Warehouse) sessionFactory.getCurrentSession()
                .createQuery("select wh from Warehouse wh where wh.warehouse= :warehouse and wh.name= :name ")
                .setParameter("warehouse", 0)
                .setParameter("name", name)
                .getSingleResult();

        return pharmacy;
    }

    @Override
    public Warehouse getPharmacyById(int id) {
        Warehouse pharmacy = (Warehouse) sessionFactory.getCurrentSession()
                .createQuery("select wh from Warehouse wh where wh.warehouse= :warehouse and wh.id= :id ")
                .setParameter("warehouse", 0)
                .setParameter("id", id)
                .getSingleResult();

        return pharmacy;
    }

    @Override
    public List<MedicineStateInGivenPharmacy> medicinesInPharmacyAndDemandedState(int pharmacyId) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery<Object[]> query = session.createNativeQuery(" SELECT T.id, T.name, T.kind, T.dose, T.doseUnit, T.capacity, T.price, (SELECT COUNT(*) FROM (Medicine M JOIN Medicine_Warehouse MW ON M.serial = MW.serial) " +
                " LEFT JOIN Transfer TR ON TR.serial=M.serial WHERE MW.warehouse=:warehouse AND M.type = T.id AND MW.status = 1 AND M.utilized = 0 and (TR.endDate IS NULL   OR   TR.endDate < (SELECT date FROM Time)   OR  (TR.endDate = (SELECT date FROM Time) and TR.endTime <= (SELECT time FROM Time)) )) AS Count, DS.number " +
                " FROM Type T JOIN DemandedState DS ON T.id=DS.type " +
                " WHERE DS.warehouse = :warehouse" +
                " ORDER BY Count")
                .setParameter("warehouse", pharmacyId);

        List<Object[]> rows = query.list();

        System.out.println("LICZBA LEKOW DO UZUPELNIENIA " + rows.size());

        List<MedicineStateInGivenPharmacy> resultList = new ArrayList<>();
        for (Object[] current : rows) {
            System.out.println("Leki: " + pharmacyId + " " + current[7] + " " + current[0] + " " +
                    current[1] + " " + current[2] + " " +
                    current[3] + " " + current[4] + " " + current[5] + " " + Math.round((float) current[6] * 100.0) / 100.0
                    + " " + current[8]);

            if (current[3] == null)
                current[3] = 0;

            resultList.add(new MedicineStateInGivenPharmacy(pharmacyId,
                    ((BigInteger) current[7]).intValue(),
                    (int) current[0],
                    (String) current[1],
                    (String) current[2],
                    (int) current[3],
                    (String) current[4],
                    (int) current[5],
                    Math.round((float) current[6] * 100.0) / 100.0,
                    (int) current[8]));
        }

        return resultList;
    }

    @Override
    public List<MedicineWithShortBestBeforeDate> medicinesWithShortBestBeforeDateInPharmacy(int pharmacyId, String currentDate) {
        Session session = sessionFactory.getCurrentSession();

        currentDate = DateParser.getDateWithShift_withStartDate(currentDate, 3);

        NativeQuery<Object[]> query = session.createNativeQuery("SELECT T.id, T.name, M.serial, T.dose, T.doseUnit, T.kind, T.capacity, M.expiryDate " +
                " FROM ((Medicine_Warehouse MW JOIN Medicine M ON MW.serial=M.serial) JOIN Type T ON M.type=T.id) LEFT JOIN Transfer TR ON TR.serial=M.serial " +
                " WHERE M.expiryDate < :currentDate and MW.status = 1 and M.utilized = 0 and MW.warehouse = :warehouse and (TR.endDate IS NULL   OR   TR.endDate < (SELECT date FROM Time)   OR  (TR.endDate = (SELECT date FROM Time) and TR.endTime <= (SELECT time FROM Time)) )" +
                " ORDER BY M.expiryDate")
                .setParameter("warehouse", pharmacyId)
                .setParameter("currentDate", currentDate);

        List<Object[]> rows = query.list();

        List<MedicineWithShortBestBeforeDate> resultList = new ArrayList<>();
        for (Object[] current : rows) {
            resultList.add(new MedicineWithShortBestBeforeDate((int) current[0],
                    (String) current[1],
                    (int) current[2],
                    (int) current[3],
                    (String) current[4],
                    (String) current[5],
                    (int) current[6],
                    (String) current[7]));

        }
        return resultList;
    }

    @Override
    public List<MedicineStateInGivenPharmacy> medicinesInPharmacy(int pharmacyId) {
        Session session = sessionFactory.getCurrentSession();

        NativeQuery<Object[]> query = session.createNativeQuery("SELECT T.id, T.name, T.kind, T.dose, T.doseUnit, " +
                " T.capacity, T.price, COUNT(T.id), DS.number " +
                " FROM (((Medicine_Warehouse MW JOIN Medicine M ON MW.serial=M.serial) JOIN Type T ON M.type=T.id) " +
                " JOIN DemandedState DS ON T.id=DS.type) " +
                " LEFT JOIN Transfer TR ON TR.serial=M.serial " +
                " WHERE MW.status = 1 " +
                " and M.utilized = 0 " +
                " and MW.warehouse = :warehouse " +
                " and DS.warehouse = :warehouse " +
                " and (TR.endDate IS NULL   OR   TR.endDate < (SELECT date FROM Time)   OR  (TR.endDate = (SELECT date FROM Time) and TR.endTime <= (SELECT time FROM Time)) ) " +
                " GROUP BY T.id ")
                .setParameter("warehouse", pharmacyId);

        List<Object[]> rows = query.list();


        List<MedicineStateInGivenPharmacy> resultList = new ArrayList<>();
        for (Object[] current : rows) {

            resultList.add(new MedicineStateInGivenPharmacy(pharmacyId,
                    ((BigInteger) current[7]).intValue(),
                    (int) current[0],
                    (String) current[1],
                    (String) current[2],
                    (int) current[3],
                    (String) current[4],
                    (int) current[5],
                    Math.round((float) current[6] * 100.0) / 100.0,
                    (int) current[8]
            ));
        }

        return resultList;
    }

    @Override
    public List<MedicineStateInGivenPharmacy> medicinesInPharmacy(int pharmacyId, String drugName, String kind, String order) {
        Session session = sessionFactory.getCurrentSession();

        String queryString = "SELECT T.id, T.name, T.kind, T.dose, T.doseUnit, T.capacity, T.price, " +
                "COUNT(T.id) AS drugCount, :drugName AS d, :kind as k" +
                " FROM ((Medicine_Warehouse MW JOIN Medicine M ON MW.serial=M.serial) JOIN Type T ON M.type=T.id) " +
                " LEFT JOIN Transfer TR ON TR.serial=M.serial " +
                " WHERE MW.status = 1 " +
                " and M.utilized = 0 " +
                " and MW.warehouse = :warehouse " +
                " and (TR.endDate IS NULL   OR   TR.endDate < (SELECT date FROM Time)   OR  (TR.endDate = (SELECT date FROM Time) and TR.endTime <= (SELECT time FROM Time)) ) ";

        if (!drugName.equals(""))
            queryString += " and T.name = :drugName ";

        if (!kind.equals(""))
            queryString += " and T.kind = :kind ";

        queryString += " GROUP BY T.id ORDER BY ";
        queryString += getKind(order);

        NativeQuery<Object[]> query = session.createNativeQuery(queryString)
                .setParameter("warehouse", pharmacyId)
                .setParameter("kind", kind.equals("") ? "All" : kind)
                .setParameter("drugName", drugName.equals("") ? "All" : drugName);


        List<Object[]> rows = query.list();

        List<MedicineStateInGivenPharmacy> resultList = new ArrayList<>();
        for (Object[] current : rows) {
            resultList.add(new MedicineStateInGivenPharmacy(pharmacyId,
                    ((BigInteger) current[7]).intValue(),
                    (int) current[0],
                    (String) current[1],
                    (String) current[2],
                    (int) current[3],
                    (String) current[4],
                    (int) current[5],
                    Math.round((float) current[6] * 100.0) / 100.0
            ));
        }

        return resultList;
    }


    private String getKind(String order) {
        switch (order) {
            case "drugName":
                return " T.name ";
            case "count":
                return " drugCount ";
            case "capacity":
                return " T.capacity ";
            case "price":
                return " T.price ";
        }
        return "";
    }
}
