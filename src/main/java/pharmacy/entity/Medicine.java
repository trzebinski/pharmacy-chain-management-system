package pharmacy.entity;

import javax.persistence.*;

@Entity
@Table(name = "Medicine")
public class Medicine {
    @Id
    @GeneratedValue
    @Column(name = "serial")
    private int serial;

    @Column(name = "expiryDate")
    private String expiryDate;

    @Column(name = "type")
    private int type;

    @Column(name = "utilized")
    private int utilized;

    @Column(name = "soldDate")
    private String soldDate;

    public Medicine(int serial, String expiryDate, int type, int utilized, String soldDate) {
        this.serial = serial;
        this.expiryDate = expiryDate;
        this.type = type;
        this.utilized = utilized;
        this.soldDate = soldDate;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUtilized() {
        return utilized;
    }

    public void setUtilized(int utilized) {
        this.utilized = utilized;
    }

    public String getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(String soldDate) {
        this.soldDate = soldDate;
    }
}
