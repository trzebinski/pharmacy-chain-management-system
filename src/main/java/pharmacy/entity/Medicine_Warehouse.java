package pharmacy.entity;

import javax.persistence.*;

@Entity
@Table(name = "Medicine_Warehouse")
public class Medicine_Warehouse {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "serial")
    private int serial;

    @Column(name = "warehouse")
    private int warehouse;

    @Column(name = "status")
    private int status;

    public Medicine_Warehouse(int serial, int warehouse, int status) {
        this.serial = serial;
        this.warehouse = warehouse;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public int getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(int warehouse) {
        this.warehouse = warehouse;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
