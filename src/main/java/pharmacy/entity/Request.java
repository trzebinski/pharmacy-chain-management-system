package pharmacy.entity;

import javax.persistence.*;

@Entity
@Table(name = "Request")
public class Request {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "type")
    private int type;

    @Column(name = "warehouse_src")
    private int warehouse_src;

    @Column(name = "warehouse_dst")
    private int warehouse_dst;

    @Column(name = "number")
    private int number;

    @Column(name = "status")
    private int status;

    public Request(int type, int warehouse_src, int warehouse_dst, int number, int status) {
        this.type = type;
        this.warehouse_src = warehouse_src;
        this.warehouse_dst = warehouse_dst;
        this.number = number;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getWarehouse_src() {
        return warehouse_src;
    }

    public void setWarehouse_src(int warehouse_src) {
        this.warehouse_src = warehouse_src;
    }

    public int getWarehouse_dst() {
        return warehouse_dst;
    }

    public void setWarehouse_dst(int warehouse_dst) {
        this.warehouse_dst = warehouse_dst;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
