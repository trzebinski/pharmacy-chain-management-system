package pharmacy.entity;

import javax.persistence.*;

@Entity
@Table(name = "Time")
public class Time {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "date")
    private String date;

    @Column(name = "time")
    private String time;

    public Time() {
    }

    public Time(String date, String time) {
        this.date = date;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
