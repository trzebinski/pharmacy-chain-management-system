package pharmacy.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateParser {

    // YYYYMMDD -> YYYY-MM-DD
    public static String enlargeDate(String shortDate) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(shortDate.substring(0, 4));
        stringBuilder.append("-");
        stringBuilder.append(shortDate.substring(4, 6));
        stringBuilder.append("-");
        stringBuilder.append(shortDate.substring(6, 8));

        return stringBuilder.toString();
    }

    public static String shortDate(String enlargeDate) {
        return enlargeDate.replace("-", "");
    }

    // HHMM -> HH:MM
    public static String enlargeTime(String shortTime) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(shortTime.substring(0, 2));
        stringBuilder.append(":");
        stringBuilder.append(shortTime.substring(2, 4));

        return stringBuilder.toString();
    }

    // YYYYMMDD
    public static String getCurrentDate() {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        String currentDate = df.format(today);
        return currentDate;
    }

    // YYYYMMDD shift the current date by amount of months
    public static String getDateWithShift(int amount) {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, amount);
        Date today = cal.getTime();
        String currentDate = df.format(today);
        return currentDate;
    }


    // YYYYMMDD HHmm
    public static String[] getDateWithShiftInHours(int amount) {
        String[] date;
        DateFormat df = new SimpleDateFormat("yyyyMMdd HHmm");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, amount);
        Date today = cal.getTime();
        String currentDate = df.format(today);
        date = currentDate.split(" ");
        return date;
    }

    // YYYYMMDD HHmm
    public static String[] getDateWithShiftInHours_withStartDate(String startDate, String startTime, int amount) {
        String[] date;
        DateFormat df = new SimpleDateFormat("yyyyMMdd HHmm");
        Calendar cal = Calendar.getInstance();

        int year = Integer.parseInt(startDate.substring(0, 4));
        int month = (Integer.parseInt(startDate.substring(4, 6)) - 1);
        int day = Integer.parseInt(startDate.substring(6, 8));
        int hour = Integer.parseInt(startTime.substring(0, 2));
        int minute = Integer.parseInt(startTime.substring(2, 4));
        int second = Integer.parseInt(startTime.substring(4, 6));

        cal.set(year, month, day, hour, minute, second);

        cal.add(Calendar.HOUR, amount);
        Date today = cal.getTime();
        String currentDate = df.format(today);
        date = currentDate.split(" ");
        return date;
    }

    // YYYYMMDD HHmm
    public static String[] getDateWithShiftInMonths_withStartDate(String startDate, String startTime, int amount) {
        String[] date;
        DateFormat df = new SimpleDateFormat("yyyyMMdd HHmm");
        Calendar cal = Calendar.getInstance();

        int year = Integer.parseInt(startDate.substring(0, 4));
        int month = (Integer.parseInt(startDate.substring(4, 6)) - 1);
        int day = Integer.parseInt(startDate.substring(6, 8));
        int hour = Integer.parseInt(startTime.substring(0, 2));
        int minute = Integer.parseInt(startTime.substring(2, 4));
        int second = Integer.parseInt(startTime.substring(4, 6));

        cal.set(year, month, day, hour, minute, second);

        cal.add(Calendar.MONTH, amount);
        Date today = cal.getTime();
        String currentDate = df.format(today);
        date = currentDate.split(" ");
        return date;
    }

    // YYYYMMDD shift the current date by amount of months
    public static String getDateWithShift_withStartDate(String startDate, int amount) {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();

        int year = Integer.parseInt(startDate.substring(0, 4));
        int month = (Integer.parseInt(startDate.substring(4, 6)) - 1);
        int day = Integer.parseInt(startDate.substring(6, 8));
        cal.set(year, month, day);

        cal.add(Calendar.MONTH, amount);
        Date today = cal.getTime();
        String currentDate = df.format(today);
        return currentDate;
    }

    // YYYYMMDD shift the current date by amount of months, data start from 1st of month
    public static String getDateWithShiftFromFirstDayOfMonth_withStartDate(String startDate, int amount) {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();

        int year = Integer.parseInt(startDate.substring(0, 4));
        int month = (Integer.parseInt(startDate.substring(4, 6)) - 1);
        int day = Integer.parseInt(startDate.substring(6, 8));
        cal.set(year, month, day);

        cal.add(Calendar.MONTH, amount);
        Date today = cal.getTime();
        String currentDate = df.format(today);
        return currentDate;
    }

    public static int[] getDateToGraphWithShift_withStartDate(String startDate, int amount) {

        int year = Integer.parseInt(startDate.substring(2, 4));
        int month = (Integer.parseInt(startDate.substring(4, 6)) - 1);
        int day = Integer.parseInt(startDate.substring(6, 8));
        Calendar cal = Calendar.getInstance();

        cal.set(year, month, day);

        cal.add(Calendar.MONTH, amount);

        int toReturn[] = new int[2];
        toReturn[0] = cal.get(Calendar.MONTH);
        toReturn[1] = cal.get(Calendar.YEAR);
        return toReturn;
    }
}
