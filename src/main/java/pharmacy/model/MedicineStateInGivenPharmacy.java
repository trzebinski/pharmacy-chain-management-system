package pharmacy.model;

import java.util.List;

public class MedicineStateInGivenPharmacy {

    private int pharmacyId;
    private int count;

    private int id;
    private String name;
    private String kind;
    private int dose;
    private String doseUnit;
    private int capacity;
    private double price;

    private int demandedCount;
    private int deficite;

    public MedicineStateInGivenPharmacy() {
    }

    public MedicineStateInGivenPharmacy(int pharmacyId, int count, int id, String name, String kind, int dose, String doseUnit, int capacity, double price) {
        this.pharmacyId = pharmacyId;
        this.count = count;
        this.id = id;
        this.name = name;
        this.kind = kind;
        this.dose = dose;
        this.doseUnit = doseUnit;
        this.capacity = capacity;
        this.price = price;
    }

    public MedicineStateInGivenPharmacy(int pharmacyId, int count, int id, String name, String kind, int dose, String doseUnit, int capacity, double price, int demandedCount) {
        this.pharmacyId = pharmacyId;
        this.count = count;
        this.id = id;
        this.name = name;
        this.kind = kind;
        this.dose = dose;
        this.doseUnit = doseUnit;
        this.capacity = capacity;
        this.price = price;
        this.demandedCount = demandedCount;
        this.deficite = demandedCount - count > 0 ? demandedCount - count : 0;
    }

    public MedicineStateInGivenPharmacy(int pharmacyId, int count, int id, String name, String kind, int dose, String doseUnit, int capacity, int demandedCount) {
        this.pharmacyId = pharmacyId;
        this.count = count;
        this.id = id;
        this.name = name;
        this.kind = kind;
        this.dose = dose;
        this.doseUnit = doseUnit;
        this.capacity = capacity;
        this.demandedCount = demandedCount;
    }

    public int getPharmacyId() {
        return pharmacyId;
    }

    public void setPharmacyId(int pharmacyId) {
        this.pharmacyId = pharmacyId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getDose() {
        return dose;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDemandedCount() {
        return demandedCount;
    }

    public void setDemandedCount(int demandedCount) {
        this.demandedCount = demandedCount;
    }

    public int getDeficite() {
        return deficite;
    }

    public void setDeficite(int deficite) {
        this.deficite = deficite;
    }

    @Override
    public String toString() {
        return "MedicineStateInGivenPharmacy{" +
                "pharmacyId=" + pharmacyId +
                ", count=" + count +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", kind='" + kind + '\'' +
                ", dose=" + dose +
                ", doseUnit='" + doseUnit + '\'' +
                ", capacity=" + capacity +
                ", price=" + price +
                '}';
    }

    public static boolean updateMedicine(List<MedicineStateInGivenPharmacy> list, int typeId, boolean canDelete) {
        int position = 0;
        boolean toDelete = false;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == typeId) {
                list.get(i).setCount(list.get(i).getCount() - 1);
                list.get(i).setDeficite(list.get(i).getDeficite() + 1);
                if (list.get(i).getCount() == 0) {
                    toDelete = true;
                    position = i;
                }
            }
        }

        if (toDelete && canDelete)
            list.remove(position);
        return toDelete;
    }
}
