package pharmacy.model;

/**
 * Created by Konrad on 03.01.2018.
 */
public class MedicineToRaport {
    private int id;
    private String name;
    private String kind;
    private int dose;
    private String doseUnit;
    private String soldYear;
    private String soldMonth;
    private int countMonth;

    public MedicineToRaport(int id, String name, String kind, int dose, String doseUnit, String soldYear, String soldMonth, int countMonth) {
        this.id = id;
        this.name = name;
        this.kind = kind;
        this.dose = dose;
        this.doseUnit = doseUnit;
        this.soldYear = soldYear;
        this.soldMonth = soldMonth;
        this.countMonth = countMonth;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getDose() {
        return dose;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    public String getSoldYear() {
        return soldYear;
    }

    public void setSoldYear(String soldYear) {
        this.soldYear = soldYear;
    }

    public String getSoldMonth() {
        return soldMonth;
    }

    public void setSoldMonth(String soldMonth) {
        this.soldMonth = soldMonth;
    }

    public int getCountMonth() {
        return countMonth;
    }

    public void setCountMonth(int countMonth) {
        this.countMonth = countMonth;
    }
}
