package pharmacy.model;

import java.util.List;

public class MedicineWithShortBestBeforeDate {
    private int id;
    private String name;
    private int serial;
    private int dose;
    private String doseUnit;
    private String kind;
    private int capacity;
    private String expirationDate;

    public MedicineWithShortBestBeforeDate(int id, String name, int serial, int dose, String doseUnit, String kind, int capacity, String expirationDate) {
        this.id = id;
        this.name = name;
        this.serial = serial;
        this.dose = dose;
        this.doseUnit = doseUnit;
        this.kind = kind;
        this.capacity = capacity;
        this.expirationDate = DateParser.enlargeDate(expirationDate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public int getDose() {
        return dose;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public static boolean updateMedicine(List<MedicineWithShortBestBeforeDate> list, int serial) {
        int position = 0;
        boolean toDelete = false;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getSerial() == serial) {
                toDelete = true;
                position = i;
            }
        }

        if (toDelete)
            list.remove(position);
        return toDelete;
    }
}
