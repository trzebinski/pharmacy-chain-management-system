package pharmacy.model;

import pharmacy.model.filters.ReportFilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static pharmacy.model.DateParser.getDateToGraphWithShift_withStartDate;

public class Operations {

    public static int getNumberOfMedicines(List<List<MedicineStateInGivenPharmacy>> medicineStatePharmacies) {
        int sum = 0;

        for (List<MedicineStateInGivenPharmacy> current : medicineStatePharmacies) {
            if (current != null)
                sum += current.size();
        }

        return sum;
    }

    public static List<String> getListOfLast12Months(String currentDate) {
        List<String> list = new ArrayList<>();
        for (int i = 12; i >= 0; i--) {
            int result[] = getDateToGraphWithShift_withStartDate(currentDate, -i);

            String monthName = getNameOfMonth(result[0] + 1);

            list.add(monthName + "'" + result[1]);
        }
        return list;
    }

    public static String getNameOfMonth(int month) {
        String monthName = "";
        switch (month) {
            case 1:
                monthName = "Styczeń";
                break;
            case 2:
                monthName = "Luty";
                break;
            case 3:
                monthName = "Marzec";
                break;
            case 4:
                monthName = "Kwiecień";
                break;
            case 5:
                monthName = "Maj";
                break;
            case 6:
                monthName = "Czerwiec";
                break;
            case 7:
                monthName = "Lipiec";
                break;
            case 8:
                monthName = "Sierpnień";
                break;
            case 9:
                monthName = "Wrzesień";
                break;
            case 10:
                monthName = "Październik";
                break;
            case 11:
                monthName = "Listopad";
                break;
            case 12:
                monthName = "Grudzień";
                break;
        }

        return monthName;
    }

    public static List<Integer> prepareDataToGraph(List<String> monthLists, List<MedicineToRaport> medicineList, ReportFilter filter) {
        List<Integer> resultList = new ArrayList<>(Collections.nCopies(monthLists.size(), 0));

        for (int i = 0; i < monthLists.size(); i++) {
            for (MedicineToRaport medicine : medicineList) {
                if (filter == null) {
                    System.out.println("MIESIĄC SPRZEDAŻY: " + medicine.getSoldMonth());
                    if (!medicine.getSoldMonth().equals("") && Operations.getNameOfMonth(Integer.valueOf(medicine.getSoldMonth())).equals(monthLists.get(i).substring(0, monthLists.get(i).length() - 3))
                            && !medicine.getSoldYear().equals("") && monthLists.get(i).substring(monthLists.get(i).length() - 2).equals(medicine.getSoldYear())) {
                        resultList.set(i, resultList.get(i) + medicine.getCountMonth());
                    }
                } else {
                    if (!medicine.getSoldMonth().equals("") && Operations.getNameOfMonth(Integer.valueOf(medicine.getSoldMonth())).equals(monthLists.get(i).substring(0, monthLists.get(i).length() - 3))
                            && !medicine.getSoldYear().equals("") && monthLists.get(i).substring(monthLists.get(i).length() - 2).equals(medicine.getSoldYear())
                            && (!filter.getDrugName().equals("") ? medicine.getName().toLowerCase().equals(filter.getDrugName().toLowerCase()) : true)
                            && (filter.getKind().equals("") ? true : filter.getKind().toLowerCase().equals(medicine.getKind()))) {
                        resultList.set(i, resultList.get(i) + medicine.getCountMonth());
                    }
                }
            }
        }

        return resultList;
    }
}
