package pharmacy.model;

import java.util.ArrayList;
import java.util.List;

public class Pagination {
    public static List<MedicineStateInGivenPharmacy> divideMedicineState(List<MedicineStateInGivenPharmacy> list, int split, int page) {
        List<MedicineStateInGivenPharmacy> currentData = new ArrayList<>();
        page -= 1;

        if (page * split + split < list.size()) {
            currentData = list.subList(page * split, page * split + split);
        } else {
            currentData = list.subList(page * split, list.size());
        }

        return currentData;
    }

    public static List<RequestToGivenPharmacy> divideRequestTo(List<RequestToGivenPharmacy> list, int split, int page) {
        List<RequestToGivenPharmacy> currentData = new ArrayList<>();
        page -= 1;

        if (page * split + split < list.size()) {
            currentData = list.subList(page * split, page * split + split);
        } else {
            currentData = list.subList(page * split, list.size());
        }

        return currentData;
    }

    public static List<MedicineWithShortBestBeforeDate> divideMedicineWithShortBestBeforeDate(List<MedicineWithShortBestBeforeDate> list, int split, int page) {
        List<MedicineWithShortBestBeforeDate> currentData = new ArrayList<>();
        page -= 1;

        if (page * split + split < list.size()) {
            currentData = list.subList(page * split, page * split + split);
        } else {
            currentData = list.subList(page * split, list.size());
        }

        return currentData;
    }

    public static List<TransferToSource> divideTransferToSource(List<TransferToSource> list, int split, int page) {
        List<TransferToSource> currentData = new ArrayList<>();
        page -= 1;

        if (page * split + split < list.size()) {
            currentData = list.subList(page * split, page * split + split);
        } else {
            currentData = list.subList(page * split, list.size());
        }

        return currentData;
    }

    public static List<TransferToDestination> divideTransferToDestination(List<TransferToDestination> list, int split, int page) {
        List<TransferToDestination> currentData = new ArrayList<>();
        page -= 1;

        if (page * split + split < list.size()) {
            currentData = list.subList(page * split, page * split + split);
        } else {
            currentData = list.subList(page * split, list.size());
        }

        return currentData;
    }

    public static int getNumberOfPages(int size, int divide) {
        int pages = size / divide;
        if (size % divide > 0)
            pages++;

        return pages;
    }
}
