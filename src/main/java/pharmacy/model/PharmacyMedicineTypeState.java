package pharmacy.model;

public class PharmacyMedicineTypeState {

    private int pharmacyId;
    private String pharmacyName;
    private int numberOfMedicine;

    public PharmacyMedicineTypeState(int pharmacyId, String pharmacyName, int numberOfMedicine) {
        this.pharmacyId = pharmacyId;
        this.pharmacyName = pharmacyName;
        this.numberOfMedicine = numberOfMedicine;
    }

    public int getPharmacyId() {
        return pharmacyId;
    }

    public void setPharmacyId(int pharmacyId) {
        this.pharmacyId = pharmacyId;
    }

    public String getPharmacyName() {
        return pharmacyName;
    }

    public void setPharmacyName(String pharmacyName) {
        this.pharmacyName = pharmacyName;
    }

    public int getNumberOfMedicine() {
        return numberOfMedicine;
    }

    public void setNumberOfMedicine(int numberOfMedicine) {
        this.numberOfMedicine = numberOfMedicine;
    }

    @Override
    public String toString() {
        return "PharmacyMedicineTypeState{" +
                "pharmacyId=" + pharmacyId +
                ", pharmacyName='" + pharmacyName + '\'' +
                ", numberOfMedicine=" + numberOfMedicine +
                '}';
    }
}
