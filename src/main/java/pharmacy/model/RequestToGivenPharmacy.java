package pharmacy.model;

import java.util.List;

public class RequestToGivenPharmacy {
    private int id;
    private int requestId;
    private String name;
    private String kind;
    private int dose;
    private String doseUnit;
    private int capacity;
    private String pharmacy;
    private int pharmacySourceId;
    private int pharmacyDstId;
    private String priority;
    private int number;
    private int state;
    private int status;

    public int getDemandedState() {
        return demandedState;
    }

    public void setDemandedState(int demandedState) {
        this.demandedState = demandedState;
    }

    private int demandedState;

    private String info;

    public RequestToGivenPharmacy(int requestId, int id, String name, String kind, int dose, String doseUtin, int capacity, String pharmacy,
                                  int pharmacySourceId, int pharmacyDstId, String priority, int number, int state) {
        this.requestId = requestId;
        this.id = id;
        this.name = name;
        this.kind = kind;
        this.dose = dose;
        this.doseUnit = doseUtin;
        this.capacity = capacity;
        this.pharmacy = pharmacy;
        this.pharmacySourceId = pharmacySourceId;
        this.pharmacyDstId = pharmacyDstId;
        this.priority = priority;
        this.number = number;
        this.state = state;
    }

    public RequestToGivenPharmacy(int requestId, int id, String name, String kind, int dose, String doseUtin, int capacity, String pharmacy,
                                  int pharmacySourceId, int pharmacyDstId, String priority, int number, int state, int status) {
        this.requestId = requestId;
        this.id = id;
        this.name = name;
        this.kind = kind;
        this.dose = dose;
        this.doseUnit = doseUtin;
        this.capacity = capacity;
        this.pharmacy = pharmacy;
        this.pharmacySourceId = pharmacySourceId;
        this.pharmacyDstId = pharmacyDstId;
        this.priority = priority;
        this.number = number;
        this.state = state;
        this.status = status;
        this.info = setInfo(status);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRequestId() {
        return requestId;
    }

    public int getPharmacyDstId() {
        return pharmacyDstId;
    }

    public void setPharmacyDstId(int pharmacyDstId) {
        this.pharmacyDstId = pharmacyDstId;
    }

    public String setInfo(int state) {
        String info = "";
        switch (state) {
            case 0:
                info = "Zrealizowane";
                break;
            case 1:
                info = "W trakcie realizacji";
                break;
            case 2:
                info = "Odrzucone";
                break;
        }
        return info;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getDose() {
        return dose;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getPharmacy() {
        return pharmacy;
    }

    public void setPharmacy(String pharmacy) {
        this.pharmacy = pharmacy;
    }

    public int getPharmacySourceId() {
        return pharmacySourceId;
    }

    public void setPharmacySourceId(int pharmacySourceId) {
        this.pharmacySourceId = pharmacySourceId;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public static boolean updateRequests(List<RequestToGivenPharmacy> list, int requestId) {
        int position = 0;
        boolean toDelete = false;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getRequestId() == requestId) {
                position = i;
                toDelete = true;
            }
        }
        if (toDelete)
            list.remove(position);
        return toDelete;
    }
}
