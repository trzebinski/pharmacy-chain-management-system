package pharmacy.model;

/**
 * Created by Konrad on 06.12.2017.
 */
public class TransferToDestination {
    private int id;
    private int serial;
    private String name;
    private String startDate;
    private String startTime;
    private String destination;

    public TransferToDestination(int id, int serial, String name, String startDate, String startTime, String destination) {
        this.id = id;
        this.serial = serial;
        this.name = name;
        this.startDate = DateParser.enlargeDate(startDate);
        this.startTime = DateParser.enlargeTime(startTime);
        this.destination = destination;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
