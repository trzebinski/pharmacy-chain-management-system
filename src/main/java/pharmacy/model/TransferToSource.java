package pharmacy.model;

/**
 * Created by Konrad on 06.12.2017.
 */
public class TransferToSource {
    private int id;
    private int serial;
    private String name;
    private String endDate;
    private String endTime;
    private String source;

    public TransferToSource(int id, int serial, String name, String endDate, String endTime, String source) {
        this.id = id;
        this.serial = serial;
        this.name = name;
        this.endDate = DateParser.enlargeDate(endDate);
        this.endTime = DateParser.enlargeTime(endTime);
        this.source = source;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
