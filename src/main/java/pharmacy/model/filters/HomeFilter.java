package pharmacy.model.filters;

import org.springframework.stereotype.Component;

@Component
public class HomeFilter {

    private String drugName;
    private String kind;
    private String order;
    private int pharmacy;

    public HomeFilter() {
    }

    public HomeFilter(String drugName, String kind, String order, int pharmacy) {
        this.drugName = drugName;
        this.kind = kind;
        this.order = order;
        this.pharmacy = pharmacy;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getPharmacy() {
        return pharmacy;
    }

    public void setPharmacy(int pharmacy) {
        this.pharmacy = pharmacy;
    }

    @Override
    public String toString() {
        return "HomeFilter{" +
                "drugName='" + drugName + '\'' +
                ", kind=" + kind +
                ", order=" + order +
                ", pharmacy=" + pharmacy +
                '}';
    }
}
