package pharmacy.model.filters;

import org.springframework.stereotype.Component;

@Component
public class ReportFilter {

    private String drugName;
    private String kind;

    public ReportFilter() {
    }

    public ReportFilter(String drugName, String kind, int dose) {
        this.drugName = drugName;
        this.kind = kind;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }
}
