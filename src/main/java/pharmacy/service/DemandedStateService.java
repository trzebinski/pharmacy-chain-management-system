package pharmacy.service;

import pharmacy.model.MedicineStateInGivenPharmacy;

import java.util.List;

/**
 * Created by Konrad on 22.11.2017.
 */
public interface DemandedStateService {
    List<MedicineStateInGivenPharmacy> demandedState(int pharmacyId);

    List<MedicineStateInGivenPharmacy> demandedState(int pharmacyId, String drugName,
                                                     String kind, String order);

    boolean changeDemandedState(int pharmacyId, int typeId, int newDemandedState);
}
