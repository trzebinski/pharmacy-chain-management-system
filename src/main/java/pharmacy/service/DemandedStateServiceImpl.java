package pharmacy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pharmacy.dao.DemandedStateDao;
import pharmacy.model.MedicineStateInGivenPharmacy;

import java.util.List;

/**
 * Created by Konrad on 22.11.2017.
 */
@Service
public class DemandedStateServiceImpl implements DemandedStateService {

    @Autowired
    private DemandedStateDao demandedStateDao;

    @Transactional
    public List<MedicineStateInGivenPharmacy> demandedState(int pharmacyId) {
        return demandedStateDao.demandedState(pharmacyId);
    }

    @Transactional
    public List<MedicineStateInGivenPharmacy> demandedState(int pharmacyId, String drugName, String kind, String order) {
        return demandedStateDao.demandedState(pharmacyId, drugName, kind, order);
    }

    @Transactional
    public boolean changeDemandedState(int pharmacyId, int typeId, int newDemandedState) {
        return demandedStateDao.changeDemandedState(pharmacyId, typeId, newDemandedState);
    }
}
