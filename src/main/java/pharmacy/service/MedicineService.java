package pharmacy.service;

import pharmacy.entity.Medicine;
import pharmacy.model.PharmacyMedicineTypeState;

import java.util.List;

public interface MedicineService {
    int getMaxSerialNumber();

    boolean addMedicine(Medicine medicine);

    List<Medicine> getMedicines();

    List<Medicine> getMedicine(int pharmacyId, int typeId);

    List<PharmacyMedicineTypeState> getGroupedMedicines(int typeId, int currentPharmacyId);

    boolean utilizeMedicine(int serial);

    boolean soldMedicine(int serial);

    boolean transferMedicine(int serial, int oldPharmacy, int newPharmacy);
}
