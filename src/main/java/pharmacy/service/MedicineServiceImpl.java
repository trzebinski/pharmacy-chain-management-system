package pharmacy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pharmacy.dao.MedicineDao;
import pharmacy.entity.Medicine;
import pharmacy.model.PharmacyMedicineTypeState;

import java.util.List;

@Service
public class MedicineServiceImpl implements MedicineService {

    @Autowired
    private MedicineDao medicineDao;

    @Transactional
    public int getMaxSerialNumber() {
        return medicineDao.getMaxSerialNumber();
    }

    @Transactional
    public boolean addMedicine(Medicine medicine) {
        return medicineDao.addMedicine(medicine);
    }

    @Transactional
    public List<Medicine> getMedicines() {
        return medicineDao.getMedicines();
    }

    @Transactional
    public List<Medicine> getMedicine(int pharmacyId, int typeId) {
        return medicineDao.getMedicines(pharmacyId, typeId);
    }

    @Transactional
    public List<PharmacyMedicineTypeState> getGroupedMedicines(int typeId, int currentPharmacyId) {
        return medicineDao.getGroupedMedicines(typeId, currentPharmacyId);
    }

    @Transactional
    public boolean utilizeMedicine(int serial) {
        return medicineDao.utilizeMedicine(serial);
    }

    @Transactional
    public boolean soldMedicine(int serial) {
        return medicineDao.soldMedicine(serial);
    }

    @Transactional
    public boolean transferMedicine(int serial, int oldPharmacy, int newPharmacy) {
        return medicineDao.transferMedicine(serial, oldPharmacy, newPharmacy);
    }
}
