package pharmacy.service;

import pharmacy.entity.Medicine_Warehouse;

public interface Medicine_WarehouseService {
    boolean addMedicine_WarehouseDao(Medicine_Warehouse medicine_warehouse);

    int getPharmacyIdWithGivenMedicineSerial(int serial);

}
