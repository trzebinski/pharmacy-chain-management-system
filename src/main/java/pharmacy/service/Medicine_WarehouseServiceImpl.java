package pharmacy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pharmacy.dao.Medicine_WarehouseDao;
import pharmacy.entity.Medicine_Warehouse;

@Service
public class Medicine_WarehouseServiceImpl implements Medicine_WarehouseService {

    @Autowired
    private Medicine_WarehouseDao medicine_warehouseDao;

    @Transactional
    public boolean addMedicine_WarehouseDao(Medicine_Warehouse medicine_warehouse) {
        return medicine_warehouseDao.addMedicine_WarehouseDao(medicine_warehouse);
    }

    @Transactional
    public int getPharmacyIdWithGivenMedicineSerial(int serial) {
        return medicine_warehouseDao.getPharmacyIdWithGivenMedicineSerial(serial);
    }
}
