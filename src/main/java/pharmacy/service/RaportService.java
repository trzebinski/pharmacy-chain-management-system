package pharmacy.service;

import pharmacy.model.MedicineToRaport;

import java.util.List;

/**
 * Created by Konrad on 22.11.2017.
 */
public interface RaportService {
    List<MedicineToRaport> getMedicineToRaport(int pharmacyId, String currentDate);
}
