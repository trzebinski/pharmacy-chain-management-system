package pharmacy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pharmacy.dao.RaportDao;
import pharmacy.model.MedicineToRaport;

import java.util.List;

/**
 * Created by Konrad on 22.11.2017.
 */
@Service
public class RaportServiceImpl implements RaportService {

    @Autowired
    private RaportDao raportDao;

    @Transactional
    public List<MedicineToRaport> getMedicineToRaport(int pharmacyId, String currentDate) {
        return raportDao.getMedicineToRaport(pharmacyId, currentDate);
    }
}
