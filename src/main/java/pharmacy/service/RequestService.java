package pharmacy.service;

import pharmacy.entity.Request;
import pharmacy.model.RequestToGivenPharmacy;

import java.util.List;

public interface RequestService {
    List<Request> getRequests();

    List<RequestToGivenPharmacy> getRequests(int pharmacyId);

    List<RequestToGivenPharmacy> getRequestsFrom(int pharmacyId);

    void updateRequest(int requestId, int state);

    boolean addRequest(Request request);
}
