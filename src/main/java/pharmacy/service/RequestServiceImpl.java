package pharmacy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pharmacy.dao.RequestDao;
import pharmacy.entity.Request;
import pharmacy.model.RequestToGivenPharmacy;

import java.util.List;

@Service
public class RequestServiceImpl implements RequestService {

    @Autowired
    RequestDao requestDao;

    @Transactional
    public List<Request> getRequests() {
        return requestDao.getRequests();
    }

    @Transactional
    public List<RequestToGivenPharmacy> getRequests(int pharmacyId) {
        return requestDao.getRequests(pharmacyId);
    }

    @Transactional
    public List<RequestToGivenPharmacy> getRequestsFrom(int pharmacyId) {
        return requestDao.getRequestsFrom(pharmacyId);
    }

    @Transactional
    public void updateRequest(int requestId, int state) {
        requestDao.updateRequest(requestId, state);
    }

    @Transactional
    public boolean addRequest(Request request) {
        return requestDao.addRequest(request);
    }
}
