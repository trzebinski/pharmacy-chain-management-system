package pharmacy.service;

import pharmacy.entity.Time;

public interface TimeService {
    boolean setDate(String date);

    boolean setTime(String time);

    Time getTimeObj();
}
