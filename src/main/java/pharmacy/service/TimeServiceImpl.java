package pharmacy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pharmacy.dao.TimeDao;
import pharmacy.entity.Time;

@Service
public class TimeServiceImpl implements TimeService {

    @Autowired
    private TimeDao timeDao;

    @Transactional
    public boolean setDate(String date) {
        return timeDao.setDate(date);
    }

    @Transactional
    public boolean setTime(String time) {
        return timeDao.setTime(time);
    }

    @Transactional
    public Time getTimeObj() {
        return timeDao.getTimeObj();
    }
}
