package pharmacy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pharmacy.dao.TransferDao;
import pharmacy.entity.Transfer;
import pharmacy.model.TransferToDestination;
import pharmacy.model.TransferToSource;

import java.util.List;

/**
 * Created by Konrad on 23.11.2017.
 */
@Service
public class TransferServicelmpl implements TransferService {

    @Autowired
    private TransferDao transferDao;

    @Transactional
    public List<Transfer> getTransfers() {
        return transferDao.getTransfers();
    }

    @Transactional
    public List<TransferToSource> getTransferToSource(int pharmacyId, String currentDate, String currentTime) {
        return transferDao.getTransferToSource(pharmacyId, currentDate, currentTime);
    }

    @Transactional
    public List<TransferToDestination> getTransferToDestination(int pharmacyId, String currentDate, String currentTime) {
        return transferDao.getTransferToDestination(pharmacyId, currentDate, currentTime);
    }

    @Transactional
    public void newTransfer(int serial, int pharmacy_dst, int pharmacy_src, String[] startDate, String[] endDate) {
        transferDao.newTransfer(serial, pharmacy_dst, pharmacy_src, startDate, endDate);
    }

    @Transactional
    public int getMaxId() {
        return transferDao.getMaxId();
    }

    @Transactional
    public boolean addTransfer(Transfer transfer) {
        return transferDao.addTransfer(transfer);
    }
}
