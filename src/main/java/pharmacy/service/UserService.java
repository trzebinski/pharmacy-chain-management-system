package pharmacy.service;

public interface UserService {
    boolean logIn(String login, String password);
}
