package pharmacy.service;

import pharmacy.entity.Warehouse;
import pharmacy.model.MedicineStateInGivenPharmacy;
import pharmacy.model.MedicineWithShortBestBeforeDate;

import java.util.List;

public interface WarehouseService {
    List<Warehouse> getPharmacies();

    Warehouse getPharmacyByName(String name);

    Warehouse getPharmacyById(int id);

    List<MedicineStateInGivenPharmacy> medicinesInPharmacyAndDemandedState(int pharmacyId);

    List<MedicineStateInGivenPharmacy> medicinesInPharmacy(int pharmacyId);

    List<MedicineStateInGivenPharmacy> medicinesInPharmacy(int pharmacyId, String drugName,
                                                           String kind, String order);

    List<MedicineWithShortBestBeforeDate> medicinesWithShortBestBeforeDateInPharmacy(int pharmacyId, String currentDate);
}
