package pharmacy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pharmacy.dao.WarehouseDao;
import pharmacy.entity.Warehouse;
import pharmacy.model.MedicineStateInGivenPharmacy;
import pharmacy.model.MedicineWithShortBestBeforeDate;

import java.util.List;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    @Autowired
    private WarehouseDao warehouseDao;

    @Transactional
    public List<Warehouse> getPharmacies() {
        return warehouseDao.getPharmacies();
    }

    @Transactional
    public Warehouse getPharmacyByName(String name) {
        return warehouseDao.getPharmacyByName(name);
    }

    @Transactional
    public Warehouse getPharmacyById(int id) {
        return warehouseDao.getPharmacyById(id);
    }

    @Transactional
    public List<MedicineStateInGivenPharmacy> medicinesInPharmacy(int pharmacyId) {
        return warehouseDao.medicinesInPharmacy(pharmacyId);
    }

    @Transactional
    public List<MedicineStateInGivenPharmacy> medicinesInPharmacy(int pharmacyId, String drugName, String kind, String order) {
        return warehouseDao.medicinesInPharmacy(pharmacyId, drugName, kind, order);
    }

    @Transactional
    public List<MedicineStateInGivenPharmacy> medicinesInPharmacyAndDemandedState(int pharmacyId) {
        return warehouseDao.medicinesInPharmacyAndDemandedState(pharmacyId);
    }

    @Transactional
    public List<MedicineWithShortBestBeforeDate> medicinesWithShortBestBeforeDateInPharmacy(int pharmacyId, String currentDate) {
        return warehouseDao.medicinesWithShortBestBeforeDateInPharmacy(pharmacyId, currentDate);
    }
}
