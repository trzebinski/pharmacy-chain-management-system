package pharmacy.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pharmacy.model.DateParser;
import pharmacy.service.TimeService;
import pharmacy.service.WarehouseService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class HeaderController {

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private TimeService timeService;

    @RequestMapping(value = "/header", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String schedule(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String action = request.getParameter("action");
        String json = "";

        if (action.equals("changeCurrentPharmacy")) {
            String currentPharmacyName = request.getParameter("currentPharmacy");

            int currentPharmacy = warehouseService.getPharmacyByName(currentPharmacyName).getId();
            System.out.println(currentPharmacy);

            request.getSession().setAttribute("currentPharmacy", currentPharmacy);
            json = "success";
        } else if (action.equals("shiftDay")) {

            String currentDate = (String) request.getSession().getAttribute("currentDate");
            String currentTime = (String) request.getSession().getAttribute("currentTime");

            String shiftedDate = DateParser.getDateWithShiftInHours_withStartDate(currentDate, currentTime, 24)[0];

            timeService.setDate(shiftedDate);

            request.getSession().setAttribute("currentDate", shiftedDate);
        } else if (action.equals("shiftHour")) {

            String currentDate = (String) request.getSession().getAttribute("currentDate");
            String currentTime = (String) request.getSession().getAttribute("currentTime");

            String shiftedTime = DateParser.getDateWithShiftInHours_withStartDate(currentDate, currentTime, 1)[1] + "00";
            String shiftedDate = DateParser.getDateWithShiftInHours_withStartDate(currentDate, currentTime, 1)[0];

            timeService.setTime(shiftedTime);
            timeService.setDate(shiftedDate);
            request.getSession().setAttribute("currentTime", shiftedTime);
            request.getSession().setAttribute("currentDate", shiftedDate);
        }

        return json;
    }

}
