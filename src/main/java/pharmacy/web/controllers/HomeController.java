package pharmacy.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pharmacy.algorithm.Algorithm;
import pharmacy.entity.*;
import pharmacy.model.*;
import pharmacy.model.filters.HomeFilter;
import pharmacy.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
public class HomeController {
    private final int DIVIDE = 10;
    private List<List<MedicineStateInGivenPharmacy>> medicineStatePharmacies;
    private List<Warehouse> pharmaciesList;
    private List<HashMap<Integer, Integer>> medicineImportList;

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private MedicineService medicineService;

    @Autowired
    private RequestService requestService;

    @Autowired
    private Medicine_WarehouseService medicine_warehouseService;

    @Autowired
    private TimeService timeService;

    @Autowired
    private TransferService transferService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model, HttpServletRequest request) {

        if (request.getSession().getAttribute("currentPharmacy") == null || request.getSession().getAttribute("currentPharmacy").equals("")) {
            System.out.println("Atrybut currentPharmacy: " + request.getSession().getAttribute("currentPharmacy"));
            request.getSession().setAttribute("currentPharmacy", 1);
        }

        request.getSession().setAttribute("currentDate", timeService.getTimeObj().getDate());
        request.getSession().setAttribute("currentTime", timeService.getTimeObj().getTime());

        String user = (String) request.getSession().getAttribute("user");
        int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");

        pharmaciesList = warehouseService.getPharmacies();

        medicineStatePharmacies = new ArrayList<>();
        List<List<MedicineStateInGivenPharmacy>> medicineStatePharmaciesDevidedOnPieces = new ArrayList<>();
        List<Integer> medicineStatePagination = new ArrayList<>();
        medicineImportList = new ArrayList<>();

        for (int pharmacyId = 1; pharmacyId <= pharmaciesList.size(); pharmacyId++) {
            medicineStatePharmacies.add(warehouseService.medicinesInPharmacy(pharmacyId));
            medicineStatePharmaciesDevidedOnPieces.add(Pagination.divideMedicineState(medicineStatePharmacies.get(pharmacyId - 1), DIVIDE, 1));
            medicineStatePagination.add(Pagination.getNumberOfPages(medicineStatePharmacies.get(pharmacyId - 1).size(), DIVIDE));
        }

        for (int i = 0; i < medicineStatePharmacies.size(); i++) {
            medicineImportList.add(Algorithm.getActionOnMissingMedicines(medicineStatePharmacies, i));
        }

        for (int i : medicineStatePagination)
            System.out.println("Number of pages: " + i);

        model.addAttribute("user", user);
        model.addAttribute("currentPharmacy", currentPharmacy);
        model.addAttribute("pharmaciesList", pharmaciesList);
        model.addAttribute("medicineStatePharmacies", medicineStatePharmacies);
        model.addAttribute("medicineStatePharmaciesDivideOnPieces", medicineStatePharmaciesDevidedOnPieces);
        model.addAttribute("medicineStatePagination", medicineStatePagination);
        model.addAttribute("numberOfMedicines", Operations.getNumberOfMedicines(medicineStatePharmacies));
        model.addAttribute("pharmacyFilter", request.getSession().getAttribute("pharmacyFilter"));
        model.addAttribute("medicineImportList", medicineImportList.get(currentPharmacy - 1));

        model.addAttribute("medicineIL_json", JsonConverter.objectToJson(medicineImportList.get(currentPharmacy - 1)));


        return "home";
    }


    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public String home(Model model, HttpServletRequest request,
                       HomeFilter homeFilter,
                       @RequestParam(value = "action", required = false, defaultValue = "") String action) {

        System.out.println("!!! " + action);
        if (action.equals("search")) {

            String user = (String) request.getSession().getAttribute("user");
            int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");

            List<Warehouse> pharmaciesList = warehouseService.getPharmacies();

            medicineStatePharmacies = new ArrayList<>();
            List<List<MedicineStateInGivenPharmacy>> medicineStatePharmaciesDevidedOnPieces = new ArrayList<>();
            List<Integer> medicineStatePagination = new ArrayList<>();
            medicineImportList = new ArrayList<>();

            for (int pharmacyId = 1; pharmacyId <= pharmaciesList.size(); pharmacyId++) {

                System.out.println("Pharmacy: " + homeFilter.getPharmacy());

                if (homeFilter.getPharmacy() == 0 || pharmacyId == homeFilter.getPharmacy()) {

                    medicineStatePharmacies.add(warehouseService.medicinesInPharmacy(pharmacyId, homeFilter.getDrugName(),
                            homeFilter.getKind(), homeFilter.getOrder()));

                    medicineStatePharmaciesDevidedOnPieces.add(Pagination.divideMedicineState(medicineStatePharmacies.get(pharmacyId - 1), DIVIDE, 1));
                    medicineStatePagination.add(Pagination.getNumberOfPages(medicineStatePharmacies.get(pharmacyId - 1).size(), DIVIDE));
                } else {
                    medicineStatePharmacies.add(null);
                    medicineStatePharmaciesDevidedOnPieces.add(null);
                    medicineStatePagination.add(null);
                }
            }

            for (int i = 0; i < medicineStatePharmacies.size(); i++) {
                medicineImportList.add(Algorithm.getActionOnMissingMedicines(medicineStatePharmacies, i));
            }

            request.getSession().setAttribute("drugNameFilter", homeFilter.getDrugName());
            request.getSession().setAttribute("kindFilter", homeFilter.getKind());
            request.getSession().setAttribute("orderFilter", homeFilter.getOrder());
            request.getSession().setAttribute("pharmacyFilter", homeFilter.getPharmacy());


            model.addAttribute("user", user);
            model.addAttribute("currentPharmacy", currentPharmacy);
            model.addAttribute("pharmaciesList", pharmaciesList);
            model.addAttribute("medicineStatePharmacies", medicineStatePharmacies);
            model.addAttribute("medicineStatePharmaciesDivideOnPieces", medicineStatePharmaciesDevidedOnPieces);
            model.addAttribute("medicineStatePagination", medicineStatePagination);
            model.addAttribute("numberOfMedicines", Operations.getNumberOfMedicines(medicineStatePharmacies));
            model.addAttribute("pharmacyFilter", request.getSession().getAttribute("pharmacyFilter"));
            model.addAttribute("medicineImportList", medicineImportList.get(currentPharmacy - 1));

            model.addAttribute("medicineIL_json", JsonConverter.objectToJson(medicineImportList.get(currentPharmacy - 1)));


            return "home";
        }

        return "error";
    }

    @RequestMapping(value = "/home", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String schedule(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String action = request.getParameter("action");
        String json = "";

        if (action.equals("getDetails")) {
            int pharmacyId = Integer.parseInt(request.getParameter("pharmacyId"));
            int typeId = Integer.parseInt(request.getParameter("typeId"));

            List<Medicine> medicines = medicineService.getMedicine(pharmacyId, typeId);

            for (int i = 0; i < medicines.size(); i++) {
                medicines.get(i).setExpiryDate(DateParser.enlargeDate(medicines.get(i).getExpiryDate()));
            }

            json = JsonConverter.objectToJson(medicines);

        } else if (action.equals("utilize")) {

            int serial = Integer.parseInt(request.getParameter("serial"));
            int pharmacy = Integer.parseInt(request.getParameter("pharmacyName"));
            int typeId = Integer.parseInt(request.getParameter("typeId"));
            medicineService.utilizeMedicine(serial);
            boolean deleted = MedicineStateInGivenPharmacy.updateMedicine(medicineStatePharmacies.get(pharmacy - 1), typeId, true);
            request.getSession().setAttribute("medicineStatePharmacies", medicineStatePharmacies);

        } else if (action.equals("sold")) {

            int serial = Integer.parseInt(request.getParameter("serial"));
            medicineService.soldMedicine(serial);

        } else if (action.equals("pagination")) {

            int page = Integer.parseInt(request.getParameter("page"));
            int table = Integer.parseInt(request.getParameter("table"));
            json = JsonConverter.objectToJson(Pagination.divideMedicineState(medicineStatePharmacies.get(table), DIVIDE, page));

        } else if (action.equals("getMedicinesFromAnotherPharmacy")) {

            int currentPharmacyId = (int) request.getSession().getAttribute("currentPharmacy");
            int typeId = Integer.parseInt(request.getParameter("typeId"));
            List<PharmacyMedicineTypeState> list = medicineService.getGroupedMedicines(typeId, currentPharmacyId);

            json = JsonConverter.objectToJson(list);

        } else if (action.equals("getMedicinesFromAnotherPharmacyPart2")) {

            int type = Integer.parseInt(request.getParameter("typeId"));
            int warehouse_src = Integer.parseInt(request.getParameter("warehouse_src"));
            int warehouse_dst = Integer.parseInt(request.getParameter("warehouse_dst"));
            int number = Integer.parseInt(request.getParameter("number"));
            int status = Integer.parseInt(request.getParameter("status"));

            requestService.addRequest(new Request(type, warehouse_src, warehouse_dst, number, status));

        } else if (action.equals("getMedicinesFromWholesale")) {

            int type = Integer.parseInt(request.getParameter("typeId"));
            int warehouse_src = Integer.parseInt(request.getParameter("warehouse_src"));
            int warehouse_dst = Integer.parseInt(request.getParameter("warehouse_dst"));
            int number = Integer.parseInt(request.getParameter("number"));
            int status = Integer.parseInt(request.getParameter("status"));

            int maxSerialNumber = medicineService.getMaxSerialNumber();

            System.out.println("Max serial number: " + maxSerialNumber);
            int currentSerialNumber = maxSerialNumber;

            Time currentTime = timeService.getTimeObj();
            String startDate = currentTime.getDate();
            String startTime = currentTime.getTime();

            for (int i = 0; i < number; i++) {

                currentSerialNumber++;
                String expiryDate = DateParser.getDateWithShiftInMonths_withStartDate(startDate, startTime, 12)[0];
                Medicine addedMedicine = new Medicine(currentSerialNumber, expiryDate, type, 0, null);
                medicineService.addMedicine(addedMedicine);

                Medicine_Warehouse addedMedicine_Warehouse = new Medicine_Warehouse(currentSerialNumber, 4, 1);
                addedMedicine_Warehouse.setStatus(0);
                medicine_warehouseService.addMedicine_WarehouseDao(addedMedicine_Warehouse);

                Medicine_Warehouse addedMedicine_warehouse_proper = new Medicine_Warehouse(currentSerialNumber, warehouse_dst, 1);
                medicine_warehouseService.addMedicine_WarehouseDao(addedMedicine_warehouse_proper);

                String endDate = DateParser.getDateWithShiftInHours_withStartDate(startDate, startTime, 48)[0];
                String endTime = DateParser.getDateWithShiftInHours_withStartDate(startDate, startTime, 48)[1] + "00";

                Transfer addedTransfer = new Transfer(0, warehouse_src, warehouse_dst, startDate, endDate, startTime, endTime, currentSerialNumber);
                transferService.addTransfer(addedTransfer);
            }
        } else if (action.equals("moveToCurrentPharmacy")) {
            int type = Integer.parseInt(request.getParameter("typeId"));
            int warehouse_src = Integer.parseInt(request.getParameter("warehouse_src"));

            int serial = Integer.parseInt(request.getParameter("serial"));

            int warehouse_dst = medicine_warehouseService.getPharmacyIdWithGivenMedicineSerial(serial);
            System.out.println("Warehouse_src: " + warehouse_src);
            requestService.addRequest(new Request(type, warehouse_src, warehouse_dst, 1, 1));
        }


        return json;
    }

}
