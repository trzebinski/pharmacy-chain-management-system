package pharmacy.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pharmacy.service.UserService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("error", false);
        return "login";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String login(Model model, HttpServletRequest request,
                        @RequestParam(value = "login", required = false, defaultValue = "") String login,
                        @RequestParam(value = "password", required = false, defaultValue = "") String password) {
        System.out.println(login + " " + password);

        boolean logginCorrect = userService.logIn(login, password);
        if (logginCorrect) {
            System.out.println("Login correct");
            model.addAttribute("error", false);
            request.getSession().setAttribute("user", login);
            return "redirect:/home";
        } else {
            System.out.println("Login incorrect");
        }

        model.addAttribute("error", true);
        return "login";
    }
}
