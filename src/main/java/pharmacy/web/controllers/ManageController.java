package pharmacy.web.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pharmacy.algorithm.Algorithm;
import pharmacy.entity.*;
import pharmacy.model.*;
import pharmacy.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
public class ManageController {
    private final int DIVIDE = 10;
    private List<List<MedicineStateInGivenPharmacy>> medicineStatePharmacies;
    private List<List<RequestToGivenPharmacy>> requestToGivenPharmacy;
    private List<List<RequestToGivenPharmacy>> requestFromGivenPharmacy;
    private List<List<MedicineWithShortBestBeforeDate>> medicineWithShortBestBeforeDate;
    private List<HashMap<Integer, Integer>> medicineImportList, requestDecisionList, sendOrUtilizeList;
    private Model m;

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private MedicineService medicineService;

    @Autowired
    private RequestService requestService;

    @Autowired
    private TransferService transferService;

    @Autowired
    private Medicine_WarehouseService medicine_warehouseService;

    @Autowired
    private TimeService timeService;

    @RequestMapping(value = "/manage", method = RequestMethod.GET)
    public String manage(Model model, HttpServletRequest request) {

        if (request.getSession().getAttribute("currentPharmacy") == null || request.getSession().getAttribute("currentPharmacy").equals("")) {
            System.out.println("Atrybut currentPharmacy: " + request.getSession().getAttribute("currentPharmacy"));
            request.getSession().setAttribute("currentPharmacy", 1);
        }

        request.getSession().setAttribute("currentDate", timeService.getTimeObj().getDate());
        request.getSession().setAttribute("currentTime", timeService.getTimeObj().getTime());

        Time currentTime = timeService.getTimeObj();
        String startDate = currentTime.getDate();
        String startTime = currentTime.getTime();

        m = model;
        String user = (String) request.getSession().getAttribute("user");
        int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");
        System.out.println("CURRENT PHARMACY: " + currentPharmacy);

        List<Warehouse> pharmaciesList = warehouseService.getPharmacies();

        medicineStatePharmacies = new ArrayList<>();
        requestToGivenPharmacy = new ArrayList<>();
        requestFromGivenPharmacy = new ArrayList<>();
        medicineWithShortBestBeforeDate = new ArrayList<>();
        medicineImportList = new ArrayList<>();
        requestDecisionList = new ArrayList<>();
        sendOrUtilizeList = new ArrayList<>();

        for (int pharmacyId = 1; pharmacyId <= pharmaciesList.size(); pharmacyId++) {
            medicineStatePharmacies.add(warehouseService.medicinesInPharmacyAndDemandedState(pharmacyId));
            requestToGivenPharmacy.add(requestService.getRequests(pharmacyId));
            requestFromGivenPharmacy.add(requestService.getRequestsFrom(pharmacyId));
            medicineWithShortBestBeforeDate.add(warehouseService.medicinesWithShortBestBeforeDateInPharmacy(pharmacyId, startDate));
        }
        for (List<MedicineStateInGivenPharmacy> list : medicineStatePharmacies)
            list.sort((o1, o2) -> o2.getDeficite() - o1.getDeficite());

        for (int i = 0; i < medicineStatePharmacies.size(); i++) {
            medicineImportList.add(Algorithm.getActionOnMissingMedicines(medicineStatePharmacies, i));
            requestDecisionList.add(Algorithm.getDecisionOnRequest(medicineStatePharmacies.get(i), requestToGivenPharmacy.get(i)));
            sendOrUtilizeList.add(Algorithm.getDecisionSendOrUtilize(medicineWithShortBestBeforeDate.get(i), medicineStatePharmacies, i, startDate));
        }

        Algorithm.setPriorityOfRequest(medicineStatePharmacies, requestToGivenPharmacy);
        Algorithm.setPriorityOfRequest(medicineStatePharmacies, requestFromGivenPharmacy);

        System.out.println("ROZMIAR LISTY: " + medicineImportList.get(currentPharmacy - 1).size());


        model.addAttribute("user", user);
        model.addAttribute("currentPharmacy", currentPharmacy);
        model.addAttribute("pharmaciesList", pharmaciesList);

        model.addAttribute("requestToGivenPharmaciesDivideOnPieces", Pagination.divideRequestTo(requestToGivenPharmacy.get(currentPharmacy - 1), DIVIDE, 1));
        model.addAttribute("requestFromPharmacyDivideOnPieces", Pagination.divideRequestTo(requestFromGivenPharmacy.get(currentPharmacy - 1), DIVIDE, 1));
        model.addAttribute("medicineWithShortBestBeforeDateDivideOnPieces", Pagination.divideMedicineWithShortBestBeforeDate(medicineWithShortBestBeforeDate.get(currentPharmacy - 1), DIVIDE, 1));
        model.addAttribute("medicineStatePharmaciesDivideOnPieces", Pagination.divideMedicineState(medicineStatePharmacies.get(currentPharmacy - 1), DIVIDE, 1));
        model.addAttribute("medicineStatePagination", Pagination.getNumberOfPages(medicineStatePharmacies.get(currentPharmacy - 1).size(), DIVIDE));
        model.addAttribute("shortBestBeforePagination", Pagination.getNumberOfPages(medicineWithShortBestBeforeDate.get(currentPharmacy - 1).size(), DIVIDE));
        model.addAttribute("requestToPagination", Pagination.getNumberOfPages(requestToGivenPharmacy.get(currentPharmacy - 1).size(), DIVIDE));
        model.addAttribute("requestFromPagination", Pagination.getNumberOfPages(requestFromGivenPharmacy.get(currentPharmacy - 1).size(), DIVIDE));
        model.addAttribute("medicineImportList", medicineImportList.get(currentPharmacy - 1));
        model.addAttribute("requestDecisionList", requestDecisionList.get(currentPharmacy - 1));
        model.addAttribute("sendOrUtilizeList", sendOrUtilizeList.get(currentPharmacy - 1));

        model.addAttribute("medicineIL_json", JsonConverter.objectToJson(medicineImportList.get(currentPharmacy - 1)));
        model.addAttribute("requestDecisionList_json", JsonConverter.objectToJson(requestDecisionList.get(currentPharmacy - 1)));
        model.addAttribute("sendOrtilizeList_json", JsonConverter.objectToJson(sendOrUtilizeList.get(currentPharmacy - 1)));

        return "manage";
    }

    @RequestMapping(value = "/manage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String schedule(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String action = request.getParameter("action");
        String json = "";

        Time currentTime = timeService.getTimeObj();
        String startDate = currentTime.getDate();
        String startTime = currentTime.getTime();

        if (action.equals("details")) {
            System.out.println("Details");
            int pharmacyId = Integer.parseInt(request.getParameter("pharmacyId"));
            int typeId = Integer.parseInt(request.getParameter("typeId"));

            List<Medicine> medicines = medicineService.getMedicine(pharmacyId, typeId);

            for (int i = 0; i < medicines.size(); i++) {
                medicines.get(i).setExpiryDate(DateParser.enlargeDate(medicines.get(i).getExpiryDate()));
            }

            json = JsonConverter.objectToJson(medicines);

        } else if (action.equals("pagination")) {
            int table = Integer.parseInt(request.getParameter("table"));
            int page = Integer.parseInt(request.getParameter("page"));
            int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");
            switch (table) {
                case 1:
                    json = JsonConverter.objectToJson(Pagination.divideMedicineState(medicineStatePharmacies.get(currentPharmacy - 1), DIVIDE, page));
                    break;
                case 2:
                    json = JsonConverter.objectToJson(Pagination.divideRequestTo(requestToGivenPharmacy.get(currentPharmacy - 1), DIVIDE, page));
                    break;
                case 3:
                    json = JsonConverter.objectToJson(Pagination.divideMedicineWithShortBestBeforeDate(medicineWithShortBestBeforeDate.get(currentPharmacy - 1), DIVIDE, page));
                    break;
            }

        } else if (action.equals("addToSession")) {
            request.getSession().setAttribute("requestToRefuse", request.getParameter("requestId"));

        } else if (action.equals("refuseRequest")) {
            int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");
            requestService.updateRequest(Integer.valueOf((String) request.getSession().getAttribute("requestToRefuse")), 2);
            requestToGivenPharmacy.set(currentPharmacy - 1, requestService.getRequests(currentPharmacy));
            m.addAttribute("requestToGivenPharmaciesDivideOnPieces", Pagination.divideRequestTo(requestToGivenPharmacy.get(currentPharmacy - 1), DIVIDE, 1));
            json = JsonConverter.objectToJson(Pagination.divideRequestTo(requestToGivenPharmacy.get(currentPharmacy - 1), DIVIDE, 1));

        } else if (action.equals("utilize")) {
            int serial = Integer.parseInt(request.getParameter("serial"));
            medicineService.utilizeMedicine(serial);

        } else if (action.equals("sendToPharmacy")) {
            int serial = Integer.parseInt(request.getParameter("serial"));
            int pharmacy_dst = Integer.parseInt(request.getParameter("pharmacy_dst"));
            int pharmacy_src = Integer.parseInt(request.getParameter("pharmacy_src"));
            int typeId = Integer.parseInt(request.getParameter("typeId"));

            String[] start = DateParser.getDateWithShiftInHours_withStartDate(startDate, startTime, 0);
            String[] end = DateParser.getDateWithShiftInHours_withStartDate(startDate, startTime, 6);
            end[1] += "00";

            boolean transfered = medicineService.transferMedicine(serial, pharmacy_src, pharmacy_dst);
            transferService.newTransfer(serial, pharmacy_dst, pharmacy_src, start, end);
            if (transfered) {
                MedicineStateInGivenPharmacy.updateMedicine(medicineStatePharmacies.get(pharmacy_src - 1), typeId, false);
            }

        } else if (action.equals("acceptRequest")) {
            int kind = Integer.parseInt(request.getParameter("kind"));
            int pharmacy_dst = Integer.parseInt(request.getParameter("pharmacy_dst"));
            int pharmacy_src = Integer.parseInt(request.getParameter("pharmacy_src"));
            int typeId = Integer.parseInt(request.getParameter("typeId"));
            int requestId = Integer.parseInt(request.getParameter("requestId"));
            System.out.println("RODZAJ: " + kind);

            String[] start = DateParser.getDateWithShiftInHours_withStartDate(startDate, startTime, 0);
            String[] end = DateParser.getDateWithShiftInHours_withStartDate(startDate, startTime, 6);
            end[1] += "00";

            if (kind == 0) {
                ObjectMapper mapper = new ObjectMapper();
                List<Integer> list = mapper.readValue(request.getParameter("data"), new TypeReference<List<Integer>>() {
                });
                for (int i : list) {
                    boolean transfered = medicineService.transferMedicine(i, pharmacy_dst, pharmacy_src);
                    transferService.newTransfer(i, pharmacy_src, pharmacy_dst, start, end);
                    if (transfered) {
                        MedicineStateInGivenPharmacy.updateMedicine(medicineStatePharmacies.get(pharmacy_dst - 1), typeId, false);
                        requestService.updateRequest(requestId, 0);
                        RequestToGivenPharmacy.updateRequests(requestToGivenPharmacy.get(pharmacy_dst - 1), requestId);
                        MedicineWithShortBestBeforeDate.updateMedicine(medicineWithShortBestBeforeDate.get(pharmacy_dst - 1), i);
                    }
                }
            } else {
                int number = Integer.parseInt(request.getParameter("number"));
                System.out.println("NUMBER IS: " + number);
                List<Medicine> medicines = medicineService.getMedicine(pharmacy_dst, typeId);
                List<Medicine> result;
                //Enlarge dates
                for (int i = 0; i < medicines.size(); i++) {
                    medicines.get(i).setExpiryDate(DateParser.enlargeDate(medicines.get(i).getExpiryDate()));
                }

                for (int i = 0; i < number; i++) {
                    boolean transfered = medicineService.transferMedicine(medicines.get(i).getSerial(), pharmacy_dst, pharmacy_src);
                    transferService.newTransfer(medicines.get(i).getSerial(), pharmacy_src, pharmacy_dst, start, end);
                    if (transfered) {
                        MedicineStateInGivenPharmacy.updateMedicine(medicineStatePharmacies.get(pharmacy_dst - 1), typeId, false);
                        requestService.updateRequest(requestId, 0);
                        RequestToGivenPharmacy.updateRequests(requestToGivenPharmacy.get(pharmacy_dst - 1), typeId);
                        MedicineWithShortBestBeforeDate.updateMedicine(medicineWithShortBestBeforeDate.get(pharmacy_dst - 1), i);
                    }
                }
                result = medicines.subList(0, number);
                System.out.println("RESULT SIZE: " + result.size());
                json = JsonConverter.objectToJson(result);
            }
        } else if (action.equals("getMedicinesFromAnotherPharmacy")) {

            int currentPharmacyId = (int) request.getSession().getAttribute("currentPharmacy");
            int typeId = Integer.parseInt(request.getParameter("typeId"));
            List<PharmacyMedicineTypeState> list = medicineService.getGroupedMedicines(typeId, currentPharmacyId);

            json = JsonConverter.objectToJson(list);

        } else if (action.equals("getMedicinesFromAnotherPharmacyPart2")) {

            int type = Integer.parseInt(request.getParameter("typeId"));
            int warehouse_src = Integer.parseInt(request.getParameter("warehouse_src"));
            int warehouse_dst = Integer.parseInt(request.getParameter("warehouse_dst"));
            int number = Integer.parseInt(request.getParameter("number"));
            int status = Integer.parseInt(request.getParameter("status"));

            requestService.addRequest(new Request(type, warehouse_src, warehouse_dst, number, status));

        } else if (action.equals("getMedicinesFromWholesale")) {

            int type = Integer.parseInt(request.getParameter("typeId"));
            int warehouse_src = Integer.parseInt(request.getParameter("warehouse_src"));
            int warehouse_dst = Integer.parseInt(request.getParameter("warehouse_dst"));
            int number = Integer.parseInt(request.getParameter("number"));
            int status = Integer.parseInt(request.getParameter("status"));

            int maxSerialNumber = medicineService.getMaxSerialNumber();

            System.out.println("Max serial number: " + maxSerialNumber);
            int currentSerialNumber = maxSerialNumber;
            for (int i = 0; i < number; i++) {

                currentSerialNumber++;
                String expiryDate = DateParser.getDateWithShiftInMonths_withStartDate(startDate, startTime, 12)[0];
                Medicine addedMedicine = new Medicine(currentSerialNumber, expiryDate, type, 0, null);
                medicineService.addMedicine(addedMedicine);

                Medicine_Warehouse addedMedicine_Warehouse = new Medicine_Warehouse(currentSerialNumber, 4, 1);
                addedMedicine_Warehouse.setStatus(0);
                medicine_warehouseService.addMedicine_WarehouseDao(addedMedicine_Warehouse);

                Medicine_Warehouse addedMedicine_warehouse_proper = new Medicine_Warehouse(currentSerialNumber, warehouse_dst, 1);
                medicine_warehouseService.addMedicine_WarehouseDao(addedMedicine_warehouse_proper);

                String endDate = DateParser.getDateWithShiftInHours_withStartDate(startDate, startTime, 48)[0];
                String endTime = DateParser.getDateWithShiftInHours_withStartDate(startDate, startTime, 48)[1] + "00";

                Transfer addedTransfer = new Transfer(0, warehouse_src, warehouse_dst, startDate, endDate, startTime, endTime, currentSerialNumber);
                transferService.addTransfer(addedTransfer);
            }
        }


        return json;
    }
}
