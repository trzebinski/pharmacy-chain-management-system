package pharmacy.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pharmacy.entity.Time;
import pharmacy.entity.Warehouse;
import pharmacy.model.JsonConverter;
import pharmacy.model.Pagination;
import pharmacy.model.TransferToDestination;
import pharmacy.model.TransferToSource;
import pharmacy.service.TimeService;
import pharmacy.service.TransferService;
import pharmacy.service.WarehouseService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konrad on 05.11.2017.
 */
@Controller
public class OrderController {
    private final int DIVIDE = 10;
    private List<List<TransferToSource>> transferPharmacyToSource;
    private List<List<TransferToDestination>> transferPharmacyToDestination;

    @Autowired
    private TimeService timeService;

    @Autowired
    private TransferService transferService;

    @Autowired
    private WarehouseService warehouseService;

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public String order(Model model, HttpServletRequest request) {

        if (request.getSession().getAttribute("currentPharmacy") == null || request.getSession().getAttribute("currentPharmacy").equals("")) {
            System.out.println("Atrybut currentPharmacy: " + request.getSession().getAttribute("currentPharmacy"));
            request.getSession().setAttribute("currentPharmacy", 1);
        }

        request.getSession().setAttribute("currentDate", timeService.getTimeObj().getDate());
        request.getSession().setAttribute("currentTime", timeService.getTimeObj().getTime());

        Time currentTime = timeService.getTimeObj();
        String startDate = currentTime.getDate();
        String startTime = currentTime.getTime();

        String user = (String) request.getSession().getAttribute("user");
        int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");

        List<Warehouse> pharmaciesList = warehouseService.getPharmacies();

        transferPharmacyToSource = new ArrayList<>();
        transferPharmacyToDestination = new ArrayList<>();

        for (int pharmacyId = 1; pharmacyId <= pharmaciesList.size(); pharmacyId++) {
            transferPharmacyToSource.add(transferService.getTransferToSource(pharmacyId, startDate, startTime));
            transferPharmacyToDestination.add(transferService.getTransferToDestination(pharmacyId, startDate, startTime));
        }

        model.addAttribute("user", user);
        model.addAttribute("currentPharmacy", currentPharmacy);
        model.addAttribute("pharmaciesList", pharmaciesList);
        model.addAttribute("transferPharmacyToSourceDivideOnPieces", Pagination.divideTransferToSource(transferPharmacyToSource.get(currentPharmacy - 1), DIVIDE, 1));
        model.addAttribute("transferPharmacyToDestinationDivideOnPieces", Pagination.divideTransferToDestination(transferPharmacyToDestination.get(currentPharmacy - 1), DIVIDE, 1));
        model.addAttribute("transferPharmacyToSourcePagination", Pagination.getNumberOfPages(transferPharmacyToSource.get(currentPharmacy - 1).size(), DIVIDE));
        model.addAttribute("transferPharmacyToDestinationPagination", Pagination.getNumberOfPages(transferPharmacyToDestination.get(currentPharmacy - 1).size(), DIVIDE));
        return "order";
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String schedule(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String action = request.getParameter("action");
        String json = "";
        if (action.equals("pagination")) {
            int table = Integer.parseInt(request.getParameter("table"));
            int page = Integer.parseInt(request.getParameter("page"));
            int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");
            switch (table) {
                case 1:
                    json = JsonConverter.objectToJson(Pagination.divideTransferToSource(transferPharmacyToSource.get(currentPharmacy - 1), DIVIDE, page));
                    break;
                case 2:
                    json = JsonConverter.objectToJson(Pagination.divideTransferToDestination(transferPharmacyToDestination.get(currentPharmacy - 1), DIVIDE, page));
                    break;
            }
        }

        return json;
    }
}
