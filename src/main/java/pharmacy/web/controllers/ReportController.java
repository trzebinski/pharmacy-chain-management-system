package pharmacy.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pharmacy.entity.Warehouse;
import pharmacy.model.JsonConverter;
import pharmacy.model.MedicineToRaport;
import pharmacy.model.Operations;
import pharmacy.model.filters.ReportFilter;
import pharmacy.service.RaportService;
import pharmacy.service.TimeService;
import pharmacy.service.WarehouseService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konrad on 05.11.2017.
 */
@Controller
public class ReportController {

    @Autowired
    private RaportService raportService;

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private TimeService timeService;

    @RequestMapping(value = "/report", method = RequestMethod.GET)
    public String report(Model model, HttpServletRequest request) {

        if (request.getSession().getAttribute("currentPharmacy") == null || request.getSession().getAttribute("currentPharmacy").equals("")) {
            System.out.println("Atrybut currentPharmacy: " + request.getSession().getAttribute("currentPharmacy"));
            request.getSession().setAttribute("currentPharmacy", 1);
        }
        String currentDate = timeService.getTimeObj().getDate();
        String currentTime = timeService.getTimeObj().getTime();

        request.getSession().setAttribute("currentDate", currentDate);
        request.getSession().setAttribute("currentTime", currentTime);

        String user = (String) request.getSession().getAttribute("user");
        int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");

        List<Warehouse> pharmaciesList = warehouseService.getPharmacies();

        List<List<MedicineToRaport>> medicineToRaport = new ArrayList<>();
        for (int pharmacyId = 1; pharmacyId <= pharmaciesList.size(); pharmacyId++) {
            medicineToRaport.add(raportService.getMedicineToRaport(pharmacyId, currentDate));
        }

        List<String> data = Operations.getListOfLast12Months(currentDate);

        model.addAttribute("user", user);
        model.addAttribute("currentPharmacy", currentPharmacy);
        model.addAttribute("pharmaciesList", pharmaciesList);
        model.addAttribute("medicineToRaport", medicineToRaport.get(currentPharmacy - 1));
        model.addAttribute("pharmacyName", pharmaciesList.get(currentPharmacy - 1).getName());
        model.addAttribute("last12MonthsList", JsonConverter.objectToJson(Operations.getListOfLast12Months(currentDate)));
        model.addAttribute("dataToGraph", JsonConverter.objectToJson(Operations.prepareDataToGraph(Operations.getListOfLast12Months(currentDate), medicineToRaport.get(currentPharmacy - 1), null)));

        model.addAttribute("medicineToRaport_json", JsonConverter.objectToJson(medicineToRaport.get(currentPharmacy - 1)));
        return "report";
    }

    @RequestMapping(value = "/report", method = RequestMethod.POST)
    public String home(Model model, HttpServletRequest request,
                       ReportFilter reportFilter,
                       @RequestParam(value = "action", required = false, defaultValue = "") String action) {

        System.out.println("!!! " + action);
        if (action.equals("createGraph")) {

            String currentDate = timeService.getTimeObj().getDate();
            String currentTime = timeService.getTimeObj().getTime();

            request.getSession().setAttribute("currentDate", currentDate);
            request.getSession().setAttribute("currentTime", currentTime);

            String user = (String) request.getSession().getAttribute("user");
            int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");

            List<Warehouse> pharmaciesList = warehouseService.getPharmacies();

            List<List<MedicineToRaport>> medicineToRaport = new ArrayList<>();
            for (int pharmacyId = 1; pharmacyId <= pharmaciesList.size(); pharmacyId++) {
                medicineToRaport.add(raportService.getMedicineToRaport(pharmacyId, currentDate));
            }

            List<String> data = Operations.getListOfLast12Months(currentDate);


            request.getSession().setAttribute("drugNameFilter", reportFilter.getDrugName());
            request.getSession().setAttribute("kindFilter", reportFilter.getKind());

            model.addAttribute("user", user);
            model.addAttribute("currentPharmacy", currentPharmacy);
            model.addAttribute("pharmaciesList", pharmaciesList);
            model.addAttribute("medicineToRaport", medicineToRaport.get(currentPharmacy - 1));
            model.addAttribute("pharmacyName", pharmaciesList.get(currentPharmacy - 1).getName());
            model.addAttribute("last12MonthsList", JsonConverter.objectToJson(Operations.getListOfLast12Months(currentDate)));
            model.addAttribute("dataToGraph", JsonConverter.objectToJson(Operations.prepareDataToGraph(Operations.getListOfLast12Months(currentDate), medicineToRaport.get(currentPharmacy - 1), reportFilter)));

            model.addAttribute("medicineToRaport_json", JsonConverter.objectToJson(medicineToRaport.get(currentPharmacy - 1)));

            return "report";
        }

        return "error";
    }
}
