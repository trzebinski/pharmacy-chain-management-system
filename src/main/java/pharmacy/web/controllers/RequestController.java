package pharmacy.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pharmacy.entity.Warehouse;
import pharmacy.model.JsonConverter;
import pharmacy.model.MedicineStateInGivenPharmacy;
import pharmacy.model.Operations;
import pharmacy.model.Pagination;
import pharmacy.model.filters.HomeFilter;
import pharmacy.service.DemandedStateService;
import pharmacy.service.TimeService;
import pharmacy.service.WarehouseService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konrad on 01.11.2017.
 */
@Controller
public class RequestController {
    private final int DIVIDE = 10;
    private List<List<MedicineStateInGivenPharmacy>> medicineStatePharmacies;

    @Autowired
    private DemandedStateService demandedStateService;

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private TimeService timeService;

    @RequestMapping(value = "/request", method = RequestMethod.GET)
    public String request(Model model, HttpServletRequest request) {

        if (request.getSession().getAttribute("currentPharmacy") == null || request.getSession().getAttribute("currentPharmacy").equals("")) {
            System.out.println("Atrybut currentPharmacy: " + request.getSession().getAttribute("currentPharmacy"));
            request.getSession().setAttribute("currentPharmacy", 1);
        }

        request.getSession().setAttribute("currentDate", timeService.getTimeObj().getDate());
        request.getSession().setAttribute("currentTime", timeService.getTimeObj().getTime());

        String user = (String) request.getSession().getAttribute("user");
        int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");

        List<Warehouse> pharmaciesList = warehouseService.getPharmacies();

        medicineStatePharmacies = new ArrayList<>();

        for (int pharmacyId = 1; pharmacyId <= pharmaciesList.size(); pharmacyId++) {
            medicineStatePharmacies.add(demandedStateService.demandedState(pharmacyId));
        }

        model.addAttribute("user", user);
        model.addAttribute("currentPharmacy", currentPharmacy);
        model.addAttribute("currentPharmacyName", warehouseService.getPharmacyById(currentPharmacy).getName());
        model.addAttribute("pharmaciesList", pharmaciesList);
        model.addAttribute("medicineStatePharmaciesDivideOnPieces", Pagination.divideMedicineState(medicineStatePharmacies.get(currentPharmacy - 1), DIVIDE, 1));
        model.addAttribute("medicineStatePagination", Pagination.getNumberOfPages(medicineStatePharmacies.get(currentPharmacy - 1).size(), DIVIDE));
        model.addAttribute("numberOfMedicines", Operations.getNumberOfMedicines(medicineStatePharmacies));

        return "request";
    }

    @RequestMapping(value = "/request", method = RequestMethod.POST)
    public String request(Model model, HttpServletRequest request,
                          HomeFilter homeFilter,
                          @RequestParam(value = "action", required = false, defaultValue = "") String action) {

        if (action.equals("search")) {

            String user = (String) request.getSession().getAttribute("user");
            int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");

            List<Warehouse> pharmaciesList = warehouseService.getPharmacies();

            List<List<MedicineStateInGivenPharmacy>> medicineStatePharmacies = new ArrayList<>();
            for (int pharmacyId = 1; pharmacyId <= pharmaciesList.size(); pharmacyId++) {
                if (homeFilter.getPharmacy() == 0 || pharmacyId == homeFilter.getPharmacy()) {
                    medicineStatePharmacies.add(demandedStateService.demandedState(pharmacyId,
                            homeFilter.getDrugName(), homeFilter.getKind(), homeFilter.getOrder()));
                } else {
                    medicineStatePharmacies.add(null);
                }
            }

            request.getSession().setAttribute("drugNameFilter", homeFilter.getDrugName());
            request.getSession().setAttribute("kindFilter", homeFilter.getKind());
            request.getSession().setAttribute("orderFilter", homeFilter.getOrder());
            request.getSession().setAttribute("pharmacyFilter", homeFilter.getPharmacy());

            model.addAttribute("user", user);
            model.addAttribute("currentPharmacy", currentPharmacy);
            model.addAttribute("pharmaciesList", pharmaciesList);
            model.addAttribute("medicineStatePharmaciesDivideOnPieces", Pagination.divideMedicineState(medicineStatePharmacies.get(currentPharmacy - 1), DIVIDE, 1));
            model.addAttribute("medicineStatePagination", Pagination.getNumberOfPages(medicineStatePharmacies.get(currentPharmacy - 1).size(), DIVIDE));
            model.addAttribute("numberOfMedicines", Operations.getNumberOfMedicines(medicineStatePharmacies));

            return "request";
        }

        return "error";
    }

    @RequestMapping(value = "/request", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String schedule(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String action = request.getParameter("action");
        String json = "";
        if (action.equals("pagination")) {
            int table = Integer.parseInt(request.getParameter("table"));
            int page = Integer.parseInt(request.getParameter("page"));
            int currentPharmacy = (int) request.getSession().getAttribute("currentPharmacy");

            json = JsonConverter.objectToJson(Pagination.divideMedicineState(medicineStatePharmacies.get(currentPharmacy - 1), DIVIDE, page));
        } else if (action.equals("save")) {
            int pharmacyId = Integer.parseInt(request.getParameter("pharmacyId"));
            int typeId = Integer.parseInt(request.getParameter("typeId"));
            int newDemandedState = Integer.parseInt(request.getParameter("newDemandedState"));

            System.out.println("type:" + typeId + " newDemandedState: " + newDemandedState + " pharmacyId: " + pharmacyId);
            demandedStateService.changeDemandedState(pharmacyId, typeId, newDemandedState);

            for (MedicineStateInGivenPharmacy current : medicineStatePharmacies.get(pharmacyId - 1)) {
                if (current.getId() == typeId) {
                    current.setDemandedCount(newDemandedState);
                }
            }
        }

        return json;
    }
}
