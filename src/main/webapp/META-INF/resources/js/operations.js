
window.onload = function init()
{
    var dateSpan = document.getElementById("date_and_time");
    setDateAndTime(dateSpan);

    hideLoader();
};


window.onbeforeunload = function(){
    showLoader();
};


$(document).ready(function() {
    $('select').material_select();
    $('.modal').modal();

    setInterval(function() {
        $('.flicker').fadeTo(600, 0.2, function() {
            $(this).fadeTo(600, 1);
        });
    },1500);
});


function showLoader(){
    var loader = document.getElementById("loader");
    loader.style.visibility = "visible";
}

function hideLoader(){
    var loader = document.getElementById("loader");
    loader.style.visibility = "hidden";
}


function changeCurrentPharmacy(event) {

    var select = event.target;
    var currentPharmacy = $(select).children(":selected").html();

    var json = {"action": "changeCurrentPharmacy", "currentPharmacy": currentPharmacy};

    $.ajax({
        type: "post",
        url: "/header.json",
        cache: false,

        data: json,
        dataType: "text",
        success: function (response) {
            location.reload();
        },
        error: function () {
        }
    });
}


function shiftDay(){
    var json = {
        "action": "shiftDay"
    };

    $.ajax({
        type: "post",
        url: "/header.json",
        cache: false,

        data: json,
        dataType: "text",
        success: function (response) {
            location.reload();
        },
        error: function () {

        }
    });
}

function shiftHour(){
    var json = {
        "action": "shiftHour"
    };

    $.ajax({
        type: "post",
        url: "/header.json",
        cache: false,

        data: json,
        dataType: "text",
        success: function (response) {
            location.reload();
        },
        error: function () {

        }
    });
}
