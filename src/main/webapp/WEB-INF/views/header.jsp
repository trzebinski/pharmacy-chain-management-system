<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cor" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div>
    <ul id="dropdown1" class="dropdown-content">
        <li><a href="/" target="_self">Wyloguj</a></li>
    </ul>

    <nav class="nav-extended">
        <div class="nav-wrapper" style="height: 15%">
            <a href="/home" target="_self" class="brand-logo"><img
                    src="<c:url value="/resources/images/pharmacyIcon.png" />" alt="Apteka"
                    style="max-height: 20%; max-width: 20%;"></a>

            <span id="date_and_time" class="right" style="margin-right: 20px;"></span>
        </div>

        <div class="nav-content">
            <ul class="tabs tabs-transparent">
                <li id="home" class="tab"><a id="homeUnderline" class="active" href="/home" target="_self">Stan
                    magazynu</a></li>
                <li id="manage" class="tab"><a id="manageUnderline" href="/manage" target="_self">Zarządzaj</a></li>
                <li id="order" class="tab"><a id="orderUnderline" href="/order" target="_self">Zamówienia</a></li>
                <li id="request" class="tab"><a id="requestUnderline" href="/request" target="_self">Stany żądane</a>
                </li>
                <li id="report" class="tab"><a id="reportUnderline" href="/report" target="_self">Raporty</a></li>
                <li class="right hide-on-med-and-down"><a class="dropdown-button" href="#!" data-activates="dropdown1">
                    <i class="material-icons right">person</i></a></li>
                <li class="right tab">${user}</li>
            </ul>
        </div>
    </nav>
</div>
</br>
<div id="loader" class="preloader-wrapper active" style="position: fixed; z-index: 10; right: 47%; top: 25%;">
    <div class="spinner-layer spinner-red-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div>
        <div class="gap-patch">
            <div class="circle"></div>
        </div>
        <div class="circle-clipper right">
            <div class="circle"></div>
        </div>
    </div>
</div>

<div class="right" style="margin-right: 20px;">
    <i id="shiftDayIcon" onclick="shiftDay();" class="material-icons tooltipped" data-tooltip="Przesuń 1 dzień do przodu!">play_arrow</i><i
        id="shiftHourIcon" onclick="shiftHour();" class="material-icons tooltipped"
        data-tooltip="Przesuń 1 godzinę do przodu!">fast_forward</i>
</div>

<%-- Current pharmacy changer --%>
<div class="row">
    <div class="input-field col s2 right">
        <select onchange="changeCurrentPharmacy(event)">
            <cor:forEach items="${pharmaciesList}" var="current">
                <option value="${current.id}"
                        <cor:if test="${current.id == currentPharmacy}">selected</cor:if>>${current.name}</option>
            </cor:forEach>
        </select>
        <label>Wybierz aktywną aptekę:</label>
    </div>
</div>
