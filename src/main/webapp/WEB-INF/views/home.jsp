<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="cor" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="filter">
    <div class="row">
        <sf:form class="col s12" method="post" commandName="homePharmacy">
            <div class="row">
                <h6 class="left">FILTRY:</h6>
            </div>
            <div class="row">
                <div class="input-field col s3">
                    <input id="drugName" name="drugName" type="text" class="validate" value="${drugNameFilter}">
                    <label for="drugName">Nazwa leku:</label>
                </div>
                <div class="input-field col s2">
                    <select id="kind" name="kind">
                            <%--<option value="0">Wszystkie</option>--%>
                        <option value="" <cor:if test="${kindFilter == ''}">selected</cor:if>>Wszystkie</option>
                        <option value="tabletki" <cor:if test="${kindFilter == 'tabletki'}">selected</cor:if>>Tabletki
                        </option>
                        <option value="saszetki" <cor:if test="${kindFilter == 'saszetki'}">selected</cor:if>>Saszetki
                        </option>
                        <option value="syrop" <cor:if test="${kindFilter == 'syrop'}">selected</cor:if>>Syropy</option>
                        <option value="czopki" <cor:if test="${kindFilter == 'czopki'}">selected</cor:if>>Czopki
                        </option>
                    </select>
                    <label>Rodzaj leku:</label>
                </div>
                <div class="input-field col s3">
                    <select id="order" name="order">
                        <option value="drugName" <cor:if test="${orderFilter == 'drugName'}">selected</cor:if>>Nazwa
                        </option>
                        <option value="count" <cor:if test="${orderFilter == 'count'}">selected</cor:if>>Liczba leków na
                            stanie
                        </option>
                        <option value="capacity" <cor:if test="${orderFilter == 'capacity'}">selected</cor:if>>
                            Opakowanie
                        </option>
                        <option value="price" <cor:if test="${orderFilter == 'price'}">selected</cor:if>>Cena</option>
                    </select>
                    <label>Sortowanie:</label>
                </div>
                <div class="input-field col s3">
                    <select id="pharmacyName" name="pharmacy">
                        <option value="0">Wszystkie</option>

                        <cor:forEach items="${pharmaciesList}" var="current">
                            <option value="${current.id}"
                                    <cor:if test="${pharmacyFilter == current.id}">selected</cor:if>>${current.name}</option>
                        </cor:forEach>
                    </select>
                    <label>Wybierz aptekę:</label>
                </div>
                    <%--<div class="col s1">--%>
                <button class="btn-floating btn-large waves-effect waves-light orange tooltipped"
                        data-position="bottom"
                        data-delay="50" data-tooltip="Szukaj!" type="submit" name="action" value="search">
                    <i class="large material-icons">search</i>
                </button>
                    <%--</div>--%>
            </div>
        </sf:form>
    </div>
</div>
<br>


<div class="container">
    <%--<cor:forEach var="processedPharmacy" begin="1" end="${pharmaciesList.size()}">--%>
    <%--<span>${pharmacyMedicines}</span>--%>
    <%--</cor:forEach>--%>

    <%--If nothing to display--%>
    <cor:if test="${numberOfMedicines == 0}">
        <div>
            <br><br>
            <h4 class="center-align">Brak leków spełniających wybrane kryteria!</h4>
            <br><br>
        </div>
    </cor:if>

    <cor:forEach var="processedPharmacy" begin="0" end="${pharmaciesList.size()-1}">

        <cor:if test="${medicineStatePharmacies.get(processedPharmacy).size() > 0}">

            <h4>${pharmaciesList.get(processedPharmacy).name}</h4>
            <hr>

            <cor:if test="${pharmaciesList.get(processedPharmacy).id == currentPharmacy}">
                <table id="tableCurrent" class="bordered striped highlight responsive-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nazwa</th>
                        <th>Dawka</th>
                        <th>Rodzaj</th>
                        <th>Opakowanie</th>
                        <th>Cena</th>
                        <th>Na stanie</th>
                        <th>Akcje</th>
                    </tr>
                    </thead>

                    <tbody>

                    <cor:forEach items="${medicineStatePharmaciesDivideOnPieces.get(processedPharmacy)}"
                                 var="currentMedicine">
                        <tr>
                            <td>${currentMedicine.id}</td>
                            <td>${currentMedicine.name}</td>
                            <td>${currentMedicine.dose} ${currentMedicine.doseUnit}</td>
                            <td>${currentMedicine.kind}</td>
                            <td>${currentMedicine.capacity}</td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" value="${currentMedicine.price}"/>
                                zł
                            </td>
                            <td>${currentMedicine.count}</td>
                            <td>
                                <a class="dropdown-button tooltipped modal-trigger" href="#modal1"
                                   data-position="bottom"
                                   data-delay="50"
                                   data-tooltip="Szczegóły"
                                   onclick="getDetails(event,${pharmaciesList.get(processedPharmacy).id},${currentMedicine.id},1)">
                                    <i class="small material-icons">info</i>
                                </a>
                                <a class="dropdown-button tooltipped modal-trigger ${(medicineImportList.get(currentMedicine.id)) == 2 ? "flicker" : ""}" href="#wholesaler_modal" data-position="bottom"
                                   data-delay="50"
                                   data-tooltip="Zamów z hurtowni"
                                   onclick="getMedicinesFromWholesale_prepare(event);">
                                    <i class="small material-icons">reply</i>
                                </a>
                                <a class="dropdown-button tooltipped modal-trigger ${(medicineImportList.get(currentMedicine.id)) == 1 ? "flicker" : ""}" href="#typeState_modal"
                                   data-position="bottom"
                                   data-delay="50"
                                   data-tooltip="Zamów z innej apteki"
                                   onclick="getMedicinesFromAnotherPharmacy(event);">
                                    <i class="small material-icons">reply_all</i>
                                </a>
                                    <%--<a class="dropdown-button tooltipped" href="#" data-position="bottom" data-delay="50" data-tooltip="">--%>
                                    <%--<i class="small material-icons">publish</i>--%>
                                    <%--</a>--%>
                            </td>
                        </tr>
                    </cor:forEach>
                    </tbody>
                </table>

                <%--<ul class="pagination">
                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="active"><a href="#!">1</a></li>
                    <li class="waves-effect"><a href="#!">2</a></li>
                    <li class="waves-effect"><a href="#!">3</a></li>
                    <li class="waves-effect"><a href="#!">4</a></li>
                    <li class="waves-effect"><a href="#!">5</a></li>
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                </ul>--%>

                <ul class="pagination">
                    <cor:forEach var="counter" begin="1" end="${medicineStatePagination.get(processedPharmacy)}">
                        <cor:if test="${counter == 1}">
                            <li class="active waves-effect tablePagination${processedPharmacy}"><a
                                    onclick="changePharmacyList(this,${counter},${processedPharmacy},'tableCurrent', ${pharmaciesList.get(processedPharmacy).id})">${counter}</a>
                            </li>
                        </cor:if>
                        <cor:if test="${counter > 1}">
                            <li class="waves-effect tablePagination${processedPharmacy}"><a
                                    onclick="changePharmacyList(this,${counter},${processedPharmacy},'tableCurrent', ${pharmaciesList.get(processedPharmacy).id})">${counter}</a>
                            </li>
                        </cor:if>
                    </cor:forEach>
                </ul>
            </cor:if>

            <cor:if test="${pharmaciesList.get(processedPharmacy).id != currentPharmacy}">
                <table id="tableAnother${processedPharmacy}" class="bordered striped highlight responsive-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nazwa</th>
                        <th>Dawka</th>
                        <th>Rodzaj</th>
                        <th>Opakowanie</th>
                        <th>Cena</th>
                        <th>Na stanie</th>
                        <th>Akcje</th>
                    </tr>
                    </thead>

                    <tbody>

                    <cor:forEach items="${medicineStatePharmaciesDivideOnPieces.get(processedPharmacy)}"
                                 var="currentMedicine">
                        <tr>
                            <td>${currentMedicine.id}</td>
                            <td>${currentMedicine.name}</td>
                            <td>${currentMedicine.dose} ${currentMedicine.doseUnit}</td>
                            <td>${currentMedicine.kind}</td>
                            <td>${currentMedicine.capacity}</td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" value="${currentMedicine.price}"/>
                                zł
                            </td>
                            <td>${currentMedicine.count}</td>
                            <td>
                            <td>
                                <a class="dropdown-button tooltipped modal-trigger" href="#modal2"
                                   data-position="bottom"
                                   data-delay="50"
                                   data-tooltip="Szczegóły"
                                   onclick="getDetails(event,${pharmaciesList.get(processedPharmacy).id},${currentMedicine.id},2)">
                                    <i class="small material-icons">info</i>
                                </a>
                            </td>
                        </tr>
                    </cor:forEach>

                    </tbody>
                </table>

                <%--<ul class="pagination">
                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="active"><a href="#!">1</a></li>
                    <li class="waves-effect"><a href="#!">2</a></li>
                    <li class="waves-effect"><a href="#!">3</a></li>
                    <li class="waves-effect"><a href="#!">4</a></li>
                    <li class="waves-effect"><a href="#!">5</a></li>
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                </ul>--%>

                <ul class="pagination">
                    <cor:forEach var="counter" begin="1" end="${medicineStatePagination.get(processedPharmacy)}">
                        <cor:if test="${counter == 1}">
                            <li class="active waves-effect tablePagination${processedPharmacy}"><a
                                    onclick="changePharmacyList(this,${counter},${processedPharmacy},'tableAnother${processedPharmacy}', ${pharmaciesList.get(processedPharmacy).id})">${counter}</a>
                            </li>
                        </cor:if>
                        <cor:if test="${counter > 1}">
                            <li class="waves-effect tablePagination${processedPharmacy}"><a
                                    onclick="changePharmacyList(this,${counter},${processedPharmacy},'tableAnother${processedPharmacy}', ${pharmaciesList.get(processedPharmacy).id})">${counter}</a>
                            </li>
                        </cor:if>
                    </cor:forEach>
                </ul>
            </cor:if>
            <br><br><br><br>
        </cor:if>

    </cor:forEach>


    <%--MODALS--%>
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <div id="modalLoader1" class="preloader-wrapper active"
                 style="position: fixed; z-index: 10; right: 47%; top: 20%;">
                <div class="spinner-layer spinner-red-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <h4>Szczegóły:</h4>
            <table id="detailsTable" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>Nr. seryjny</th>
                    <th>Data ważności</th>
                    <th>Akcje</th>
                </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zamknij</a>
        </div>
    </div>


    <div id="modal2" class="modal modal-fixed-footer">
        <div class="modal-content">
            <div id="modalLoader2" class="preloader-wrapper active"
                 style="position: fixed; z-index: 10; right: 47%; top: 20%;">
                <div class="spinner-layer spinner-red-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <h4>Szczegóły:</h4>
            <table id="detailsTable2" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>Nr. seryjny</th>
                    <th>Data ważności</th>
                    <th>Akcje</th>
                </tr>
                </thead>

                <tbody>

                <cor:forEach var="i" begin="1" end="15">
                    <tr>
                        <td>${152051 + i}</td>
                        <td>2017-10-28</td>
                        <td>
                            <a class="dropdown-button tooltipped" href="#" data-position="bottom" data-delay="50"
                               data-tooltip="Przenieś do obecnej apteki!">
                                <i class="small material-icons">play_for_work</i>
                            </a>
                            <a class="dropdown-button tooltipped modal-trigger" href="#modal4" data-position="bottom"
                               data-delay="50"
                               data-tooltip="Utylizuj">
                                <i class="small material-icons">delete</i>
                            </a>
                        </td>
                    </tr>
                </cor:forEach>

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zamknij</a>
        </div>
    </div>

    <div id="typeState_modal" class="modal modal-fixed-footer">
        <div class="modal-content">
            <div id="typeState_modal_loader" class="preloader-wrapper active"
                 style="position: fixed; z-index: 10; right: 47%; top: 20%;">
                <div class="spinner-layer spinner-red-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <h4>Wybór apteki:</h4>
            <table id="type_table" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa apteki</th>
                    <th>Na stanie</th>
                    <th>Zamów</th>
                    <th>Akcje</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <br>
            <div class="center-align" id="typeState_info">
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zamknij</a>
        </div>
    </div>

    <div id="wholesaler_modal" class="modal modal-fixed-footer">
        <div class="modal-content">
            <div id="wholesaler_modal_loader" class="preloader-wrapper active"
                 style="position: fixed; z-index: 10; right: 47%; top: 20%;">
                <div class="spinner-layer spinner-red-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <h4>Zamówienie z hurtowni:</h4>
            <table id="wholesale_table" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>Hurtownia</th>
                    <th>Zamów</th>
                    <th>Akcje</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Katowice</td>
                    <td>
                        <div class="input-field col s3">
                            <input type="number" min="0" max="100" value="1">
                            <label for="drugName">Zamawiana liczba:</label>
                        </div>
                    </td>
                    <td>
                        <a class="dropdown-button addedTooltip tooltipped" href="#" data-position="bottom" data-delay="50"
                        data-tooltip="Zamów"><i class="small material-icons" onclick="getMedicinesFromWholesale(event);">file_download</i></a>
                    </td>
                </tr>
                </tbody>
            </table>
            <br>
            <div class="center-align" id="wholesale_info">
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zamknij</a>
        </div>
    </div>
</div>


<div id="utilize_modal" class="modal">
    <div class="modal-content">
        <div id="utilize_modal_loader" class="preloader-wrapper active"
             style="position: fixed; z-index: 10; right: 47%; top: 20%;">
            <div class="spinner-layer spinner-red-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <h4>Utylizacja</h4>
        <br>
        <div class="center-align"><h5>Czy na pewno chcesz zutylizować lek?</h5></div>
        <br>
        <div class="row center-align" id="confirmation_button">
            <a class="modal-action modal-close waves-effect waves-light btn orange">tak</a>
            <a class="modal-action modal-close waves-effect waves-light btn orange">nie</a>
        </div>
    </div>
</div>

<div id="info_modal" class="modal">
    <div class="modal-content">
        <div class="center-align" id="info_text"><h5>Operacja zakończona pomyślnie</h5></div>
    </div>
</div>

<script>
    var home = document.getElementById("homeUnderline");
    home.className += "active";

    var order = document.getElementById("orderUnderline");
    order.classList.remove("active");

    var request = document.getElementById("requestUnderline");
    request.classList.remove("active");

    var report = document.getElementById("reportUnderline");
    report.classList.remove("active");

    var manage = document.getElementById("manageUnderline");
    manage.classList.remove("active");
</script>


<%-- AJAX --%>
<%--===============================Details===============================--%>
<script type="text/javascript" charset="utf-8">

    var lastTarget;
    var lastRow;
    var countCell;
    var detailRow;

    function getDetails(event, pharmacyId, typeId2, detailsTableNumber) {
        showLoaderModal();

        console.log("Start");
        console.log("Pharmacy id: " + pharmacyId);
        console.log("Medicine id: " + typeId2);

        var json = {"action": "getDetails", "pharmacyId": pharmacyId, "typeId": typeId2};

        lastTarget = event.target;

        var row = event.target.parentElement.parentElement.parentElement;
        typeId = $(row.cells[0]).html();

        lastRow = event.target.parentElement.parentElement.parentElement;
        var allCells = lastRow.cells;
        countCell = allCells[6];
        console.log($(countCell).html());

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                var medicines = JSON.parse(response);

                var table;

                if (detailsTableNumber == 1) {
                    table = document.getElementById("detailsTable");
                } else if (detailsTableNumber == 2) {
                    table = document.getElementById("detailsTable2");

                }
                deleteRows(table);
                table = table.tBodies[0];

                for (i = 0; i < medicines.length; i++) {
                    var row = table.insertRow(-1);

                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    var cell3 = row.insertCell(2);

                    cell1.innerHTML = medicines[i].serial;
                    cell2.innerHTML = medicines[i].expiryDate;
                    cell3.innerHTML = getActionsButton(event, detailsTableNumber, medicines[i].serial, pharmacyId, typeId2);
                }

                $('.addedTooltip').tooltip({delay: 50});

                hideLoaderModal();
            },
            error: function () {
                hideLoaderModal();
            }
        });
    }

    function deleteRows(table) {
        var rowCount = table.rows.length;
        for (var i = rowCount - 1; i > 0; i--) {
            table.deleteRow(i);
        }
    }

    function getActionsButton(event, detailsTableNumber, serial, pharmacyId, typeId) {

        var actionButtons;


        if (detailsTableNumber == 1) {
            actionButtons = '<a class="dropdown-button tooltipped addedTooltip" href="#" data-position="bottom" data-delay="50" ' +
                'data-tooltip="Sprzedaj" onclick="sold(' + serial + ', event);"> ' +
                '<i class="small material-icons">attach_money</i> ' +
                '</a> ' +
                '<a class="dropdown-button tooltipped modal-trigger addedTooltip" href="#modal4" data-position="bottom" ' +
                'data-delay="50" ' +
                'data-tooltip="Utylizuj" onclick="utilizeConfirmation(' + pharmacyId + ',' + typeId + ',' + serial + ', event);"> ' +
                '<i class="small material-icons">delete</i> ' +
                '</a>';
        } else if (detailsTableNumber == 2) {
            actionButtons = '<a class="dropdown-button tooltipped addedTooltip" data-position="bottom" data-delay="50" ' +
                'data-tooltip="Przenieś do obecnej apteki!" onclick="moveToCurrentPharmacy(event);" > ' +
                '<i class="small material-icons">play_for_work</i> ' +
                '</a> ' +
                '<a class="dropdown-button tooltipped modal-trigger addedTooltip" data-position="bottom" ' +
                'data-delay="50" ' +
                'data-tooltip="Utylizuj" onclick="utilizeConfirmation(' + pharmacyId + ',' + typeId + ',' + serial + ', event);"> ' +
                '<i class="small material-icons">delete</i> ' +
                '</a> ';
        }
        return actionButtons;
    }

    function sold(serial, event) {
        console.log("Sold start");
        showLoaderModal();
        var info_text = document.getElementById("info_text");

        var json = {"action": "sold", "serial": serial};

        detailRow = event.target.parentElement.parentElement.parentElement;

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                console.log("sold end");

                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');

                var count = $(countCell).html()
                if (count > 1) {
                    countCell.innerHTML = '<td>' + (count - 1) + '</td>';
                } else {
                    lastRow.remove();
                }

                detailRow.remove();


                hideLoaderModal();
            },
            error: function () {
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');

                hideLoaderModal();
            }
        });
    }

    function utilizeConfirmation(pharmacyId, typeId, serial, event) {
        detailRow = event.target.parentElement.parentElement.parentElement;

        var confirmation_button = document.getElementById("confirmation_button");
        confirmation_button.innerHTML = '<a class="modal-action modal-close waves-effect waves-light btn orange" onclick="utilize(' + pharmacyId + ',' + typeId + ',' + serial + ');">tak</a> ' +
            '<a class="modal-action modal-close waves-effect waves-light btn orange">nie</a> ';

        $('#utilize_modal').modal('open');

    }

    function utilize(pharmacyId, typeId, serial, event) {
        console.log("Utilize start");
        showLoaderModal();

        var json = {"action": "utilize", "serial": serial, "pharmacyName": pharmacyId, "typeId": typeId};

        console.log("Utilize serial: " + serial);

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                console.log("utilize end");

                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');

                var count = $(countCell).html()
                if (count > 1) {
                    console.log("Something stayed");
                    countCell.innerHTML = '<td>' + (count - 1) + '</td>';
                } else {
                    console.log("Everything deleted");
                    lastRow.remove();
                }

                detailRow.remove();

                hideLoaderModal();
            },
            error: function () {
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');

                hideLoaderModal();
            }
        });
    }
</script>

<script>
    function showLoaderModal() {
        var loader = document.getElementById("modalLoader1");
        loader.style.visibility = "visible";
        var loader2 = document.getElementById("modalLoader2");
        loader2.style.visibility = "visible";
        var loader3 = document.getElementById("utilize_modal_loader");
        loader3.style.visibility = "visible";
        var loader4 = document.getElementById("typeState_modal_loader");
        loader4.style.visibility = "visible";
        var loader5 = document.getElementById("wholesaler_modal_loader");
        loader5.style.visibility = "visible";
    }

    function hideLoaderModal() {
        var loader = document.getElementById("modalLoader1");
        loader.style.visibility = "hidden";
        var loader2 = document.getElementById("modalLoader2");
        loader2.style.visibility = "hidden";
        var loader3 = document.getElementById("utilize_modal_loader");
        loader3.style.visibility = "hidden";
        var loader4 = document.getElementById("typeState_modal_loader");
        loader4.style.visibility = "hidden";
        var loader5 = document.getElementById("wholesaler_modal_loader");
        loader5.style.visibility = "hidden";
    }

    function changePharmacyList(event, page, tableNr, tableId, pharmacyId) {

        var json = {"action": "pagination", "page": page, "table": tableNr, "tableId": tableId};

        var target = event.target;

        var pages = document.getElementsByClassName("tablePagination" + tableNr);
        for (i = 0; i < pages.length; i++) {
            pages[i].classList.remove("active");
        }
        event.parentNode.classList.add("active");

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {

                var medicines = JSON.parse(response);


                var table;

                if (tableId === "tableCurrent") {
                    table = document.getElementById("tableCurrent");
                } else {
                    table = document.getElementById(tableId);
                }
                deleteRows(table);
                table = table.tBodies[0];

                for (i = 0; i < medicines.length; i++) {

                    var row = table.insertRow(-1);
                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    var cell3 = row.insertCell(2);
                    var cell4 = row.insertCell(3);
                    var cell5 = row.insertCell(4);
                    var cell6 = row.insertCell(5);
                    var cell7 = row.insertCell(6);
                    var cell8 = row.insertCell(7);

                    cell1.innerHTML = medicines[i].id;
                    cell2.innerHTML = medicines[i].name;
                    cell3.innerHTML = medicines[i].dose + ' ' + medicines[i].doseUnit;
                    cell4.innerHTML = medicines[i].kind;
                    cell5.innerHTML = medicines[i].capacity;
                    cell6.innerHTML = medicines[i].price + ' zł';
                    cell7.innerHTML = medicines[i].count;

                    console.log("Table nr: " + tableNr);
                    console.log("PharmacyID: " + pharmacyId);

                    cell8.innerHTML = getActionButtonsInTable(tableId, pharmacyId, medicines[i].id);
                }
                $('.addedTooltip').tooltip({delay: 50});
            },

            error: function () {
            }
        });
    }

    function getActionButtonsInTable(tableId, pharmacyName, medicineId) {
        var actionButtons;

        var medicineImportList = ${medicineIL_json};
        var val1 = medicineImportList[medicineId] == 1 ? "flicker" : "";
        var val2 = medicineImportList[medicineId] == 2 ? "flicker" : "";

        if (tableId === "tableCurrent") {
            actionButtons = '<a class="dropdown-button tooltipped modal-trigger addedTooltip" href="#modal1" data-position="bottom" ' +
                'data-delay="50" data-tooltip="Szczegóły" ' +
                'onclick="getDetails(event,' + pharmacyName + ',' + medicineId + ',1)"> ' +
                '<i class="small material-icons">info</i> ' +
                '</a>' +
                '<a class="dropdown-button tooltipped modal-trigger addedTooltip '+ val2 +'" href="#wholesaler_modal" data-position="bottom" data-delay="50" ' +
                'data-tooltip="Zamów z hurtowni"  onclick="getMedicinesFromWholesale_prepare(event);"> ' +
                '<i class="small material-icons">reply</i> ' +
                '</a>' +
                '<a class="dropdown-button tooltipped modal-trigger addedTooltip '+ val1 +'" href="#typeState_modal" data-position="bottom" data-delay="50" ' +
                'data-tooltip="Zamów z innej apteki" onclick="getMedicinesFromAnotherPharmacy(event);"> ' +
                '<i class="small material-icons">reply_all</i> ' +
                '</a>';
        } else {
            actionButtons = '<a class="dropdown-button tooltipped modal-trigger addedTooltip" href="#modal2" data-position="bottom" ' +
                'data-delay="50" data-tooltip="Szczegóły" ' +
                'onclick="getDetails(event,' + pharmacyName + ',' + medicineId + ',2)"> ' +
                '<i class="small material-icons">info</i> ' +
                '</a>';
        }
        return actionButtons;
    }

    var typeId;
    var currentPharmacy = ${currentPharmacy};
    var typeState_info = document.getElementById("typeState_info");

    function getMedicinesFromAnotherPharmacy(event) {
        showLoaderModal();

        var row = event.target.parentElement.parentElement.parentElement;
        typeId = $(row.cells[0]).html();

        var json = {"action": "getMedicinesFromAnotherPharmacy", "typeId": typeId};

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                var medicines = JSON.parse(response);

                var table = document.getElementById("type_table");

                typeState_info.innerHTML = "";
                deleteRows(table);
                table = table.tBodies[0];

                if (medicines.length > 0) {
                    for (i = 0; i < medicines.length; i++) {
                        var row = table.insertRow(-1);

                        var cell1 = row.insertCell(0);
                        var cell2 = row.insertCell(1);
                        var cell3 = row.insertCell(2);
                        var cell4 = row.insertCell(3);
                        var cell5 = row.insertCell(4);

                        cell1.innerHTML = medicines[i].pharmacyId;
                        cell2.innerHTML = medicines[i].pharmacyName;
                        cell3.innerHTML = medicines[i].numberOfMedicine;
                        cell4.innerHTML = '<div class="input-field col s3"> ' +
                            '<input type="number" min="0" max="100"> ' +
                            '<label for="drugName">Zamawiana liczba:</label> ' +
                            '</div> ';
                        cell5.innerHTML = '<a class="dropdown-button addedTooltip tooltipped" href="#" data-position="bottom" data-delay="50" ' +
                            ' data-tooltip="Zamów"><i class="small material-icons" onclick="getMedicinesFromAnotherPharmacyPart2(event);">file_download</i></a> ';
                    }

                    $('.addedTooltip').tooltip({delay: 50});
                } else {
                    typeState_info.innerHTML = "<h4>Brak wybranego leku w innych aptekach!</h4>";
                }

                hideLoaderModal();
            },
            error: function () {
                hideLoaderModal();
            }
        });
    }

    function getMedicinesFromAnotherPharmacyPart2(event) {
        showLoaderModal();

        var row = event.target.parentElement.parentElement.parentElement;
        var warehouse_dst = $(row.cells[0]).html();
        var number = row.getElementsByTagName("input")[0].value;

        var json = {
            "action": "getMedicinesFromAnotherPharmacyPart2", "typeId": typeId, "warehouse_src": currentPharmacy,
            "warehouse_dst": warehouse_dst, "number": number, "status": 1
        };

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');
            },
            error: function () {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');
            }
        });
    }

    function getMedicinesFromWholesale_prepare(event) {
        var row = event.target.parentElement.parentElement.parentElement;
        typeId = $(row.cells[0]).html();
        console.log(typeId);
        hideLoaderModal();
    }

    function getMedicinesFromWholesale(event) {
        showLoaderModal();

        var row = event.target.parentElement.parentElement.parentElement;
        var number = row.getElementsByTagName("input")[0].value;

        var json = {
            "action": "getMedicinesFromWholesale", "typeId": typeId, "warehouse_src": 4,
            "warehouse_dst": currentPharmacy, "number": number, "status": 1
        };

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');
            },
            error: function () {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');
            }
        });
    }

    function moveToCurrentPharmacy(event) {
        var row = event.target.parentElement.parentElement.parentElement;
        var serial = $(row.cells[0]).html();
        console.log(serial);
        console.log(typeId);

        var json = {
            "action": "moveToCurrentPharmacy", "typeId": typeId,
            "warehouse_src": currentPharmacy, "serial": serial
        };

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');
            },
            error: function () {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');
            }
        });
    }

    function setDateAndTime(dateSpan) {
        var nazwy_mies = ['Stycznia', 'Lutego', 'Marca', 'Kwietnia', 'Maja',
            'Czerwca', 'Lipca', 'Sierpnia', 'Września', 'Października',
            'Listopada', 'Grudnia'];

        var currentDate = "${currentDate}";
        var currentTime = "${currentTime}";

        var rok = currentDate.toString().substring(0,4);
        var mies = (currentDate-1).toString().substring(4,6);
        var dzien = currentDate.toString().substring(6,8);
        var godz = currentTime.toString().substring(0,2);
        var min = currentTime.toString().substring(2,4);
        var sec = currentTime.toString().substring(4,6);


        var data_i_czas = dzien + ' ' + nazwy_mies[mies-1] + ' ' + rok
            + '\n ' + godz + ':' + min + ':' + sec;

        dateSpan.innerHTML = data_i_czas;
    }

</script>
