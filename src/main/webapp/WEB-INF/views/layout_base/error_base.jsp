<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<html>
<head>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="<c:url value="/resources/images/pharmacyIcon_fav.png" />" rel="icon">

    <link href="<c:url value="/resources/css/materialize.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/materialize.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/custom.css" />" rel="stylesheet">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <%--Skrypty JavaScript--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="<c:url value="/resources/js/materialize.js" />"></script>
    <script src="<c:url value="/resources/js/dateAndTime.js" />"></script>
    <script src="<c:url value="/resources/js/operations.js" />"></script>
</head>
<body style="background: orange;">

<div id="content">
    <t:insertAttribute name="body"></t:insertAttribute>
</div>

<%--Skrypty JavaScript--%>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/js/materialize.js" />"></script>
<script src="<c:url value="/resources/js/dateAndTime.js" />"></script>
<script src="<c:url value="/resources/js/operations.js" />"></script>-->

</body>
</html>
