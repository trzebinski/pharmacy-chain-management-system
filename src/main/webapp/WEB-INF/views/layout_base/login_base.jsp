<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>

<html>
<head>
    <title>Apteka</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="<c:url value="/resources/images/pharmacyIcon_fav.png" />" rel="icon">

    <link href="<c:url value="/resources/css/materialize.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/materialize.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/custom.css" />" rel="stylesheet">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body id="page" style="background: #000;">


<div id="content">
    <t:insertAttribute name="body"/>
</div>

<%--Skrypty JavaScript--%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/js/materialize.js" />"></script>

</body>
</html>
