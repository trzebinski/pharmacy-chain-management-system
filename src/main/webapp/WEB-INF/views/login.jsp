<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="loader" class="preloader-wrapper active" style="position: fixed; z-index: 10; right: 47%; top: 15%;">
    <div class="spinner-layer spinner-red-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
        <div class="circle"></div>
    </div><div class="circle-clipper right">
        <div class="circle"></div>
    </div>
    </div>
</div>

<div class="iris">
    <div class="card transparent z-depth-5">
        <%--<div class="container ">--%>
        <%--<div class="valign-wrapper row login-box">--%>
        <div class="valign-wrapper row login-box">
            <div class="col card hoverable s10 pull-s1 m6 pull-m3 l4 pull-l4">
                <sf:form method="post">
                    <div class="card-content">
                        <span class="card-title">Logowanie</span>

                        <c:if test="${error == true}">
                            <span style="color: red;">Błędne dane logowania!</span>
                        </c:if>

                        <div class="row">
                            <div class="input-field col s12">
                                <label for="login">Nazwa użytkownika</label>
                                <input type="text" class="validate" name="login" id="login"/>
                            </div>
                            <div class="input-field col s12">
                                <label for="password">Hasło</label>
                                <input type="password" class="validate" name="password" id="password"/>
                            </div>
                        </div>
                    </div>
                    <div class="card-action right-align">
                        <input type="reset" id="reset" class="btn-flat">
                        <input type="submit" class="btn orange" value="Login">
                    </div>
                </sf:form>
            </div>
        </div>
    </div>
</div>


<script>

    window.onload = function init()
    {
        hideLoader();
    };

    window.onbeforeunload = function(){
        showLoader();
    };


    function showLoader(){
        var loader = document.getElementById("loader");
        loader.style.visibility = "visible";
    }

    function hideLoader(){
        var loader = document.getElementById("loader");
        loader.style.visibility = "hidden";
    }
</script>
