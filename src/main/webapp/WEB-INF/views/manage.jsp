<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="cor" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">

    <cor:forEach var="processedPharmacy" begin="0" end="${pharmaciesList.size()-1}">
        <cor:if test="${pharmaciesList.get(processedPharmacy).id == currentPharmacy}">
            <h4>Uzupełnienie braków:</h4>
            <hr>

            <table id="supplementOfDeficiencies" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th>Dawka</th>
                    <th>Rodzaj</th>
                    <th>Opakowanie</th>
                    <th>Na stanie</th>
                    <th>Stan docelowy</th>
                    <th>Brakuje</th>
                    <th>Akcje</th>
                    </tr>
                </thead>

                <tbody>
                <cor:forEach items="${medicineStatePharmaciesDivideOnPieces}" var="currentMedicine">
                    <tr>
                        <td>${currentMedicine.id}</td>
                        <td>${currentMedicine.name}</td>
                        <cor:choose>
                            <cor:when test="${currentMedicine.dose==0}">
                                <td> </td>
                            </cor:when>
                            <cor:otherwise>
                                <td>${currentMedicine.dose} ${currentMedicine.doseUnit}</td>
                            </cor:otherwise>
                        </cor:choose>
                        <td>${currentMedicine.kind}</td>
                        <td>${currentMedicine.capacity}</td>
                        <td>${currentMedicine.count}</td>
                        <td>${currentMedicine.demandedCount}</td>
                        <td>${currentMedicine.deficite}</td>
                        <td>
                            <a class="dropdown-button tooltipped modal-trigger" href="#modal2" data-position="bottom"
                               data-delay="50"
                               data-tooltip="Szczegóły"
                               onclick="getDetails(this,${pharmaciesList.get(processedPharmacy).id},${currentMedicine.id},1, null, null, null)">
                                <i class="small material-icons">info</i>
                            </a>
                            <a class="dropdown-button tooltipped modal-trigger ${(medicineImportList.get(currentMedicine.id)) == 2 ? "flicker" : ""}" href="#modal4" data-position="bottom"
                               data-delay="50"
                               data-tooltip="Zamów z hurtowni"
                               onclick="getMedicinesFromWholesale_prepare(event);">
                                <i class="small material-icons">reply</i>
                            </a>
                            <a class="dropdown-button tooltipped modal-trigger ${(medicineImportList.get(currentMedicine.id)) == 1 ? "flicker" : ""}" href="#modal3" data-position="bottom"
                               data-delay="50"
                               data-tooltip="Zamów z innej apteki"
                               onclick="getMedicinesFromAnotherPharmacy(event)">
                                <i class="small material-icons">reply_all</i>
                            </a>
                        </td>
                    </tr>
                </cor:forEach>
            </tbody>
            </table>

            <ul class="pagination">
                <cor:forEach var="counter" begin="1" end="${medicineStatePagination}">
                    <cor:if test="${counter == 1}">
                        <li class="active waves-effect tablePagination1"><a onclick="changePharmacyList(this,${counter},1)">${counter}</a></li>
                    </cor:if>
                    <cor:if test="${counter > 1}">
                        <li class="waves-effect tablePagination1"><a onclick="changePharmacyList(this,${counter},1)">${counter}</a></li>
                    </cor:if>
                </cor:forEach>
            </ul>
        </cor:if>
    </cor:forEach>

    <br><br><br>
    <h4>Żądania z innych aptek:</h4>
    <hr>

    <cor:forEach var="processedPharmacy" begin="0" end="${pharmaciesList.size()-1}">
        <cor:if test="${pharmaciesList.get(processedPharmacy).id == currentPharmacy}">

            <table id = "requestsFromAnotherPharmacies" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th>Dawka</th>
                    <th>Rodzaj</th>
                    <th>Opakowanie</th>
                    <th>Na stanie</th>
                    <th>Apteka</th>
                    <th>Priorytet</th>
                    <th>Żądana liczba</th>
                    <th>Akcje</th>
                </tr>
                </thead>

                <tbody>
                <cor:forEach items="${requestToGivenPharmaciesDivideOnPieces}" var="currentMedicine">
                <tr>
                    <td>${currentMedicine.id}</td>
                    <td>${currentMedicine.name}</td>
                    <td>${currentMedicine.dose} ${currentMedicine.doseUnit}</td>
                    <td>${currentMedicine.kind}</td>
                    <td>${currentMedicine.capacity}</td>
                    <td>${currentMedicine.state}</td>
                    <td>${currentMedicine.pharmacy}</td>
                    <td>${currentMedicine.priority}</td>
                    <td>${currentMedicine.number}</td>
                    <td>
                        <a class="dropdown-button tooltipped modal-trigger ${(requestDecisionList.get(currentMedicine.requestId)) == 1 ? "flicker" : ""}" data-position="bottom"
                           data-delay="50"
                           data-tooltip="Zatwierdź"
                           onclick="acceptRequestConfirmation(event, ${currentMedicine.requestId}, ${currentMedicine.id}, ${currentPharmacy}, ${currentMedicine.pharmacySourceId}, ${currentMedicine.state}, ${currentMedicine.number})">
                            <i class="small material-icons">check</i>
                        </a>
                        <a class="dropdown-button tooltipped modal-trigger ${(requestDecisionList.get(currentMedicine.requestId)) == 0 ? "flicker" : ""}" href="#modal7" data-position="bottom"
                           data-delay="50"
                           data-tooltip="Odmów"
                           onclick="refuse(${currentMedicine.requestId})">
                            <i class="small material-icons">close</i>
                        </a>
                        <a class="dropdown-button tooltipped modal-trigger" href="#modal1" data-position="bottom"
                           data-delay="50"
                           data-tooltip="Szczegóły"
                           onclick="getDetails(this,${pharmaciesList.get(processedPharmacy).id},${currentMedicine.id},2, ${currentMedicine.pharmacySourceId}, ${currentMedicine.state}, ${currentMedicine.number})">
                            <i class="small material-icons">info</i>
                        </a>
                    </td>
                </tr>
                </cor:forEach>
                </tbody>
            </table>

            </ul>
            <ul class="pagination">
                <cor:forEach var="counter" begin="1" end="${requestToPagination}">
                    <cor:if test="${counter == 1}">
                        <li class="active waves-effect tablePagination2"><a onclick="changePharmacyList(this,${counter},2)">${counter}</a></li>
                    </cor:if>
                    <cor:if test="${counter > 1}">
                        <li class="waves-effect tablePagination2"><a onclick="changePharmacyList(this,${counter},2)">${counter}</a></li>
                    </cor:if>
                </cor:forEach>
            </ul>
        </cor:if>
    </cor:forEach>

    <br><br><br>
    <h4>Przenieś leki z krótką datą ważności:</h4>
    <hr>

    <cor:forEach var="processedPharmacy" begin="0" end="${pharmaciesList.size()-1}">
        <cor:if test="${pharmaciesList.get(processedPharmacy).id == currentPharmacy}">
        <table id="shortBestBeforeDate" class="bordered striped highlight responsive-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nazwa</th>
                <th>Nr seryjny</th>
                <th>Dawka</th>
                <th>Rodzaj</th>
                <th>Opakowanie</th>
                <th>Data ważności</th>
                <th>Akcje</th>
            </tr>
            </thead>

            <tbody>
            <cor:forEach items="${medicineWithShortBestBeforeDateDivideOnPieces}" var="currentMedicine">
                <tr>
                    <td>${currentMedicine.id}</td>
                    <td>${currentMedicine.name}</td>
                    <td>${currentMedicine.serial}</td>
                    <td>${currentMedicine.dose} ${currentMedicine.doseUnit}</td>
                    <td>${currentMedicine.kind}</td>
                    <td>${currentMedicine.capacity}</td>
                    <td>${currentMedicine.expirationDate}</td>
                    <td>
                        <a class="dropdown-button tooltipped modal-trigger ${(sendOrUtilizeList.get(currentMedicine.serial)) == 2 ? "flicker" : ""}" href="#modal5" data-position="bottom"
                           data-delay="50"
                           data-tooltip="Wyślij do innej apteki"
                            onclick="saveMedicineToSend(event, ${currentMedicine.serial}, ${currentMedicine.id})">
                            <i class="small material-icons">redo</i>
                        </a>
                        <a class="dropdown-button tooltipped modal-trigger ${(sendOrUtilizeList.get(currentMedicine.serial)) == 1 ? "flicker" : ""}" onclick="utilizeConfirmation(${currentMedicine.serial}, event, ${currentMedicine.id})" data-position="bottom"
                           data-delay="50"
                           data-tooltip="Utylizuj">
                            <i class="small material-icons">delete</i>
                        </a>
                    </td>
                </tr>
            </cor:forEach>
            </tbody>
        </table>

            <ul class="pagination">
                <cor:forEach var="counter" begin="1" end="${shortBestBeforePagination}">
                    <cor:if test="${counter == 1}">
                        <li class="active waves-effect tablePagination3"><a onclick="changePharmacyList(this,${counter},3)">${counter}</a></li>
                    </cor:if>
                    <cor:if test="${counter > 1}">
                        <li class="waves-effect tablePagination3"><a onclick="changePharmacyList(this,${counter},3)">${counter}</a></li>
                    </cor:if>
                </cor:forEach>
            </ul>

        </cor:if>
    </cor:forEach>

    <br><br><br>
    <h4>Żądania do innych aptek:</h4>
    <hr>

    <cor:forEach var="processedPharmacy" begin="0" end="${pharmaciesList.size()-1}">
        <cor:if test="${pharmaciesList.get(processedPharmacy).id == currentPharmacy}">
            <table id="requestFromPharmacy" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th>Dawka</th>
                    <th>Rodzaj</th>
                    <th>Opakowanie</th>
                    <th>Apteka</th>
                    <th>Priorytet</th>
                    <th>Żądana liczba</th>
                    <th>Status</th>
                </tr>
                </thead>

                <tbody>
                <cor:forEach items="${requestFromPharmacyDivideOnPieces}" var="currentMedicine">
                    <tr>
                        <td>${currentMedicine.id}</td>
                        <td>${currentMedicine.name}</td>
                        <td>${currentMedicine.dose} ${currentMedicine.doseUnit}</td>
                        <td>${currentMedicine.kind}</td>
                        <td>${currentMedicine.capacity}</td>
                        <td>${currentMedicine.pharmacy}</td>
                        <td>${currentMedicine.priority}</td>
                        <td>${currentMedicine.number}</td>
                        <td>${currentMedicine.info}</td>
                    </tr>
                </cor:forEach>
                </tbody>
            </table>

            <ul class="pagination">
                <cor:forEach var="counter" begin="1" end="${requestFromPagination}">
                    <cor:if test="${counter == 1}">
                        <li class="active waves-effect tablePagination4"><a onclick="changePharmacyList(this,${counter},4)">${counter}</a></li>
                    </cor:if>
                    <cor:if test="${counter > 1}">
                        <li class="waves-effect tablePagination4"><a onclick="changePharmacyList(this,${counter},4)">${counter}</a></li>
                    </cor:if>
                </cor:forEach>
            </ul>

        </cor:if>
    </cor:forEach>
    <br><br><br>

    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Szczegóły:</h4>
            <table id="detailsTable2" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>Nr. seryjny</th>
                    <th>Data ważności</th>
                    <th>Akcje</th>
                </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zamknij</a>
        </div>
    </div>

    <div id="modal2" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Szczegóły:</h4>
            <table id="detailsTable1" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>Nr. seryjny</th>
                    <th>Data ważności</th>
                    <th>Status</th>
                </tr>
                </thead>

                <tbody>

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zamknij</a>
        </div>
    </div>

    <div id="modal3" class="modal modal-fixed-footer">
        <div class="modal-content">
            <div id="modalLoader3" class="preloader-wrapper active"
                 style="position: fixed; z-index: 10; right: 47%; top: 20%;">
                <div class="spinner-layer spinner-red-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <h4>Wybór apteki:</h4>
            <table id="type_table" class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa apteki</th>
                    <th>Na stanie</th>
                    <th>Zamów</th>
                    <th>Akcje</th>
                </tr>
                </thead>

                <tbody>
                <!--
                %<cor:forEach var="i" begin="1" end="3">
                    <tr>
                        <td>Apteka${i}</td>
                        <td>${5+i}</td>
                        <td>
                            <div class="input-field col s3">
                                <input type="number" min="0" max="${5+i}">
                                <label>Zamawiana liczba:</label>
                            </div>
                        </td>
                        <td>
                            <a class="dropdown-button tooltipped" href="#" data-position="bottom" data-delay="50"
                               data-tooltip="Zamów">
                                <i class="small material-icons">file_download</i>
                            </a>
                        </td>
                    </tr>
                </cor:forEach>-->
                </tbody>
            </table>
            <br>
            <div class="center-align" id="typeState_info">
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zamknij</a>
        </div>
    </div>

    <div id="modal4" class="modal modal-fixed-footer">
        <div class="modal-content">
            <div id="modalLoader4" class="preloader-wrapper active"
                 style="position: fixed; z-index: 10; right: 47%; top: 20%;">
                <div class="spinner-layer spinner-red-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <h4>Wybór hurtowni:</h4>
            <table class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>Hurtownia</th>
                    <th>Zamów</th>
                    <th>Akcje</th>
                </tr>
                </thead>

                <tbody>

                <tr>
                    <td>Katowice</td>
                    <td>
                        <div class="input-field col s3">
                            <input type="number" min="0" max="100" value="1">
                            <label>Zamawiana liczba:</label>
                        </div>
                    </td>
                    <td>
                        <a class="dropdown-button addedTooltip tooltipped" href="#" data-position="bottom" data-delay="50"
                           data-tooltip="Zamów"><i class="small material-icons" onclick="getMedicinesFromWholesale(event);">file_download</i></a>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zamknij</a>
        </div>
    </div>

    <div id="modal5" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Wybór apteki:</h4>
            <table class="bordered striped highlight responsive-table">
                <thead>
                <tr>
                    <th>Apteka</th>
                    <th>Akcje</th>
                </tr>
                </thead>

                <tbody>

                <cor:forEach items="${pharmaciesList}" var="processedPharmacy">
                    <cor:if test="${processedPharmacy.id != currentPharmacy}">
                        <tr>
                            <td>${processedPharmacy.name}</td>
                            <td>
                                <a class="dropdown-button tooltipped" href="#" data-position="bottom" data-delay="50"
                                   data-tooltip="Wyślij"
                                   onclick="sendToPharmacyConfirmation(${currentPharmacy}, ${processedPharmacy.id})">
                                    <i class="small material-icons">check</i>
                                </a>
                            </td>
                        </tr>
                    </cor:if>
                </cor:forEach>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Zamknij</a>
        </div>
    </div>
</div>


<div id="utilize_modal" class="modal">
    <div class="modal-content">
        <h4>Utylizacja</h4>
        <br>
        <div class="center-align"><h5>Czy na pewno chcesz zutylizować lek?</h5></div>
        <br>
        <div class="row center-align" id="confirmation_button">
            <a class="modal-action modal-close waves-effect waves-light btn orange" onclick="utilize(event);">tak</a>
            <a class="modal-action modal-close waves-effect waves-light btn orange">nie</a>
        </div>

    </div>
</div>

<div id="sendToPharmacy_modal" class="modal">
    <div class="modal-content">
        <h4>Wyślij do apteki</h4>
        <br>
        <div class="center-align"><h5>Czy na pewno chcesz wysłać lek?</h5></div>
        <br>
        <div class="row center-align" id="send_confirmation_button">
            <a class="modal-action modal-close waves-effect waves-light btn orange" onclick="sendToPharmacy(event);">tak</a>
            <a class="modal-action modal-close waves-effect waves-light btn orange">nie</a>
        </div>

    </div>
</div>

<div id="info_modal" class="modal">
    <div class="modal-content">
        <div class="center-align" id="info_text"><h5>Operacja zakończona pomyślnie</h5></div>
    </div>
</div>

<div id="modal7" class="modal">
    <div class="modal-content">
        <h4>Odrzucenie żądania</h4>
        <br>
        <div class="center-align"><h5>Czy na pewno chcesz odrzucić żądanie?</h5></div>
        <br>
        <div class="row center-align">
            <a class="modal-action modal-close waves-effect waves-light btn orange" onclick="refuseAccept()">tak</a>
            <a class="modal-action modal-close waves-effect waves-light btn orange">nie</a>
        </div>

    </div>
</div>

<div id="acceptRequest" class="modal">
    <div class="modal-content">
        <h4>Akceptacja żądania</h4>
        <br>
        <div class="center-align"><h5>Czy na pewno chcesz zrealizować żądanie?</h5></div>
        <br>
        <div class="row center-align" id="accept_confirmation_buton">
        </div>
    </div>
</div>

<script>
    var home = document.getElementById("homeUnderline");
    home.classList.remove("active");

    var order = document.getElementById("orderUnderline");
    order.classList.remove("active");

    var request = document.getElementById("requestUnderline");
    request.classList.remove("active");

    var report = document.getElementById("reportUnderline");
    report.classList.remove("active");

    var manage = document.getElementById("manageUnderline");
    manage.className += "active";
</script>

<%-- AJAX --%>
<script type="text/javascript" charset="utf-8">
    var medicineToSend;
    var listOfMedicineToSend = [];
    var pharmacy_source;
    var pharmacy_destination;
    var number;

    function getDetails(event, pharmacyId, typeId, detailsTableNumber, pharmacy_src, state, demandedNumber) {
        showLoader();

        console.log("Start");
        console.log("Wyswietlam id apteki: " + pharmacyId);

        console.log(""+pharmacyId + ", " + typeId + ", " + detailsTableNumber);

        var json = {"action": "details", "pharmacyId": pharmacyId, "typeId": typeId};

        lastTarget = event.target;

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/manage.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {

                var medicines = JSON.parse(response);

                var table;

                var canAdd;

                if (detailsTableNumber == 1) {
                    table = document.getElementById("detailsTable1");
                } else if (detailsTableNumber == 2) {
                    table = document.getElementById("detailsTable2");
                    var list = [];
                    var size = (state > demandedNumber) ? demandedNumber : state;
                    list.push(typeId);
                    list.push(pharmacy_src);
                    list.push(size);
                    canAdd = true;
                    for(var i = 0; i < listOfMedicineToSend.length; i++)
                    {
                        if(list[0] == listOfMedicineToSend[i][0] && list[1] == listOfMedicineToSend[i][1]) {
                            canAdd = false;
                        }
                    }

                    if(canAdd == true) {
                        console.log("Dodaje nowa liste");
                        listOfMedicineToSend.push(list);
                    }

                    console.log(listOfMedicineToSend.length);
                }

                deleteRows(table);

                for(i = 0; i<medicines.length; i++) {
                    var row = table.insertRow(-1);

                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    var cell3 = row.insertCell(2);

                    cell1.innerHTML = medicines[i].serial;
                    cell2.innerHTML = medicines[i].expiryDate;
                    if(detailsTableNumber == 1)
                    {
                        cell3.innerHTML = "Na stanie";
                    } else {
                        var btnState;
                        if(state < demandedNumber) {
                            if(i < state) {
                                if(canAdd == true) {
                                    btnState = "close";
                                    addMedicineToSendList(typeId, medicines[i].serial, pharmacy_src);
                                } else if(isInList(typeId, medicines[i].serial, pharmacy_src) != true) {
                                    btnState = "check";
                                } else {
                                    btnState = "close";
                                }
                            } else {
                                if(canAdd == true) {
                                    btnState = "check";
                                } else if(isInList(typeId, medicines[i].serial, pharmacy_src) != true) {
                                    btnState = "check";
                                } else {
                                    btnState = "close";
                                }
                            }
                        } else {
                            if(i < demandedNumber) {
                                if(canAdd == true) {
                                    btnState = "close";
                                    addMedicineToSendList(typeId, medicines[i].serial, pharmacy_src);
                                } else if(isInList(typeId, medicines[i].serial, pharmacy_src) != true) {
                                    btnState = "check";
                                } else {
                                    btnState = "close";
                                }
                            } else {
                                if(canAdd == true) {
                                    btnState = "check";
                                } else if(isInList(typeId, medicines[i].serial, pharmacy_src) != true) {
                                    btnState = "check";
                                } else {
                                    btnState = "close";
                                }
                            }
                        }
                        cell3.innerHTML = '<a class="dropdown-button tooltipped addedTooltip" data-position="bottom" data-delay="50" data-tooltip="Wyślij" onclick="changeMedicinesListToSend(this, ' + typeId + ',' + medicines[i].serial + ',' + pharmacy_src + ')"> <i class="small material-icons">'+btnState+'</i> </a>';
                    }
                }
                $('.addedTooltip').tooltip({delay: 50});

                hideLoader();
            },
            error: function () {
                hideLoader();
            }
        });
    }

    function deleteRows(table) {
        var rowCount = table.rows.length;
        for (var i = rowCount - 1; i > 0; i--) {
            table.deleteRow(i);
        }
    }

    function isInList(typeId, serial, pharmacy_src)
    {
        var inList = false;

        for(var i = 0; i < listOfMedicineToSend.length; i++)
        {
            if(typeId == listOfMedicineToSend[i][0] && pharmacy_src == listOfMedicineToSend[i][1])
            {
                if(serial != null) {
                    for(var j = 3; j < listOfMedicineToSend[i].length; j++)
                    {
                        if(listOfMedicineToSend[i][j] == serial)
                        {
                            inList = true;
                        }
                    }
                } else {
                    inList = true;
                }
            }
        }

        return inList;
    }

    function changePharmacyList(event, page, tableNr) {

        var json = {"action": "pagination", "page": page, "table": tableNr};

        var target = event.target;
        console.log(event.id);

        var pages = document.getElementsByClassName("tablePagination"+tableNr);
        for(i = 0; i < pages.length; i++) {
            pages[i].classList.remove("active");
        }
        event.parentNode.classList.add("active");

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/manage.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {

                var medicines = JSON.parse(response);


                var table;

                if(tableNr == 1) {
                    table = document.getElementById("supplementOfDeficiencies");
                } else {
                    if(tableNr == 2)
                    {
                        table = document.getElementById("requestsFromAnotherPharmacies");
                    } else {
                        if(tableNr == 3)
                            table = document.getElementById("shortBestBeforeDate");
                        else
                            table = document.getElementById("requestFromPharmacy");
                    }
                }
                deleteRows(table);
                table = table.tBodies[0];


                for(i = 0; i<medicines.length; i++) {
                    var row = table.insertRow(-1);

                    if(tableNr == 1)
                    {
                        var cell1 = row.insertCell(0);
                        var cell2 = row.insertCell(1);
                        var cell3 = row.insertCell(2);
                        var cell4 = row.insertCell(3);
                        var cell5 = row.insertCell(4);
                        var cell6 = row.insertCell(5);
                        var cell7 = row.insertCell(6);
                        var cell8 = row.insertCell(7);
                        var cell9 = row.insertCell(8);

                        cell1.innerHTML = medicines[i].id;
                        cell2.innerHTML = medicines[i].name;
                        if(medicines[i].dose == 0)
                            cell3.innerHTML = " ";
                        else
                            cell3.innerHTML = medicines[i].dose + ' ' + medicines[i].doseUnit;
                        cell4.innerHTML = medicines[i].kind;
                        cell5.innerHTML = medicines[i].capacity;
                        cell6.innerHTML = medicines[i].count;
                        cell7.innerHTML = medicines[i].demandedCount;
                        cell8.innerHTML = medicines[i].deficite;
                        cell9.innerHTML = getActionsButton(1, medicines[i].id, null, null, null, null);
                    } else {
                        if(tableNr == 2)
                        {
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);
                            var cell4 = row.insertCell(3);
                            var cell5 = row.insertCell(4);
                            var cell6 = row.insertCell(5);
                            var cell7 = row.insertCell(6);
                            var cell8 = row.insertCell(7);
                            var cell9 = row.insertCell(8);
                            var cell10 = row.insertCell(9);

                            cell1.innerHTML = medicines[i].id;
                            cell2.innerHTML = medicines[i].name;
                            cell3.innerHTML = medicines[i].dose + ' ' + medicines[i].doseUnit;
                            cell4.innerHTML = medicines[i].kind;
                            cell5.innerHTML = medicines[i].capacity;
                            cell6.innerHTML = medicines[i].state;
                            cell7.innerHTML = medicines[i].pharmacy;
                            cell8.innerHTML = medicines[i].priority;
                            cell9.innerHTML = medicines[i].number;
                            cell10.innerHTML = getActionsButton(2, medicines[i].id, medicines[i].requestId, medicines[i].pharmacySourceId, medicines[i].state, medicines[i].number);
                        } else {
                            if(tableNr == 3)
                            {
                                var cell1 = row.insertCell(0);
                                var cell2 = row.insertCell(1);
                                var cell3 = row.insertCell(2);
                                var cell4 = row.insertCell(3);
                                var cell5 = row.insertCell(4);
                                var cell6 = row.insertCell(5);
                                var cell7 = row.insertCell(6);
                                var cell8 = row.insertCell(7);

                                cell1.innerHTML = medicines[i].id;
                                cell2.innerHTML = medicines[i].name;
                                cell3.innerHTML = medicines[i].serial;
                                cell4.innerHTML = medicines[i].dose;
                                cell5.innerHTML = medicines[i].kind;
                                cell6.innerHTML = medicines[i].capacity;
                                cell7.innerHTML = medicines[i].expirationDate;
                                cell8.innerHTML = getActionsButton(3, medicines[i].id, medicines[i].serial, null, null, null);
                            } else {
                                if(tableNr == 4) {
                                    var cell1 = row.insertCell(0);
                                    var cell2 = row.insertCell(1);
                                    var cell3 = row.insertCell(2);
                                    var cell4 = row.insertCell(3);
                                    var cell5 = row.insertCell(4);
                                    var cell6 = row.insertCell(5);
                                    var cell7 = row.insertCell(6);
                                    var cell8 = row.insertCell(7);
                                    var cell9 = row.insertCell(8);

                                    cell1.innerHTML = medicines[i].id;
                                    cell2.innerHTML = medicines[i].name;
                                    cell3.innerHTML = medicines[i].dose + ' ' + medicines[i].doseUnit;
                                    cell4.innerHTML = medicines[i].kind;
                                    cell5.innerHTML = medicines[i].capacity;
                                    cell6.innerHTML = medicines[i].pharmacy;
                                    cell7.innerHTML = medicines[i].priority;
                                    cell8.innerHTML = medicines[i].number;
                                    cell9.innerHTML = medicines[i].info;
                                }
                            }
                        }
                    }
                }
                table.classList.add("bordered", "striped", "highlight", "responsive-table");
                $('.addedTooltip').tooltip({delay: 50});
            },

            error: function () {
            }
        });
    }

    function refuse(requestId) {

        var json = {"action": "addToSession", "requestId": requestId};
        console.log(requestId);

        var target = event.target;

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/manage.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
            },
            error: function () {
            }
        });
    }

    function refuseAccept() {
        var json = {"action": "refuseRequest"};
        console.log("UPDATE BAZY");

        var target = event.target;

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/manage.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                var table = document.getElementById("requestsFromAnotherPharmacies");
                deleteRows(table);
                table = table.tBodies[0];

                var medicines = JSON.parse(response);

                for(i = 0; i<medicines.length; i++) {
                    var row = table.insertRow(-1);

                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    var cell3 = row.insertCell(2);
                    var cell4 = row.insertCell(3);
                    var cell5 = row.insertCell(4);
                    var cell6 = row.insertCell(5);
                    var cell7 = row.insertCell(6);
                    var cell8 = row.insertCell(7);
                    var cell9 = row.insertCell(8);
                    var cell10 = row.insertCell(9);

                    cell1.innerHTML = medicines[i].id;
                    cell2.innerHTML = medicines[i].name;
                    cell3.innerHTML = medicines[i].dose + ' ' + medicines[i].doseUnit;
                    cell4.innerHTML = medicines[i].kind;
                    cell5.innerHTML = medicines[i].capacity;
                    cell6.innerHTML = medicines[i].state;
                    cell7.innerHTML = medicines[i].pharmacy;
                    cell8.innerHTML = medicines[i].priority;
                    cell9.innerHTML = medicines[i].number;
                    cell10.innerHTML = getActionsButton(2, medicines[i].id, null, null, null, null);
                }
            },
            error: function () {
            }
        });
    }

    function changeMedicinesListToSend(event, typeId, serial, pharmacy_src)
    {
        if(event.firstElementChild.innerHTML === "check")
        {
            if(addMedicineToSendList(typeId, serial, pharmacy_src) == true) {
                event.firstElementChild.innerHTML = "close";
            } else {
                info_text.innerHTML = '<h5 style="color: red;">Nie można wybrać więcej leków do wysłania!</h5>';
                $('#info_modal').modal('open');
            }
        } else
        {
            deleteMedicineFromSendList(typeId, serial, pharmacy_src);
            event.firstElementChild.innerHTML = "check";
            //event.attr("data-tooltip", "Wyślij");
        }
    }

    function addMedicineToSendList(typeId, serial, pharmacy_src)
    {
        var canAdd = false;
        for(var i = 0; i < listOfMedicineToSend.length; i++) {
            if(listOfMedicineToSend[i][0] == typeId && listOfMedicineToSend[i][1] == pharmacy_src) {
                if(listOfMedicineToSend[i][2] > listOfMedicineToSend[i].length - 3) {
                    console.log("Dodaje do listy o wielkosci: " + listOfMedicineToSend[i].length);
                    listOfMedicineToSend[i].push(serial);
                    console.log("Dodano do listy, wielkosc: " + listOfMedicineToSend[i].length);
                    canAdd = true;
                }
            }
        }
        return canAdd;
    }

    function deleteMedicineFromSendList(typeId, serial, pharmacy_src)
    {
        var toRemove = [];
        for(var i = 0; i < listOfMedicineToSend.length; i++)
        {
            if(listOfMedicineToSend[i][0] == typeId && listOfMedicineToSend[i][1] == pharmacy_src)
            {
                for(var j = 3; j < listOfMedicineToSend[i].length; j++)
                {
                    if(listOfMedicineToSend[i][j] == serial)
                    {
                        toRemove.push(i);
                        toRemove.push(j);
                    }
                }
            }
        }
        if(toRemove.length > 0)
        {
            console.log("Usuwam z listy o rozmiarze: " + listOfMedicineToSend[toRemove[0]].length);
            listOfMedicineToSend[toRemove[0]].splice(toRemove[1],1);
            console.log("Usunięto z listy. Rozmiar: " + listOfMedicineToSend[toRemove[0]].length);
        }
    }

    function getListOfMedicineToSend(medicineTypeId, pharmacy_src)
    {
        var listToSend = [];
        for(var i = 0; i < listOfMedicineToSend.length; i++)
        {
            if(listOfMedicineToSend[i][0] == medicineTypeId && listOfMedicineToSend[i][1] == pharmacy_src)
            {
                listToSend = listOfMedicineToSend[i].slice(3,listOfMedicineToSend[i].length);
            }
        }
        return listToSend;
    }

    function acceptRequest(requestId, medicineTypeId, pharmacy_dst, pharmacy_src, number)
    {
        console.log("MedicineTypeId: " + medicineTypeId);
        var kind = isInList(medicineTypeId, null, pharmacy_src) ? 0:1;
        var dataToSend = [];
        console.log("Akceptacja żądania, rodzaj: " + kind);
        if(kind == 0) {
            dataToSend = getListOfMedicineToSend(medicineTypeId, pharmacy_src);
        }

        console.log(typeof dataToSend[0] === 'number');
        console.log("Wielkosc listy: " + dataToSend.length);
        var json = {"action": "acceptRequest", "typeId": medicineTypeId, "pharmacy_dst": pharmacy_dst, "pharmacy_src": pharmacy_src, "kind": kind, "data": JSON.stringify(dataToSend), "number": number, "requestId": requestId};

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/manage.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                $('#modal5').modal('close');

                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');

                var n;
                var medicines;
                if(kind == 0)
                    n = dataToSend.length;
                else {
                    n = number;
                    medicines = JSON.parse(response);
                }

                for(var i = 0; i < n; i++) {
                    console.log("USUWAM SERAL: " + (kind == 0 ? dataToSend[i] : medicines[i].serial));
                    changeStateInSupplementOfDeficienciesTable(kind == 0 ? dataToSend[i] : medicines[i].serial);

                }
                //=============Change states===============
                // var count = $(countCell).html()
                // if ( count > 1) {
                //   countCell.innerHTML = '<td>' + (count-1) + '</td>'; //change medicine state
                //} else {
                lastRow.remove(); //remove row from rewuests table
                //}

                //remove row from details
                //detailRow.remove();
                //=======================================

                //hideLoaderModal();
            },
            error: function () {
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');

                //hideLoaderModal();
            }
        });
    }

    function acceptRequestConfirmation(event, requestId, medicineId, pharmacy_dst, pharmacy_src, state, requestNumber)
    {
        lastRow = event.target.parentElement.parentElement.parentElement;
        medicineTypeId = medicineId;
        pharmacy_source = pharmacy_src;
        pharmacy_destination = pharmacy_dst;
        var number;

        if(state > requestNumber) {
            number = requestNumber;
        } else {
            number = state;
        }

        if(state == 0)
        {
            info_text.innerHTML = '<h5 style="color: red;">Brak żądanych leków na stanie!</h5>';
            $('#info_modal').modal('open');
        } else if(isInList(medicineId,null, pharmacy_src) && getListOfMedicineToSend(medicineId, pharmacy_src).length == 0) {
            info_text.innerHTML = '<h5 style="color: red;">Nie wybrano leków do wysłania!</h5>';
            $('#info_modal').modal('open');
        } else {
            var accept_confirmation_button = document.getElementById("accept_confirmation_buton");

            accept_confirmation_button.innerHTML = '<a class="modal-action modal-close waves-effect waves-light btn orange" onclick="acceptRequest(' + requestId + ',' + medicineId + ',' + pharmacy_dst + ',' + pharmacy_src + ',' + number + ');">tak</a> ' +
                '<a class="modal-action modal-close waves-effect waves-light btn orange">nie</a> ';
            $('#acceptRequest').modal('open');
        }
    }

    function getActionsButton(detailsTableNumber, currentMedicine, serial, pharmacy_src, state, demandedNumber) {

        var actionButtons;
        var medicineId = currentMedicine;
        var pharmacyName = ${currentPharmacy};

        console.log("---");

        var medicineImportList = ${medicineIL_json};
        console.log(medicineImportList);
        console.log("---");

        var val1 = medicineImportList[medicineId] == 1 ? "flicker" : "";

        console.log("DECYZJA: " + val1);
        console.log("medicineId " + medicineId);
        var val2 = medicineImportList[medicineId] == 2 ? "flicker" : "";

        if (detailsTableNumber == 1) {
            actionButtons = '<a class="dropdown-button tooltipped modal-trigger addedTooltip" href="#modal2" data-position="bottom" data-delay="50" data-tooltip="Szczegóły" onclick="getDetails(this, ' + pharmacyName + ', ' + currentMedicine + ',' + detailsTableNumber + ',null,null,null)"> <i class="small material-icons">info</i> </a> <a class="dropdown-button tooltipped modal-trigger addedTooltip '+ val2 +'" href="#modal4" data-position="bottom"  data-delay="50" data-tooltip="Zamów z hurtowni" onclick="getMedicinesFromWholesale_prepare(event)"> <i class="small material-icons">reply</i>  </a> <a class="dropdown-button tooltipped modal-trigger addedTooltip '+ val1 +'" href="#modal3" data-position="bottom"  data-delay="50" data-tooltip="Zamów z innej apteki" onclick="getMedicinesFromAnotherPharmacy(event)"> <i class="small material-icons">reply_all</i> </a>';
        } else if (detailsTableNumber == 2) {
            var requestDecisionList = ${requestDecisionList_json};
            var val1 = requestDecisionList[serial] == 1 ? "flicker" : "";
            var val2 = requestDecisionList[serial] == 0 ? "flicker" : "";
            actionButtons = '<a class="dropdown-button tooltipped modal-trigger addedTooltip '+ val1 +'" href="#" data-position="bottom" data-delay="50" ' +
                            'data-tooltip="Zatwierdź"> ' +
                            '<i class="small material-icons">check</i> ' +
                            '</a>' +
                            '<a class="dropdown-button tooltipped modal-trigger addedTooltip '+ val2 +'" href="#modal7" data-position="bottom" data-delay="50" ' +
                            'data-tooltip="Odmów"><i class="small material-icons">close</i> ' +
                            '</a>' +
                            '<a class="dropdown-button tooltipped modal-trigger addedTooltip" href="#modal1" data-position="bottom" data-delay="50" ' +
                            'data-tooltip="Szczegóły" ' +
                            'onclick="getDetails(this, ' + pharmacyName + ', ' + currentMedicine + ',' + detailsTableNumber + ',' + pharmacy_src +',' + state + ',' + demandedNumber +')"> ' +
                            '<i class="small material-icons">info</i>' +
                            '</a>';
        } else
        {
            var sendOrUtilizeList = ${sendOrtilizeList_json};
            var val1 = sendOrUtilizeList[serial] == 1 ? "flicker" : "";
            var val2 = sendOrUtilizeList[serial] == 2 ? "flicker" : "";
            actionButtons = '<a class="dropdown-button tooltipped modal-trigger addedTooltip '+ val2 +'" href="#modal5" data-position="bottom" data-delay="50" ' +
                            'data-tooltip="Wyślij do innej apteki" ' +
                            'onclick="saveMedicineToSend(event,'+serial+','+currentMedicine+')"> ' +
                        '<i class="small material-icons">redo</i>' +
                    '</a> '+
                '<a class="dropdown-button tooltipped modal-trigger addedTooltip '+ val1 +'" onclick="utilizeConfirmation(' + serial + ', event' + medicineId + ' )" data-position="bottom" data-delay="50" ' +
                'data-tooltip="Utylizuj">' +
                '<i class="small material-icons">delete</i>' +
                '</a>';
        }
        return actionButtons;
    }

    function saveMedicineToSend(event, serial, medicineId)
    {
        lastRow = event.target.parentElement.parentElement.parentElement;
        medicineToSend = serial;
        medicineTypeId = medicineId;
        console.log("MEDICINE TO SEND: " + medicineToSend);
    }

    function sendToPharmacyConfirmation(pharmacy_src, pharmacy_dst)
    {
        console.log("Bede wyswietlal modala");
        console.log("Serial do wyslania: " + medicineToSend);
        var send_confirmation_button = document.getElementById("send_confirmation_button");

        send_confirmation_button.innerHTML = '<a class="modal-action modal-close waves-effect waves-light btn orange" onclick="sendToPharmacy(' + medicineToSend + ',' + pharmacy_dst + ',' + pharmacy_src + ');">tak</a> ' +
            '<a class="modal-action modal-close waves-effect waves-light btn orange">nie</a> ';

        $('#sendToPharmacy_modal').modal('open');
    }
    function sendToPharmacy(serial, pharmacy_dst, pharmacy_src){

        console.log("Send start");
        console.log("Serial: " + serial);
        console.log("dst: " + pharmacy_dst + ", src: " + pharmacy_src);
        //showLoaderModal();

        var json = {"action": "sendToPharmacy", "serial": serial, "pharmacy_dst": pharmacy_dst, "pharmacy_src": pharmacy_src, "typeId": medicineTypeId};

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/manage.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                console.log("send to another pharmacyName end");

                $('#modal5').modal('close');

                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');

                changeStateInSupplementOfDeficienciesTable(medicineTypeId);
                //=============Change states===============
                // var count = $(countCell).html()
                // if ( count > 1) {
                //   countCell.innerHTML = '<td>' + (count-1) + '</td>'; //change medicine state
                //} else {
                lastRow.remove(); //remove row from short best before date
                //}

                //remove row from details
                //detailRow.remove();
                //=======================================

                //hideLoaderModal();
            },
            error: function () {
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');

                //hideLoaderModal();
            }
        });
    }

    var lastTarget;
    var lastRow;
    var info_text = document.getElementById("info_text");
    var medicineTypeId;

    var countCell;

    function utilizeConfirmation(serial, event, medicineId) {
        lastRow = event.target.parentElement.parentElement.parentElement; //row represented medicine with given serial
        medicineTypeId = medicineId;

        console.log(serial);
        console.log(lastRow);

        var confirmation_button = document.getElementById("confirmation_button");
        confirmation_button.innerHTML = '<a class="modal-action modal-close waves-effect waves-light btn orange" onclick="utilize(' + serial + ');">tak</a> ' +
            '<a class="modal-action modal-close waves-effect waves-light btn orange">nie</a> ';

        $('#utilize_modal').modal('open');

    }

    function utilize(serial){

        console.log("Utilize start");

        var json = {"action": "utilize", "serial": serial};

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                console.log("utilize end");

                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');

                changeStateInSupplementOfDeficienciesTable(medicineTypeId);
                //=============Change states===============
               // var count = $(countCell).html()
               // if ( count > 1) {
                 //   countCell.innerHTML = '<td>' + (count-1) + '</td>'; //change medicine state
                //} else {
                    lastRow.remove(); //remove all row when all medicines with that kind are sold
                //}

                //remove row from details
                //detailRow.remove();
                //=======================================

                //hideLoaderModal();
            },
            error: function () {
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');

                //hideLoaderModal();
            }
        });
    }

    function changeStateInSupplementOfDeficienciesTable(serial) {
        var supplementOfDeficiencies = document.getElementById("supplementOfDeficiencies");
        var requestsFromAnotherPharmacies = document.getElementById("requestsFromAnotherPharmacies");
        var shortBestBeforeDate = document.getElementById("shortBestBeforeDate");

        var rows = supplementOfDeficiencies.rows;

        for(i=0; i<rows.length; i++) {
            var currentCells = rows[i].cells;
            var idCell = currentCells[0];
            var currentMedicineId = $(idCell).html();

            if(medicineTypeId==currentMedicineId) {
                var stateCell = currentCells[5];
                var deficitCell = currentCells[7];

                var stateOld = $(stateCell).html();
                var deficitOld = $(deficitCell).html();

                var stateNew = parseInt(stateOld) - 1;
                var deficitNew = parseInt(deficitOld) + 1;

                stateCell.innerHTML = stateNew;
                deficitCell.innerHTML = deficitNew;
            }
        }


        var rows2 = requestsFromAnotherPharmacies.rows;

        for(i=0; i<rows2.length; i++) {
            var currentCells2 = rows2[i].cells;
            var idCell2 = currentCells2[0];
            var currentMedicineId2 = $(idCell2).html();

            if(medicineTypeId==currentMedicineId2) {
                var stateCell2 = currentCells2[5];
                var stateOld2 = $(stateCell2).html();
                var stateNew2 = parseInt(stateOld2) - 1;

                stateCell2.innerHTML = stateNew2;
            }
        }

        var rows3 = shortBestBeforeDate.rows;
        var toDelete = -1;

        for(i=0; i<rows3.length; i++) {
            var currentCells = rows3[i].cells;
            var idCell = currentCells[0];
            var currentMedicineId = $(idCell).html();
            var serialCell = currentCells[2];
            var currentMedicineSerial = $(serialCell).html();

            if(medicineTypeId==currentMedicineId && currentMedicineSerial == serial) {
                toDelete = i;
            }
        }
        if(toDelete != -1)
            shortBestBeforeDate.deleteRow(toDelete);
    }

    function showLoaderModal() {
        var loader = document.getElementById("modalLoader3");
        loader.style.visibility = "visible";
        var loader = document.getElementById("modalLoader4");
        loader.style.visibility = "visible";
    }

    function hideLoaderModal() {
        var loader = document.getElementById("modalLoader3");
        loader.style.visibility = "hidden";
        var loader = document.getElementById("modalLoader4");
        loader.style.visibility = "hidden";
    }

    var typeId;
    var currentPharmacy = ${currentPharmacy};
    var typeState_info = document.getElementById("typeState_info");

    function getMedicinesFromAnotherPharmacy(event) {
        showLoaderModal();

        var row = event.target.parentElement.parentElement.parentElement;
        typeId = $(row.cells[0]).html();

        var json = {"action": "getMedicinesFromAnotherPharmacy", "typeId": typeId};

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                var medicines = JSON.parse(response);

                var table = document.getElementById("type_table");

                typeState_info.innerHTML = "";
                deleteRows(table);
                table = table.tBodies[0];

                if (medicines.length > 0) {
                    for (i = 0; i < medicines.length; i++) {
                        var row = table.insertRow(-1); // add at the end


                        var cell1 = row.insertCell(0);
                        var cell2 = row.insertCell(1);
                        var cell3 = row.insertCell(2);
                        var cell4 = row.insertCell(3);
                        var cell5 = row.insertCell(4);

                        cell1.innerHTML = medicines[i].pharmacyId;
                        cell2.innerHTML = medicines[i].pharmacyName;
                        cell3.innerHTML = medicines[i].numberOfMedicine;
                        cell4.innerHTML = '<div class="input-field col s3"> ' +
                            '<input type="number" min="0" max="100"> ' +
                            '<label for="drugName">Zamawiana liczba:</label> ' +
                            '</div> ';
                        cell5.innerHTML = '<a class="dropdown-button addedTooltip tooltipped" href="#" data-position="bottom" data-delay="50" ' +
                            ' data-tooltip="Zamów"><i class="small material-icons" onclick="getMedicinesFromAnotherPharmacyPart2(event);">file_download</i></a> ';
                    }

                    $('.addedTooltip').tooltip({delay: 50});
                } else {
                    typeState_info.innerHTML = "<h4>Brak wybranego leku w innych aptekach!</h4>";
                }

                hideLoaderModal();
            },
            error: function () {
                hideLoaderModal();
            }
        });
    }

    function getMedicinesFromAnotherPharmacyPart2(event) {
        showLoaderModal();

        var row = event.target.parentElement.parentElement.parentElement;
        var warehouse_dst = $(row.cells[0]).html();
        var number = row.getElementsByTagName("input")[0].value;

        var json = {
            "action": "getMedicinesFromAnotherPharmacyPart2", "typeId": typeId, "warehouse_src": currentPharmacy,
            "warehouse_dst": warehouse_dst, "number": number, "status": 1
        };

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');
            },
            error: function () {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');
            }
        });
    }

    function getMedicinesFromWholesale_prepare(event) {
        var row = event.target.parentElement.parentElement.parentElement;
        typeId = $(row.cells[0]).html();
        console.log(typeId);
        hideLoaderModal();
    }

    function getMedicinesFromWholesale(event) {
        showLoaderModal();

        var row = event.target.parentElement.parentElement.parentElement;
        var number = row.getElementsByTagName("input")[0].value;

        var json = {
            "action": "getMedicinesFromWholesale", "typeId": typeId, "warehouse_src": 4,
            "warehouse_dst": currentPharmacy, "number": number, "status": 1
        };

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/home.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');
            },
            error: function () {
                hideLoaderModal();
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');
            }
        });
    }


    function setDateAndTime(dateSpan) {
        var nazwy_mies = ['Stycznia', 'Lutego', 'Marca', 'Kwietnia', 'Maja',
            'Czerwca', 'Lipca', 'Sierpnia', 'Września', 'Października',
            'Listopada', 'Grudnia'];

        var currentDate = "${currentDate}";
        var currentTime = "${currentTime}";

        var rok = currentDate.toString().substring(0,4);
        var mies = (currentDate-1).toString().substring(4,6);
        var dzien = currentDate.toString().substring(6,8);
        var godz = currentTime.toString().substring(0,2);
        var min = currentTime.toString().substring(2,4);
        var sec = currentTime.toString().substring(4,6);


        var data_i_czas = dzien + ' ' + nazwy_mies[mies-1] + ' ' + rok
            + '\n ' + godz + ':' + min + ':' + sec;

        dateSpan.innerHTML = data_i_czas;
    }
</script>
