<%--
  Created by IntelliJ IDEA.
  User: Konrad Trzebiński
  Date: 05.11.2017
  Time: 21:18
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="cor" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">

    <h4>Lista leków zamówionych:</h4>
    <hr>

    <table id="transferToSource" class="bordered striped highlight responsive-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nr. seryjny</th>
            <th>Nazwa</th>
            <th>Data przybycia</th>
            <th>Godzina przybycia</th>
            <th>Źródło</th>
        </tr>
        </thead>

        <tbody>
        <cor:forEach items="${transferPharmacyToSourceDivideOnPieces}" var="currentMedicine">
            <tr>
                <td>${currentMedicine.id}</td>
                <td>${currentMedicine.serial}</td>
                <td>${currentMedicine.name}</td>
                <td>${currentMedicine.endDate}</td>
                <td>${currentMedicine.endTime}</td>
                <td>${currentMedicine.source}</td>
            </tr>
        </cor:forEach>
        </tbody>
    </table>

    <ul class="pagination">
        <cor:forEach var="counter" begin="1" end="${transferPharmacyToSourcePagination}">
            <cor:if test="${counter == 1}">
                <li class="active waves-effect tablePagination1"><a onclick="changePharmacyList(this,${counter},1)">${counter}</a></li>
            </cor:if>
            <cor:if test="${counter > 1}">
                <li class="waves-effect tablePagination1"><a id="1" onclick="changePharmacyList(this,${counter},1)">${counter}</a></li>
            </cor:if>
        </cor:forEach>
    </ul>


    <br><br><br><br>
    <h4>Lista leków wysłanych:</h4>
    <hr>

    <table id="transferDestination" class="bordered striped highlight responsive-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nr. seryjny</th>
            <th>Nazwa</th>
            <th>Data wysłania</th>
            <th>Godzina wysłania</th>
            <th>Cel</th>
        </tr>
        </thead>

        <tbody>
        <cor:forEach items="${transferPharmacyToDestinationDivideOnPieces}" var="currentMedicine">
            <tr>
                <td>${currentMedicine.id}</td>
                <td>${currentMedicine.serial}</td>
                <td>${currentMedicine.name}</td>
                <td>${currentMedicine.startDate}</td>
                <td>${currentMedicine.startTime}</td>
                <td>${currentMedicine.destination}</td>
            </tr>
        </cor:forEach>
        </tbody>
    </table>

    <ul class="pagination">
        <cor:forEach var="counter" begin="1" end="${transferPharmacyToDestinationPagination}">
            <cor:if test="${counter == 1}">
                <li class="active waves-effect tablePagination2"><a onclick="changePharmacyList(this,${counter},2)">${counter}</a></li>
            </cor:if>
            <cor:if test="${counter > 1}">
                <li class="waves-effect tablePagination2"><a onclick="changePharmacyList(this,${counter},2)">${counter}</a></li>
            </cor:if>
        </cor:forEach>
    </ul>
</div>

<script>
    var home = document.getElementById("homeUnderline");
    home.classList.remove("active");

    var order = document.getElementById("orderUnderline");
    order.className += "active";

    var request = document.getElementById("requestUnderline");
    request.classList.remove("active");

    var report = document.getElementById("reportUnderline");
    report.classList.remove("active");

    var manage = document.getElementById("manageUnderline");
    manage.classList.remove("active");
</script>

<%-- AJAX --%>
<script type="text/javascript" charset="utf-8">
    function deleteRows(table) {
        var rowCount = table.rows.length;
        for (var i = rowCount - 1; i > 0; i--) {
            table.deleteRow(i);
        }
    }

    function changePharmacyList(event, page, tableNr) {

        var json = {"action": "pagination", "page": page, "table": tableNr};

        <!-- Pobranie wydarzenia i obiektu który je wywołał -->
        var target = event.target;
        console.log(event.id);

        var pages = document.getElementsByClassName("tablePagination"+tableNr);
        for(i = 0; i < pages.length; i++) {
            pages[i].classList.remove("active");
        }
        event.parentNode.classList.add("active");

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/order.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {

                var medicines = JSON.parse(response);

                var table;

                if(tableNr == 1) {
                    table = document.getElementById("transferToSource");
                }
                else {
                    table = document.getElementById("transferDestination");
                }
                deleteRows(table);
                table = table.tBodies[0];

                for(i = 0; i<medicines.length; i++) {
                    var row = table.insertRow(-1); // add at the end

                    if(tableNr == 1) {
                        var cell1 = row.insertCell(0);
                        var cell2 = row.insertCell(1);
                        var cell3 = row.insertCell(2);
                        var cell4 = row.insertCell(3);
                        var cell5 = row.insertCell(4);
                        var cell6 = row.insertCell(5);

                        cell1.innerHTML = medicines[i].id;
                        cell2.innerHTML = medicines[i].serial;
                        cell3.innerHTML = medicines[i].name;
                        cell4.innerHTML = medicines[i].endDate;
                        cell5.innerHTML = medicines[i].endTime;
                        cell6.innerHTML = medicines[i].source;
                    } else {
                        if(tableNr == 2) {
                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);
                            var cell4 = row.insertCell(3);
                            var cell5 = row.insertCell(4);
                            var cell6 = row.insertCell(5);

                            cell1.innerHTML = medicines[i].id;
                            cell2.innerHTML = medicines[i].serial;
                            cell3.innerHTML = medicines[i].name;
                            cell4.innerHTML = medicines[i].startDate;
                            cell5.innerHTML = medicines[i].startTime;
                            cell6.innerHTML = medicines[i].destination;
                        }
                    }
                }
                $('.addedTooltip').tooltip({delay: 50});
            },

            error: function () {
            }
        });
    }

    function setDateAndTime(dateSpan) {
        var nazwy_mies = ['Stycznia', 'Lutego', 'Marca', 'Kwietnia', 'Maja',
            'Czerwca', 'Lipca', 'Sierpnia', 'Września', 'Października',
            'Listopada', 'Grudnia'];

        var currentDate = "${currentDate}";
        var currentTime = "${currentTime}";

        var rok = currentDate.toString().substring(0,4);
        var mies = (currentDate-1).toString().substring(4,6);
        var dzien = currentDate.toString().substring(6,8);
        var godz = currentTime.toString().substring(0,2);
        var min = currentTime.toString().substring(2,4);
        var sec = currentTime.toString().substring(4,6);


        var data_i_czas = dzien + ' ' + nazwy_mies[mies-1] + ' ' + rok
            + '\n ' + godz + ':' + min + ':' + sec;

        dateSpan.innerHTML = data_i_czas;
    }
</script>
