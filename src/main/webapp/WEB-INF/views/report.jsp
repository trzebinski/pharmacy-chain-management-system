<%--
  Created by IntelliJ IDEA.
  User: Konrad Trzebiński
  Date: 05.11.2017
  Time: 21:22
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="cor" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="filter">
    <div class="row">
        <sf:form class="col s12" method="post" commandName="homePharmacy">
            <div class="row">
                <h6 class="left">Wybór wykresu:</h6>
            </div>
            <div class="row">
                <div class="input-field col s3">
                    <input id="drugName" name="drugName" type="text" class="validate" value="${drugNameFilter}">
                    <label for="drugName">Nazwa leku:</label>
                </div>
                <div class="input-field col s4">
                    <select id="kind" name="kind">
                        <option value="" <cor:if test="${kindFilter == ''}">selected</cor:if>>Wszystkie</option>
                        <option value="tabletki" <cor:if test="${kindFilter == 'tabletki'}">selected</cor:if>>Tabletki
                        </option>
                        <option value="saszetki" <cor:if test="${kindFilter == 'saszetki'}">selected</cor:if>>Saszetki
                        </option>
                        <option value="syrop" <cor:if test="${kindFilter == 'syrop'}">selected</cor:if>>Syropy</option>
                        <option value="czopki" <cor:if test="${kindFilter == 'czopki'}">selected</cor:if>>Czopki
                        </option>
                    </select>
                    <label>Rodzaj leku:</label>
                </div>
                <div class="input-field col s4">
                </div>
                <!--<div class="col s1">
                    <a class="btn-floating btn-large waves-effect waves-light orange tooltipped" data-position="bottom"
                       data-delay="50" data-tooltip="Pokaż wykres!" type="submit" name="action" value="createGraph">
                        <i class="medium material-icons">create</i>
                    </a>
                </div>-->
                <button class="btn-floating btn-large waves-effect waves-light orange tooltipped"
                        data-position="bottom"
                        data-delay="50" data-tooltip="Pokaż wykres!" type="submit" name="action" value="createGraph">
                    <i class="large material-icons">search</i>
                </button>
            </div>
        </sf:form>
    </div>
</div>
<br>

<div class="container chart">
    <h4>${pharmacyName}</h4>
    <canvas id="myChart"></canvas>
</div>
<br>

<script>
    var home = document.getElementById("homeUnderline");
    home.classList.remove("active");

    var order = document.getElementById("orderUnderline");
    order.classList.remove("active");

    var request = document.getElementById("requestUnderline");
    request.classList.remove("active");

    var report = document.getElementById("reportUnderline");
    report.className += "active";

    var manage = document.getElementById("manageUnderline");
    manage.classList.remove("active");
</script>

<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var medicinetoReport = ${medicineToRaport_json};
    var graphData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for(i = 0; i<medicinetoReport.length; i++) {
        graphData[medicinetoReport[i].soldMonth-1] += medicinetoReport[i].countMonth;
        console.log("ID LEKOW: " + medicinetoReport[i].id +", miesiac sprzedania: " + medicinetoReport[1].soldMonth);
    }
    var chart = new Chart(ctx, {
        type: 'line',

        data: {
            labels: ${last12MonthsList},
            datasets: [{
                label: "Sprzedaż leków w ostatnich 12 miesiącach",
                backgroundColor: 'rgb(255, 165, 0)',
                borderColor: 'rgb(255, 165, 0)',
                data: ${dataToGraph},
            }]
        },

        options: {}
    });

    function setDateAndTime(dateSpan) {
        var nazwy_mies = ['Stycznia', 'Lutego', 'Marca', 'Kwietnia', 'Maja',
            'Czerwca', 'Lipca', 'Sierpnia', 'Września', 'Października',
            'Listopada', 'Grudnia'];

        var currentDate = "${currentDate}";
        var currentTime = "${currentTime}";

        var rok = currentDate.toString().substring(0,4);
        var mies = (currentDate-1).toString().substring(4,6);
        var dzien = currentDate.toString().substring(6,8);
        var godz = currentTime.toString().substring(0,2);
        var min = currentTime.toString().substring(2,4);
        var sec = currentTime.toString().substring(4,6);

        var data_i_czas = dzien + ' ' + nazwy_mies[mies-1] + ' ' + rok
            + '\n ' + godz + ':' + min + ':' + sec;

        dateSpan.innerHTML = data_i_czas;
    }
</script>
