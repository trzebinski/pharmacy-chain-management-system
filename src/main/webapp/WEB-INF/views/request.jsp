<%--
  Created by IntelliJ IDEA.
  User: Konrad Trzebiński
  Date: 01.11.2017
  Time: 11:59
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="cor" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="filter">
    <div class="row">
        <sf:form class="col s12" method="post" commandName="homePharmacy">
            <div class="row">
                <h6 class="left">FILTRY:</h6>
            </div>
            <div class="row">
                <div class="input-field col s4">
                    <input id="drugName" name="drugName" type="text" class="validate" value="${drugNameFilter}">
                    <label for="drugName">Nazwa leku:</label>
                </div>
                <div class="input-field col s4">
                    <select id="kind" name="kind">
                        <option value="" <cor:if test="${kindFilter == ''}">selected</cor:if>>Wszystkie</option>
                        <option value="tabletki" <cor:if test="${kindFilter == 'tabletki'}">selected</cor:if>>Tabletki
                        </option>
                        <option value="saszetki" <cor:if test="${kindFilter == 'saszetki'}">selected</cor:if>>Saszetki
                        </option>
                        <option value="syrop" <cor:if test="${kindFilter == 'syrop'}">selected</cor:if>>Syropy</option>
                        <option value="czopki" <cor:if test="${kindFilter == 'czopki'}">selected</cor:if>>Czopki
                        </option>
                    </select>
                    <label>Rodzaj leku:</label>
                </div>
                <div class="input-field col s3">
                    <select id="order" name="order">
                        <option value="drugName" <cor:if test="${orderFilter == 'drugName'}">selected</cor:if>>Nazwa
                        </option>
                        <option value="count" <cor:if test="${orderFilter == 'count'}">selected</cor:if>>Liczba leków na stanie
                        </option>
                        <option value="capacity" <cor:if test="${orderFilter == 'capacity'}">selected</cor:if>>Opakowanie
                        </option>
                    </select>
                    <label>Sortowanie:</label>
                </div>
                <button class="btn-floating btn-large waves-effect waves-light orange tooltipped"
                        data-position="bottom"
                        data-delay="50" data-tooltip="Szukaj!" type="submit" name="action" value="search">
                    <i class="large material-icons">search</i>
                </button>
            </div>
        </sf:form>
    </div>
</div>
<br>


<div class="container">

<cor:if test="${numberOfMedicines == 0}">
    <div>
        <br><br>
        <h4 class="center-align">Brak leków spełniających wybrane kryteria!</h4>
        <br><br>
    </div>
</cor:if>

<cor:if test="${numberOfMedicines > 0}">
    <h4>${currentPharmacyName}</h4>
    <hr>

    <table id="demandedState" class="bordered striped highlight responsive-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nazwa</th>
            <th>Dawka</th>
            <th>Rodzaj</th>
            <th>Opakowanie</th>
            <th>Na stanie</th>
            <th>Stan docelowy</th>
            <th>Akcje</th>
        </tr>
        </thead>

        <tbody>
        <cor:forEach items="${medicineStatePharmaciesDivideOnPieces}" var="currentMedicine">
            <tr>
                <td>${currentMedicine.id}</td>
                <td>${currentMedicine.name}</td>
                <cor:choose>
                    <cor:when test="${currentMedicine.dose==0}">
                        <td> </td>
                    </cor:when>
                    <cor:otherwise>
                        <td>${currentMedicine.dose} ${currentMedicine.doseUnit}</td>
                    </cor:otherwise>
                </cor:choose>
                <td>${currentMedicine.kind}</td>
                <td>${currentMedicine.capacity}</td>
                <td>${currentMedicine.count}</td>
                <td>
                    <input type="number" value="${currentMedicine.demandedCount}" min="0" max="100">
                </td>
                <td>
                    <a class="dropdown-button tooltipped modal-trigger" href="#" data-position="bottom"
                       data-delay="50"
                       data-tooltip="Zapisz" onclick="saveDemandedState(${currentMedicine.id}, event);">
                        <i class="small material-icons">save</i>
                    </a>
                </td>
            </tr>
        </cor:forEach>
        </tbody>
    </table>

    <ul class="pagination">
        <cor:forEach var="counter" begin="1" end="${medicineStatePagination}">
            <cor:if test="${counter == 1}">
                <li class="active waves-effect tablePagination1"><a
                        onclick="changePharmacyList(this,${counter},1)">${counter}</a></li>
            </cor:if>
            <cor:if test="${counter > 1}">
                <li class="waves-effect tablePagination1"><a id="1"
                                                             onclick="changePharmacyList(this,${counter},1)">${counter}</a>
                </li>
            </cor:if>
        </cor:forEach>
    </ul>
    </div>
</cor:if>

<div id="info_modal" class="modal">
    <div class="modal-content">
        <div class="center-align" id="info_text"><h5>Operacja zakończona pomyślnie</h5></div>
    </div>
</div>

<script>
    var home = document.getElementById("homeUnderline");
    home.classList.remove("active");

    var order = document.getElementById("orderUnderline");
    order.classList.remove("active");

    var request = document.getElementById("requestUnderline");
    request.className += "active";

    var report = document.getElementById("reportUnderline");
    report.classList.remove("active");

    var manage = document.getElementById("manageUnderline");
    manage.classList.remove("active");
</script>

<script type="text/javascript" charset="utf-8">
    function deleteRows(table) {
        var rowCount = table.rows.length;
        for (var i = rowCount - 1; i > 0; i--) {
            table.deleteRow(i);
        }
    }

    function changePharmacyList(event, page, tableNr) {

        var json = {"action": "pagination", "page": page, "table": tableNr};

        var target = event.target;
        console.log(event.id);

        var pages = document.getElementsByClassName("tablePagination" + tableNr);
        for (i = 0; i < pages.length; i++) {
            pages[i].classList.remove("active");
        }
        event.parentNode.classList.add("active");

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/request.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {

                var medicines = JSON.parse(response);

                var table;

                if (tableNr == 1) {
                    table = document.getElementById("demandedState");
                }
                deleteRows(table);
                table = table.tBodies[0];


                for (i = 0; i < medicines.length; i++) {
                    var row = table.insertRow(-1); // add at the end

                    if (tableNr == 1) {
                        var cell1 = row.insertCell(0);
                        var cell2 = row.insertCell(1);
                        var cell3 = row.insertCell(2);
                        var cell4 = row.insertCell(3);
                        var cell5 = row.insertCell(4);
                        var cell6 = row.insertCell(5);
                        var cell7 = row.insertCell(6);
                        var cell8 = row.insertCell(7);

                        cell1.innerHTML = medicines[i].id;
                        cell2.innerHTML = medicines[i].name;
                        if(medicines[i].dose == 0)
                            cell3.innerHTML = " ";
                        else
                            cell3.innerHTML = medicines[i].dose + ' ' + medicines[i].doseUnit;
                        cell4.innerHTML = medicines[i].kind;
                        cell5.innerHTML = medicines[i].capacity;
                        cell6.innerHTML = medicines[i].count;
                        cell7.innerHTML = '<input type="number" value="'+medicines[i].demandedCount+'" min="0" max="100">';
                        cell8.innerHTML =  getActionButtonsInTable(medicines[i].id);
                    }
                }
                $('.addedTooltip').tooltip({delay: 50});
            },

            error: function () {
            }
        });
    }

    function getActionButtonsInTable(typeId)
    {
        var actionButtons;

            actionButtons = ' <a class="dropdown-button tooltipped modal-trigger" data-position="bottom" '+
                ' data-delay="50" '+
                ' data-tooltip="Zapisz" onclick="saveDemandedState('+typeId+', event);"> '+
                ' <i class="small material-icons">save</i></a> ';

        return actionButtons;
    }

    function saveDemandedState(typeId, event) {
        console.log("Save start");
        var info_text = document.getElementById("info_text");

        var row = event.target.parentElement.parentElement.parentElement;
        var cells = row.cells;
        var stateCell = cells[6];
        var newDemandedState = stateCell.getElementsByTagName("input")[0].value;

        var pharmacyId = ${currentPharmacy};
        var json = {"action": "save", "typeId": typeId, "newDemandedState": newDemandedState, "pharmacyId": pharmacyId};

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/request.json",
            cache: false,

            data: json,
            dataType: "text",
            success: function (response) {
                console.log("save end");

                info_text.innerHTML = '<h5 style="color: green;">Operacja zakończona pomyślnie!</h5>';
                $('#info_modal').modal('open');
            },
            error: function () {
                info_text.innerHTML = '<h5 style="color: red;">Operacja nie powiodła się!</h5>';
                $('#info_modal').modal('open');
            }
        });
    }

    function setDateAndTime(dateSpan) {
        var nazwy_mies = ['Stycznia', 'Lutego', 'Marca', 'Kwietnia', 'Maja',
            'Czerwca', 'Lipca', 'Sierpnia', 'Września', 'Października',
            'Listopada', 'Grudnia'];

        var currentDate = "${currentDate}";
        var currentTime = "${currentTime}";

        var rok = currentDate.toString().substring(0,4);
        var mies = (currentDate-1).toString().substring(4,6);
        var dzien = currentDate.toString().substring(6,8);
        var godz = currentTime.toString().substring(0,2);
        var min = currentTime.toString().substring(2,4);
        var sec = currentTime.toString().substring(4,6);


        var data_i_czas = dzien + ' ' + nazwy_mies[mies-1] + ' ' + rok
            + '\n ' + godz + ':' + min + ':' + sec;

        dateSpan.innerHTML = data_i_czas;
    }
</script>
